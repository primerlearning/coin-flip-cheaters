﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Scene3_TruePositives : Director
{
    [SerializeField] CoinFlipper flipperPrefab;
    [SerializeField] CoinFlipSimManager manager;
    public BinaryTree treePrefab;
    [SerializeField] List<BTNode> NodePrefabs;

    PrimerText headsLabel, tailsLabel;
    public VideoClip goalsClip;
    ReferenceScreen goalsReferenceScreen;
    BinaryTree tree;
    CoinFlipper flipper;

    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        // Time.timeScale = 80;
        manager.Initialize(flipperPrefab);
        base.Start();
    }
    //Define event actions
    protected virtual IEnumerator Appear()
    {
        // Show a blob flipping and the list of goals
        // Update the second goal
        // How does a trick coin behave?
        // Show 75% and calculate 0.75^5

        // Show the group results again, 
        // shift some uncaught cheaters to the caught bucket

        // Show "If 7 of ten heads"
        Vector3 startingCamSwivelOrigin = new Vector3(0, 1, 0);
        Quaternion startingCamSwivel = Quaternion.Euler(13, 0, 0);
        float startingCamDistance = 7;

        camRig.SwivelOrigin = startingCamSwivelOrigin;
        camRig.Swivel = startingCamSwivel;
        camRig.Distance = startingCamDistance;

        flipper = manager.AddFlipper();
        flipper.transform.position = new Vector3(0, -1.3f, 1.5f);
        // flipper.resultsDisplayMode = ResultsDisplayMode.Individual;
        flipper.Appear();
        ((PrimerBlob)flipper.flipperCharacter).SetColor(PrimerColor.Blue);
        flipper.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(8, 0);

        // goalsReferenceScreen = MakeReferenceScreen(goalsClip);
        // goalsReferenceScreen.transform.localScale = Vector3.one * 60;
        // goalsReferenceScreen.transform.localPosition = new Vector3(-30, 16, 50);
        // goalsReferenceScreen.Play();
        
        yield return new WaitUntilSceneTime(8, 0);
        PrimerObject display = Instantiate(flipper.display);
        yield return new WaitUntilSceneTime(8, 6.5f);
        PrimerText hmc = Instantiate(primerTextPrefab);
        hmc.tmpro.text = "What fraction of cheaters \n do we want to catch?";
        hmc.transform.SetParent(camRig.transform);
        hmc.transform.localPosition = new Vector3(8.86f, 6.24f, 18.45f);
        hmc.transform.localRotation = Quaternion.identity;
        hmc.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(8, 12);
        PrimerText mtpr = Instantiate(primerTextPrefab);
        mtpr.tmpro.text = "What is the minimum \n true positive rate?";
        mtpr.transform.SetParent(camRig.transform);
        mtpr.transform.localPosition = new Vector3(8.86f, 2.4f, 18.45f);
        mtpr.transform.localRotation = Quaternion.identity;
        mtpr.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(8, 13);
        PrimerText tats = Instantiate(primerTextPrefab);
        tats.tmpro.text = "(These are the same)";
        tats.transform.SetParent(camRig.transform);
        tats.transform.localPosition = new Vector3(14.9f, -0.5f, 30.5f);
        tats.transform.localRotation = Quaternion.identity;
        tats.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(8, 16.5f);
        tats.ScaleDownToZero();
        yield return new WaitUntilSceneTime(8, 16.75f);
        mtpr.ScaleDownToZero();
        yield return new WaitUntilSceneTime(8, 20.5f);
        mtpr.tmpro.text = "\"Statistical Power\"";
        mtpr.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(8, 41f);
        hmc.ScaleDownToZero();
        yield return new WaitForSeconds(0.25f);
        mtpr.ScaleDownToZero();
        // goalsReferenceScreen.MoveTo(Vector3.left * 1000);
        yield return new WaitUntilSceneTime(8, 45f);
        flipper.MoveTo(new Vector3(-3.8f, -2f, 0));
        camRig.MoveCenterTo(Vector3.up);
        camRig.ZoomTo(10);
        camRig.SwivelTo(Quaternion.Euler(13, 0, 0));
        yield return new WaitForSeconds(0.5f);
        tree = BinaryTree.DecisionTree(5, treePrefab, NodePrefabs[1], NodePrefabs[0]);
        tree.transform.position = new Vector3(0, 10.5f, 23.2f);
        tree.root.buffer = 6f;
        tree.PlaceNodes(5);
        tree.DisplayToLevel(6);
        tree.transform.localScale = Vector3.one * 0.4f;
        yield return new WaitForSeconds(1f);
        PrimerText po5fc = Instantiate(primerTextPrefab);
        po5fc.tmpro.text = "Probability of five heads \n in a row from a cheater?";
        po5fc.transform.SetParent(camRig.transform);
        po5fc.transform.localPosition = new Vector3(8.6f, -4.29f, 18.45f);
        po5fc.transform.localRotation = Quaternion.identity;
        po5fc.ScaleUpFromZero();

        StartCoroutine(Flip5());

        yield return new WaitForSeconds(2);
        tree.AnimatePathColor(tree.root.Right.Right.Right.Right.Right, PrimerColor.Red);
        // yield return new WaitUntilSceneTime(3, 37);
        // tree.AnimatePathColor(tree.root.Right.Right, Color.white);
        yield return new WaitUntilSceneTime(9, 9.5f);
        StartCoroutine(ShowBranchLabels(tree.root, "", 0.1f));

        yield return new WaitUntilSceneTime(9, 16.5f);
        PrimerText assTime = Instantiate(primerTextPrefab);
        assTime.tmpro.text = "Assumption\nTime";
        assTime.transform.SetParent(camRig.transform);
        assTime.transform.localPosition = new Vector3(18.85f, 10.8f, 23.3f);
        assTime.transform.localRotation = Quaternion.identity;
        assTime.ScaleUpFromZero();
        StartCoroutine(flashAndWiggle(assTime, PrimerColor.Orange, 5, 2, 40));
        for (int i = 1; i <= 5; i++) {
            foreach (BTNode node in tree.GetLevel(i)) {
                if (node.label != null) {
                    StartCoroutine(flashAndWiggle(node.label, PrimerColor.Orange, 5, 2, 40));
                }
            }
        }

        yield return new WaitUntilSceneTime(9, 25.5f);
        PrimerText effSize = Instantiate(primerTextPrefab);
        effSize.tmpro.text = "\"Effect Size\"";
        effSize.transform.SetParent(camRig.transform);
        effSize.transform.localPosition = new Vector3(-15.3f, 9.4f, 19.4f);
        effSize.transform.localRotation = Quaternion.identity;
        effSize.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(9, 56.5f);
        effSize.ScaleDownToZero();
        assTime.ScaleDownToZero();

        yield return new WaitUntilSceneTime(9, 58);
        po5fc.ScaleDownToZero();

        yield return new WaitUntilSceneTime(10, 8);
        tree.Hide();


        yield return new WaitUntilSceneTime(10, 56);
        Time.timeScale = 1;
        flipper.display.ScaleDownToZero();
        yield return new WaitForSeconds(0.5f);
        flipper.display = display;
        flipper.results.Clear();

        flipper.headsRate = 0.75f;
        flipper.displayRowLength = 10;
        flipper.displayCoinSpacing = 3.5f;
        flipper.Flip(outcome: 0);
        yield return flipper.waitUntilCoinIsStopped();
        PrimerObject[] row = new PrimerObject[50];
        row[0] = flipper.coin;
        flipper.coin.SetIntrinsicScale(1);
        int[] outcomes = new int[] { 0b1111111000, 0b0001111111, 0b1101101101, 0b1010110111, 0b1100111011 };
        flipper.display.transform.position = new Vector3(1.6f, -0.45f, 3);
        flipper.display.transform.rotation = camRig.transform.rotation;
        flipper.display.transform.localScale = Vector3.one * 0.3f;
        for (int i = 1; i < 50; i++)
        {
            row[i] = Instantiate(flipper.coin);
            row[i].GetComponent<Rigidbody>().isKinematic = true;
        }
        for (int j = 0; j < 5; j++)
        {
            int outcome = outcomes[j];
            for (int i = 0; i < 10; i++)
            {
                flipper.coin = row[i + 10 * j];
                // Debug.Log(outcome >> (9 - i) & 1);
                StartCoroutine(flipper.recordAndDisplay(outcome: outcome >> (9 - i) & 1));
            }
            yield return new WaitForSeconds(0.5f);
        }
        yield return new WaitUntilSceneTime(11, 4.5f);
        flipper.display.transform.parent = null;
        foreach (PrimerObject po in display.transform.GetComponentsInChildren<PrimerObject>()) {
            if (po != display.GetComponent<PrimerObject>()) {
                po.ScaleDownToZero();
            }
        }
        flipper.ScaleDownToZero();
        yield return new WaitForSeconds(0.5f);
    }
    IEnumerator ShowBranchLabels(BTNode node, string labelVal, float delay) {
        if (node != tree.root) {
            node.Label(primerTextPrefab);
            node.PlaceLabel();
            node.label.tmpro.text = labelVal;
            node.label.gameObject.SetActive(true);
            node.label.ScaleUpFromZero();
            yield return new WaitForSeconds(delay);
        }
        if (node.Right != null) {
            StartCoroutine(ShowBranchLabels(node.Right, "75%", delay));
        }
        if (node.Left != null) {
            StartCoroutine(ShowBranchLabels(node.Left, "25%", delay));
        }
    }
    IEnumerator Flip5() {
        for (int i = 0; i < 5; i++)
        {
            flipper.FlipAndRecord(1);
            yield return flipper.waitUntilCoinIsStopped();
        }
    }
    IEnumerator flashAndWiggle(PrimerText pt, Color newColor, float angle, float period, float duration) {
        float startTime = Time.time;
        Color oldColor = pt.tmpro.color;
        Quaternion oldRotation = pt.transform.localRotation;
        Quaternion newRotation1 = oldRotation * Quaternion.Euler(0, 0, angle);
        Quaternion newRotation2 = oldRotation * Quaternion.Euler(0, 0, -angle);
        while (Time.time - startTime < duration) {
            pt.RotateTo(newRotation1, duration: period / 2);
            pt.ChangeColor(newColor, duration: period / 4); 
            yield return new WaitForSeconds(period / 4);
            pt.ChangeColor(oldColor, duration: period / 4);
            yield return new WaitForSeconds(period / 4);

            pt.RotateTo(newRotation2, duration: period / 2);
            pt.ChangeColor(newColor, duration: period / 4); 
            yield return new WaitForSeconds(period / 4);
            pt.ChangeColor(oldColor, duration: period / 4);
            yield return new WaitForSeconds(period / 4);
        }
        pt.RotateTo(oldRotation, duration: period / 4);
    }
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(7 * 60 + 54.5f, Appear);
    }
}