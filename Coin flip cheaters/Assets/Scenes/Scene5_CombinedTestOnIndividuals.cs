﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Scene5_CombinedTestOnIndividuals : Director
{
    [SerializeField] CoinFlipSimManager simManager;
    [SerializeField] CoinFlipper flipperPrefab;
    CoinFlipper firstFlipper = null;
    CoinFlipper secondFlipper = null;
    PrimerObject graphParent = null;
    Graph graph1;
    Graph graph2;
    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        base.Start();
    }
    //Define event actions
    protected virtual IEnumerator Appear() {
        // Time.timeScale = 10;
        Vector3 startingCamSwivelOrigin = new Vector3(0, 1, 0);
        Quaternion startingCamSwivel = Quaternion.Euler(13, 0, 0);
        float startingCamDistance = 7;

        camRig.SwivelOrigin = startingCamSwivelOrigin;
        camRig.Swivel = startingCamSwivel;
        camRig.Distance = startingCamDistance;

        // Initialize manager
        simManager.Initialize(flipperPrefab);

        // Show 1 blob
        firstFlipper = simManager.AddFlipper(headsRate: 0.75f);
        // firstFlipper.flipperCharacter = Instantiate(firstFlipper.flipperCharacterPrefab);
        // firstFlipper.flipperCharacter.transform.localScale = Vector3.zero;
        firstFlipper.transform.position = new Vector3(0, -1.3f, 1.5f);
        simManager.ShowFlippers();
        ((PrimerBlob)firstFlipper.flipperCharacter).SetColor(PrimerColor.Purple);
        ((PrimerBlob)firstFlipper.flipperCharacter).AddAccessory(accessoryType: AccessoryType.monocle, colorMatch: true);
        yield return new WaitForSeconds(1);
    }
    IEnumerator FirstTest() {
        PauseVoiceOverAtTime(14, 16);

        // yield return StartCoroutine(simManager.testFlippers(23, 16));
        firstFlipper.rng = new System.Random(1);
        List<int> outcomes = new List<int>();
        while (outcomes.Count < 23) {
            outcomes.Add(firstFlipper.rng.Next(0, 2));
            if (outcomes.Count == 23 && outcomes.Sum() != 17) {
                outcomes = new List<int>();
            }
        }

        foreach (int outcome in outcomes) {
            yield return firstFlipper.flipAndRecord(outcome: outcome, delay: 0.5f);
        }

        // yield return StartCoroutine(simManager.testFlippers(1, 1));
    }
    IEnumerator FirstDiscussion() {
        yield return new WaitUntilSceneTime(14, 24);
        BlobAccessory sign = null;
        sign = firstFlipper.flipperCharacter.GetComponent<PrimerBlob>().AddAccessory(AccessoryType.cheaterSign, animate: true);
        float vDisp = 1f;
        float extraDisp = 0;
        BlobAccessory prev = firstFlipper.flipperCharacter.GetComponent<PrimerBlob>().accessories[0];
        if (BlobAccessory.SignHeights.ContainsKey(prev.accessoryType)) { extraDisp = BlobAccessory.SignHeights[prev.accessoryType]; }
        sign.transform.localPosition = (vDisp + extraDisp) * Vector3.up;
        yield return new WaitForSeconds(0.5f);
        sign.MoveTo(extraDisp * Vector3.up);
        yield return new WaitForSeconds(0.5f);

        yield return new WaitUntilSceneTime(14, 28);
        firstFlipper.MoveTo(new Vector3(-4.7f, -1.3f, 1));
        camRig.ZoomTo(9.5f);
        camRig.MoveCenterTo(Vector3.up * 2);
        yield return new WaitForSeconds(0.5f);
        graphParent = new GameObject().MakePrimerObject();
        graphParent.gameObject.name = "graphs";
        graphParent.transform.position = new Vector3(1.3f, 0, -2.9f);
        graphParent.transform.rotation = camRig.transform.rotation;

        Dictionary<float, string> xTics = new Dictionary<float, string> () {
            {6, 5.ToString()},
            {11, 10.ToString()},
            {16, 15.ToString()},
            {21, 20.ToString()},
            // {26, 25.ToString()},
        };
        graph1 = Instantiate(primerGraphPrefab);
        graph1.Initialize(
            xAxisLength: 5f,
            xMax: 24.7f,
            manualTicMode: true,
            manualTicsX: xTics,
            yMax: 0.2f,
            yTicStep: 0.1f,
            yAxisLength: 2f,
            thickness: 1.5f,
            zHidden: true,
            xAxisLabelPos: "along",
            xAxisLabelString: "Heads",
            yAxisLabelString: "Fair player probability"
        );
        graph1.transform.parent = graphParent.transform;
        graph1.transform.localPosition = new Vector3(0, 3.5f, 0);
        graph1.transform.localRotation = Quaternion.identity;
        graph1.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);

        // Build bars
        List<float> probabilities1 = new List<float>();
        List<Color> graph1Colors = new List<Color>();
        int testThreshold = 16;
        for (int i = 0; i < 24; i++) {
            probabilities1.Add((float) Helpers.Binomial(23, i, 0.5f));
            if (i < testThreshold) {
                graph1Colors.Add(PrimerColor.Blue);
            }
            else {
                graph1Colors.Add(PrimerColor.Blue / Mathf.Sqrt(2));
            }
        }
        BarDataManager bd = graph1.AddBarData();
        graph1.barData.barWidth = 0.9f;

        graph1.barData.AnimateBars(probabilities1);
        graph1.barData.SetBarColors(graph1Colors);

        // yield return new WaitForSeconds(0.5f);
        PrimerShapesLine testThresholdLine = new GameObject().AddComponent<PrimerShapesLine>();
        testThresholdLine.line.Thickness = 0.0f;
        testThresholdLine.line.Dashed = true;
        testThresholdLine.line.DashSpace = Shapes.DashSpace.Meters;
        testThresholdLine.line.DashSize = 0.15f;
        testThresholdLine.line.DashType = Shapes.DashType.Rounded;
        testThresholdLine.line.Geometry = Shapes.LineGeometry.Flat2D;
        testThresholdLine.animatedOffsetSpeed = 0.001f;
        testThresholdLine.transform.parent = graph1.transform;
        testThresholdLine.transform.localPosition = Vector3.zero;
        testThresholdLine.transform.localScale = Vector3.one;
        testThresholdLine.transform.localRotation = Quaternion.identity;
        testThresholdLine.Start = graph1.CoordinateToPosition(16.5f, 0, 0);
        testThresholdLine.End = graph1.CoordinateToPosition(16.5f, 0.2f, 0);

        testThresholdLine.AnimateValue<float>("Thickness", 0.05f);



        yield return new WaitUntilSceneTime(14, 33);
        // Fair player graph notes
        PrimerBracket bracket = Instantiate(primerBracketPrefab);
        bracket.transform.parent = graph1.transform;
        bracket.transform.localRotation = Quaternion.identity;
        // bracket.transform.localScale = Vector3.one * 0.5f;
        bracket.SetPoints(
            graph1.CoordinateToPosition(21.5f, 0.05f, 0),
            graph1.CoordinateToPosition(17.6f, 0.02f, 0),
            graph1.CoordinateToPosition(24, 0.005f, 0)
        );
        bracket.ScaleUpFromZero();
        
        PrimerText pValue = Instantiate(primerTextPrefab);
        pValue.tmpro.text = "~1.7%";
        pValue.transform.parent = graph1.transform;
        pValue.transform.localRotation = Quaternion.identity;
        pValue.transform.localPosition = graph1.CoordinateToPosition(21.5f, 0.09f, 0);
        pValue.SetIntrinsicScale(0.3f);
        pValue.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(14, 37);

        PrimerText pValueLabel = Instantiate(primerTextPrefab);
        pValueLabel.tmpro.text = "\"p-value\"";
        pValueLabel.transform.parent = graph1.transform;
        pValueLabel.transform.localRotation = Quaternion.identity;
        pValueLabel.transform.localPosition = new Vector3(2.5f, 1.6f, 0);
        pValueLabel.SetIntrinsicScale(0.3f);
        pValueLabel.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(14, 54.5f);

        // yield return new WaitForSeconds(0.5f);
        graph2 = Instantiate(primerGraphPrefab);
        graph2.Initialize(
            xAxisLength: 5f,
            xMax: 24.7f,
            manualTicMode: true,
            manualTicsX: xTics,
            yMax: 0.2f,
            yTicStep: 0.1f,
            yAxisLength: 2f,
            thickness: 1.5f,
            zHidden: true,
            xAxisLabelPos: "along",
            xAxisLabelString: "Heads",
            yAxisLabelString: "Cheater Probability"
        );
        graph2.transform.parent = graphParent.transform;
        graph2.transform.localPosition = Vector3.down * 0.2f;
        graph2.transform.localRotation = Quaternion.identity;
        graph2.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        List<float> probabilities2 = new List<float>();
        List<Color> graph2Colors = new List<Color>();
        for (int i = 0; i < 24; i++) {
            probabilities2.Add((float) Helpers.Binomial(23, i, 0.75f));
            if (i < testThreshold) {
                graph2Colors.Add(PrimerColor.Red / Mathf.Sqrt(2));
            }
            else {
                graph2Colors.Add(PrimerColor.Red);
            }
        }
        BarDataManager bd2 = graph2.AddBarData();
        bd2.barWidth = 0.9f;
        bd2.AnimateBars(probabilities2);
        bd2.SetBarColors(graph2Colors);
        // yield return new WaitForSeconds(0.5f);

        PrimerShapesLine testThresholdLine2 = new GameObject().AddComponent<PrimerShapesLine>();
        testThresholdLine2.line.Thickness = 0.0f;
        testThresholdLine2.line.Dashed = true;
        testThresholdLine2.line.DashSpace = Shapes.DashSpace.Meters;
        testThresholdLine2.line.DashSize = 0.15f;
        testThresholdLine2.line.DashType = Shapes.DashType.Rounded;
        testThresholdLine2.animatedOffsetSpeed = 0.001f;
        testThresholdLine2.transform.parent = graph2.transform;
        testThresholdLine2.transform.localPosition = Vector3.zero;
        testThresholdLine2.transform.localScale = Vector3.one;
        testThresholdLine2.transform.localRotation = Quaternion.identity;
        testThresholdLine2.Start = graph2.CoordinateToPosition(16.5f, 0, 0);
        testThresholdLine2.End = graph2.CoordinateToPosition(16.5f, 0.2f, 0);
        testThresholdLine2.AnimateValue<float>("Thickness", 0.05f);

        // Cheater graph notes
        yield return new WaitUntilSceneTime(14, 59);
        PrimerBracket bracket2 = Instantiate(primerBracketPrefab);
        bracket2.transform.parent = graph2.transform;
        bracket2.transform.localRotation = Quaternion.identity;
        bracket2.transform.localScale = Vector3.one * 0.5f;
        bracket2.SetPoints(
            graph2.CoordinateToPosition(20.5f, 0.23f, 0),
            graph2.CoordinateToPosition(17.6f, 0.2f, 0),
            graph2.CoordinateToPosition(24, 0.18f, 0)
        );
        bracket2.ScaleUpFromZero();
        
        PrimerText bizzaroPValue = Instantiate(primerTextPrefab);
        bizzaroPValue.tmpro.text = "~65%";
        bizzaroPValue.transform.parent = graph2.transform;
        bizzaroPValue.transform.localRotation = Quaternion.identity;
        bizzaroPValue.transform.localPosition = graph2.CoordinateToPosition(21.5f, 0.27f, 0);
        bizzaroPValue.SetIntrinsicScale(0.3f);
        bizzaroPValue.ScaleUpFromZero();

        // Put away stuff specific to first flipper
        yield return new WaitUntilSceneTime(15, 13.5f);
        firstFlipper.ScaleDownToZero();
        bizzaroPValue.ScaleDownToZero();
        bracket2.ScaleDownToZero();
        pValue.ScaleDownToZero();
        pValueLabel.ScaleDownToZero();
        bracket.ScaleDownToZero();
    }
    IEnumerator SecondTest() {
        simManager.flippers = new List<CoinFlipper>();
        secondFlipper = simManager.AddFlipper(headsRate: 0.75f);
        secondFlipper.transform.position = new Vector3(-4.7f, -1.3f, 1);
        secondFlipper.rng = new System.Random(2);
        simManager.ShowFlippers();
        ((PrimerBlob)secondFlipper.flipperCharacter).SetColor(PrimerColor.Yellow);
        ((PrimerBlob)secondFlipper.flipperCharacter).AddAccessory(accessoryType: AccessoryType.froggyHat);
        ((PrimerBlob)secondFlipper.flipperCharacter).accessories[0].SetColor(textureName: "texture_froggy_hat_green");
        yield return new WaitForSeconds(0.5f);
        // yield return new WaitUntilSceneTime(
        List<int> outcomes = new List<int>();
        while (outcomes.Count < 23) {
            outcomes.Add(secondFlipper.rng.Next(0, 2));
            if (outcomes.Count == 23 && outcomes.Sum() != 13) {
                outcomes = new List<int>();
            }
        }

        foreach (int outcome in outcomes) {
            yield return secondFlipper.flipAndRecord(outcome: outcome, delay: 0.5f);
        }
        // yield return StartCoroutine(simManager.testFlippers(23, 16));
        // yield return StartCoroutine(simManager.testFlippers(1, 1));

    }
    IEnumerator SecondDiscussion() {
        yield return new WaitUntilSceneTime(15, 29.5f);
        BlobAccessory sign = null;
        sign = secondFlipper.flipperCharacter.GetComponent<PrimerBlob>().AddAccessory(AccessoryType.fairSign, animate: true);
        float vDisp = 1f;
        float extraDisp = 0;
        BlobAccessory prev = secondFlipper.flipperCharacter.GetComponent<PrimerBlob>().accessories[0];
        if (BlobAccessory.SignHeights.ContainsKey(prev.accessoryType)) { extraDisp = BlobAccessory.SignHeights[prev.accessoryType]; }
        sign.transform.localPosition = (vDisp + extraDisp) * Vector3.up;
        yield return new WaitForSeconds(0.5f);
        sign.MoveTo(extraDisp * Vector3.up);
        yield return new WaitForSeconds(0.5f);

        yield return new WaitUntilSceneTime(15, 32);
        PrimerText pValueLabel = Instantiate(primerTextPrefab);
        pValueLabel.tmpro.text = "\"p-value\"";
        pValueLabel.transform.parent = graph1.transform;
        pValueLabel.transform.localRotation = Quaternion.identity;
        pValueLabel.transform.localPosition = new Vector3(3f, 0.5f, 0);
        pValueLabel.SetIntrinsicScale(0.3f);
        pValueLabel.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(15, 33.5f);
        // Fair player graph notes
        PrimerBracket bracket = Instantiate(primerBracketPrefab);
        bracket.transform.parent = graph1.transform;
        bracket.transform.localRotation = Quaternion.identity;
        // bracket.transform.localScale = Vector3.one * 0.5f;
        bracket.SetPoints(
            graph1.CoordinateToPosition(18.8f, 0.24f, 0),
            graph1.CoordinateToPosition(13.6f, 0.2f, 0),
            graph1.CoordinateToPosition(24, 0.2f, 0)
        );
        bracket.ScaleUpFromZero();
        
        PrimerText pValue = Instantiate(primerTextPrefab);
        pValue.tmpro.text = "~34%";
        pValue.transform.parent = graph1.transform;
        pValue.transform.localRotation = Quaternion.identity;
        pValue.transform.localPosition = new Vector3(2.3f, 1.65f, 0);
        pValue.SetIntrinsicScale(0.3f);
        pValue.ScaleUpFromZero();

        // Cheater graph notes
        yield return new WaitUntilSceneTime(15, 50.7f);
        PrimerBracket bracket2 = Instantiate(primerBracketPrefab);
        bracket2.transform.parent = graph2.transform;
        bracket2.transform.localRotation = Quaternion.identity;
        bracket2.transform.localScale = Vector3.one * 0.5f;
        bracket2.SetPoints(
            graph2.CoordinateToPosition(18.8f, 0.24f, 0),
            graph2.CoordinateToPosition(13.6f, 0.2f, 0),
            graph2.CoordinateToPosition(24, 0.2f, 0)
        );
        bracket2.ScaleUpFromZero();
        
        PrimerText bizzaroPValue = Instantiate(primerTextPrefab);
        bizzaroPValue.tmpro.text = "~99%";
        bizzaroPValue.transform.parent = graph2.transform;
        bizzaroPValue.transform.localRotation = Quaternion.identity;
        bizzaroPValue.transform.localPosition = new Vector3(2.3f, 1.65f, 0);
        bizzaroPValue.SetIntrinsicScale(0.3f);
        bizzaroPValue.ScaleUpFromZero();

        // Put away stuff specific to first flipper
        // yield return new WaitUntilSceneTime(15, 13.5f);
        // secondFlipper.ScaleDownToZero();
        // bizzaroPValue.ScaleDownToZero();
        // bracket2.ScaleDownToZero();
        // pValue.ScaleDownToZero();
        // bracket.ScaleDownToZero();
    }
    IEnumerator JustBlobs() {
        Time.timeScale = 1;
        graph1.ScaleDownToZero();
        graph2.ScaleDownToZero();
        yield return new WaitForSeconds(0.5f);
        // firstFlipper.transform.position = new Vector3(3.7f, -1.3f, 1);
        // firstFlipper.ScaleUpFromZero();
        ((PrimerBlob)firstFlipper.flipperCharacter).transform.parent = null;
        ((PrimerBlob)firstFlipper.flipperCharacter).transform.position = new Vector3(2.5f, 0, -3);
        ((PrimerBlob)firstFlipper.flipperCharacter).transform.rotation = Quaternion.Euler(0, 180, 0);
        ((PrimerBlob)firstFlipper.flipperCharacter).ScaleUpFromZero();

        ((PrimerBlob)secondFlipper.flipperCharacter).transform.parent = null;
        ((PrimerBlob)secondFlipper.flipperCharacter).MoveTo(new Vector3(-2.5f, 0, -3));
        ((PrimerBlob)secondFlipper.flipperCharacter).RotateTo(Quaternion.Euler(0, 180, 0));
        secondFlipper.ScaleDownToZero();
        yield return new WaitUntilSceneTime(16, 3.5f);
        ((PrimerBlob)firstFlipper.flipperCharacter).animator.SetBool("Sad", true);
        yield return new WaitUntilSceneTime(16, 5.5f);
        ((PrimerBlob)secondFlipper.flipperCharacter).EvilPose();
        yield return new WaitUntilSceneTime(16, 19.5f);
    }
    IEnumerator Stall() {
        yield return new WaitForSeconds(1000);
    }
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(14, 15f, Appear);
        new SceneBlock(14, 16f, FirstTest, flexible: true);
        new SceneBlock(14, 17f, FirstDiscussion);
        new SceneBlock(15, 14.5f, SecondTest, flexible: true);
        new SceneBlock(15, 17.5f, SecondDiscussion);
        new SceneBlock(16, 1.5f, JustBlobs);
        // new SceneBlock(200, 0, Stall, flexible: true);
    }
}
