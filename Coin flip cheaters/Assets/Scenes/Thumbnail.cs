﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Thumbnail : Director
{
    [SerializeField] PrimerBlob blobPrefab;
    //Define event actions
    protected virtual IEnumerator Appear() {
        PrimerBlob b = Instantiate(blobPrefab);
        b.AddAccessory(accessoryType: AccessoryType.detectiveHat);
        b.transform.position = new Vector3(-0.8f, -1.36f, -8.36f);
        b.transform.rotation = Quaternion.Euler(0, 119.5f, 0);
        b.SetColor(PrimerColor.Blue);
        b.animator.SetTrigger("RightEyeStern");
        b.ScaleUpFromZero();
        yield return new WaitForSeconds(1);
    }
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(1f, Appear);
        // new SceneBlock(1f, Words);
    }
}
