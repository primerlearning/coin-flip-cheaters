﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Scene0_IntroScene : Director
{
    [SerializeField] PrimerBlob blobPrefab;
    CoinFlipSimManager manager = null;
    List<CoinFlipper> flippers = new List<CoinFlipper>();
    PrimerBlob detective;
    public AudioClip voiceOverReferenceClip2;
    PrimerText fht;

    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        base.Start();
        Vector3 startingCamSwivelOrigin = new Vector3(0, 0, 0);
        Quaternion startingCamSwivel = Quaternion.Euler(17, 0, 0);
        float startingCamDistance = 10f;
        camRig.SwivelOrigin = startingCamSwivelOrigin;
        camRig.Swivel = startingCamSwivel;
        camRig.Distance = startingCamDistance;
        // Time.timeScale = 20;
    }
    //Define event actions
    protected virtual IEnumerator Appear() {
        // Show two blobs playing the game
        // Bring in detective blob
        // Make circle of example blobs (?)
        // Not actually certain what will happen here,
        // since the interactive part will change this part of the script

        // Transition to showing the detective blob watching 
        // another blob flip coins while showing the list of goals
        manager = new GameObject().AddComponent<CoinFlipSimManager>();
        manager.Initialize(Resources.Load<CoinFlipper>("FlipperPrefabBlob"));

        for (int i = 0; i < 2; i++)
        {
            CoinFlipper flipper = manager.AddFlipper();
            flippers.Add(flipper);
        }

        yield return new WaitUntilSceneTime(1);
        for (int i = 0; i < 2; i++)
        {
            flippers[i].Appear();
            flippers[i].ScaleUpFromZero();
            flippers[i].transform.position = new Vector3(3.5f * (i * 2 - 1), -2, 0);
        }
        ((PrimerBlob) flippers[0].flipperCharacter).SetColor(PrimerColor.Green);
        ((PrimerBlob) flippers[0].flipperCharacter).AddAccessory(accessoryType: AccessoryType.wizardHat, colorMatch: true);
        ((PrimerBlob) flippers[1].flipperCharacter).SetColor(PrimerColor.Red);
        ((PrimerBlob) flippers[1].flipperCharacter).AddAccessory(accessoryType: AccessoryType.beard);
        ((PrimerBlob) flippers[1].flipperCharacter).AddAccessory(accessoryType: AccessoryType.glasses);
        flippers[1].flipperCharacter.transform.localRotation = Quaternion.Euler(Vector3.up * 225);
        flippers[1].flipperCharacter.transform.localPosition = new Vector3(2, 0, 2);
        // flippers[1].transform.rotation = Quaternion.Euler(Vector3.up * 90);
        // PrimerObject display0 = flippers[0].display, display1 = flippers[1].display;
        // PrimerObject display = Instantiate(display0);
        // display.transform.position = Vector3.left * 0.25f;
        // flippers[0].display = display;
        // flippers[1].display = display;
        yield return new WaitUntilSceneTime(3);

        flippers[0].Flip(outcome: 0, initialConditionsIndex: 11);
        yield return flippers[0].waitUntilCoinIsStopped();
        flippers[0].coin.GetComponent<Rigidbody>().isKinematic = true;
        flippers[0].coin.MoveTo(new Vector3(0, 2.5f, 0.5f));
        flippers[0].coin.RotateTo(Quaternion.Euler(0, 0, 180));
        flippers[0].coin.ScaleTo(Vector3.one * 0.5f);
        yield return new WaitForSeconds(3);

        flippers[1].Flip(outcome: 1, initialConditionsIndex: 14);
        flippers[0].coin.ScaleTo(Vector3.one * 0f);
        yield return flippers[1].waitUntilCoinIsStopped();
        flippers[1].coin.GetComponent<Rigidbody>().isKinematic = true;
        flippers[1].coin.MoveTo(new Vector3(0, 2.5f, 0.5f));
        flippers[1].coin.RotateTo(Quaternion.Euler(0, 180, 0));
        flippers[1].coin.ScaleTo(Vector3.one * 0.5f);

        yield return new WaitUntilSceneTime(11.5f);
        flippers[1].flipperCharacter.animator.SetBool("Victory", true);
        flippers[1].flipperCharacter.animator.SetTrigger("MouthSmile");

        yield return new WaitUntilSceneTime(13.5f);
        flippers[0].flipperCharacter.animator.SetBool("Sad", true);

        yield return new WaitUntilSceneTime(15.5f);
        flippers[1].flipperCharacter.animator.SetBool("Victory", false);
        flippers[1].flipperCharacter.animator.SetTrigger("MouthClosed");
        flippers[0].flipperCharacter.animator.SetBool("Sad", false);

        yield return new WaitForSeconds(1);
        flippers[1].coin.ScaleTo(Vector3.one * 0.0f);
        StartCoroutine(repeatedFlips());

        yield return new WaitUntilSceneTime(24f);
        detective = Instantiate(blobPrefab);
        detective.SetColor(PrimerColor.Blue);
        detective.AddAccessory(AccessoryType.detectiveHat);
        // detective.AddAccessory(AccessoryType.monocle);
        detective.transform.position = new Vector3(-13f, -2, 0);
        detective.transform.rotation = Quaternion.Euler(0, 90, 0);
        // detective.transform.position = new Vector3(-20, -2, 5);
        // detective.LookToward(camRig.transform.position);
        detective.WalkTo(new Vector3(-7.5f, -2, 0), duration: 3, ease: EaseMode.SmoothOut);
        // detective.WalkTo(new Vector3(0, -2, 5), duration: 3, ease: EaseMode.SmoothOut);
        // detective.ScaleUpFromZero();
        yield return new WaitForSeconds(3);
        // detective.RotateTo(Quaternion.Euler(0, 180, 0));
        yield return new WaitForSeconds(0.5f);
        detective.StartLookingAt(flippers[1].coin.transform);
        while (flippers[1].coin.transform.localScale.x > 0) {
            yield return null;
        }
        detective.StartLookingAt(camRig.transform);
        // yield return new WaitForSeconds(2);
        yield return new WaitUntilSceneTime(32);
        detective.StopLooking();
        detective.RotateTo(Quaternion.Euler(0, 180, 0));
    }
    IEnumerator repeatedFlips() {
        for (int i = 0; i < 4; i++) {
            CoinFlipper f = flippers[i % 2];
            int outcome = f.rng.NextDouble() < 0.5f ? 1 : 0;
            f.Flip(outcome);
            yield return f.waitUntilCoinIsStopped();
            f.coin.GetComponent<Rigidbody>().isKinematic = true;
            f.coin.MoveTo(new Vector3(0, 2.5f, 0.5f));
            f.coin.ScaleTo(Vector3.one * 0.5f);
            Quaternion rot = Quaternion.Euler(0, 0, 180);
            if (outcome == 1) {rot = Quaternion.Euler(0, 180, 0);}
            f.coin.RotateTo(rot);
            flippers[(i + 1 + outcome) % 2].flipperCharacter.animator.SetBool("Victory", true);
            flippers[(i + 1 + outcome) % 2].flipperCharacter.animator.SetTrigger("MouthSmile");
            flippers[(i + 2 + outcome) % 2].flipperCharacter.animator.SetBool("Sad", true);
            yield return new WaitForSeconds(1.0f);
            flippers[(i + 1 + outcome) % 2].flipperCharacter.animator.SetBool("Victory", false);
            flippers[(i + 1 + outcome) % 2].flipperCharacter.animator.SetTrigger("MouthClosed");
            flippers[(i + 2 + outcome) % 2].flipperCharacter.animator.SetBool("Sad", false);
            yield return new WaitForSeconds(0.5f);
            f.coin.ScaleDownToZero();
        }
    }
    IEnumerator FiveFlipsEach() {
        // Debug.Log("here");
        voiceOverReference.Pause();
        voiceOverReference.clip = voiceOverReferenceClip2;
        voiceOverReference.time = 6;
        voiceOverReference.Play();

        yield return new WaitUntilSceneTime(6.5f);
        camRig.SwivelTo(Quaternion.Euler(27, 0, 0));
        // camRig.MoveCenterTo(Vector3.down * 0.5f);
        camRig.ZoomTo(8.7f);

        flippers[0].ScaleTo(Vector3.one * 0.75f);
        flippers[0].MoveTo(new Vector3(-5, -3, 0));
        flippers[1].ScaleTo(Vector3.one * 0.75f);
        flippers[1].MoveTo(new Vector3(5, 0.5f, 2.5f));
        detective.MoveTo(new Vector3(-9.75f, -3, 2.75f));
        detective.ScaleTo(Vector3.one * 0.75f);
        detective.RotateTo(Quaternion.Euler(0, 135, 0));
        
        manager.AddFlippers(4);
        flippers = manager.flippers;
        foreach (CoinFlipper f in flippers) {
            f.resultsDisplayMode = ResultsDisplayMode.Numeric;
            if (flippers.IndexOf(f) % 2 == 1) {
                f.numericDisplayPosition = new Vector3(-1.25f, 0, 0);
            }
        }
        yield return new WaitUntilSceneTime(7f);
        flippers[2].transform.localPosition = new Vector3(0, -3, 0);
        flippers[2].transform.localScale = Vector3.one * 0.75f;
        flippers[2].Appear();
        ((PrimerBlob) flippers[2].flipperCharacter).SetColor(PrimerColor.Yellow);
        ((PrimerBlob) flippers[2].flipperCharacter).AddAccessory(accessoryType: AccessoryType.eyePatch);
        flippers[3].transform.localPosition = new Vector3(0, 0.5f, 2.5f);
        flippers[3].transform.localScale = Vector3.one * 0.75f;
        flippers[3].Appear();
        flippers[3].flipperCharacter.transform.localRotation = Quaternion.Euler(Vector3.up * 225);
        flippers[3].flipperCharacter.transform.localPosition = new Vector3(2, 0, 2);
        ((PrimerBlob) flippers[3].flipperCharacter).SetColor(PrimerColor.Gray);
        ((PrimerBlob) flippers[3].flipperCharacter).AddAccessory(accessoryType: AccessoryType.propellerHat);
        ((PrimerBlob) flippers[3].flipperCharacter).accessories[0].SetColor(textureName: "propeller cap textures/texture_propeller_cap_dark_green");
        yield return new WaitUntilSceneTime(7.5f);
        flippers[4].transform.localPosition = new Vector3(5, -3, 0);
        flippers[4].transform.localScale = Vector3.one * 0.75f;
        flippers[4].Appear();
        ((PrimerBlob) flippers[4].flipperCharacter).SetColor(PrimerColor.Purple);
        // ((PrimerBlob) flippers[4].flipperCharacter).AddAccessory(accessoryType: AccessoryType.none);
        flippers[5].transform.localPosition = new Vector3(-5, 0.5f, 2.5f);
        flippers[5].transform.localScale = Vector3.one * 0.75f;
        flippers[5].Appear();
        flippers[5].flipperCharacter.transform.localRotation = Quaternion.Euler(Vector3.up * 225);
        flippers[5].flipperCharacter.transform.localPosition = new Vector3(2, 0, 2);
        ((PrimerBlob) flippers[5].flipperCharacter).SetColor(PrimerColor.Orange);
        ((PrimerBlob) flippers[5].flipperCharacter).AddAccessory(accessoryType: AccessoryType.beanie);


        PauseVoiceOverAtTime(10f);
        yield return new WaitUntilSceneTime(8.0f);
        detective.WalkTo(new Vector3(-0.5f, -3, 2.75f), duration: 1.5f, ease: EaseMode.SmoothStep);
        yield return new WaitUntilSceneTime(8.5f);

        List<int> headsCounts = new List<int>() {3,2,4,1,5,0}; // I didn't plan ahead to make the target head counts the same as the indices
        List<Coroutine> crs = new List<Coroutine>();
        for (int i = 0; i < 6; i++) {
            crs.Add(StartCoroutine(Flip5(flippers[i], headsCounts[i])));
        }

        yield return new WaitForSeconds(1f);
        detective.RotateTo(Quaternion.Euler(0, 180, 0));
        detective.StartLookingAt(((PrimerBlob)flippers[2].flipperCharacter).accessories[0].transform);
        yield return new WaitForSeconds(0.5f);
        detective.StartLookingAt(((PrimerBlob)flippers[2].flipperCharacter).transform);
        yield return new WaitForSeconds(0.5f);
        detective.StartLookingAt(((PrimerBlob)flippers[2].flipperCharacter).accessories[0].transform);
        yield return new WaitForSeconds(0.5f);
        detective.StopLooking();
        detective.WalkTo(new Vector3(9, -3, 2.75f), duration: 1.5f, ease: EaseMode.SmoothStep);
        yield return new WaitForSeconds(1.5f);
        detective.RotateTo(Quaternion.Euler(0, 270, 0));
        yield return new WaitForSeconds(0.5f);
        detective.MoveTo(new Vector3(9, 0.5f, 5.25f), duration: 1.5f, ease: EaseMode.SmoothStep);
        yield return new WaitForSeconds(0.25f);
        detective.StartLookingAt(((PrimerBlob)flippers[1].flipperCharacter).accessories[0].transform);
        yield return new WaitForSeconds(1.75f);
        detective.StopLooking();
        detective.WalkTo(new Vector3(-2.4f, 0.5f, 5.25f), duration: 2.5f, ease: EaseMode.SmoothStep);
        yield return new WaitForSeconds(1.0f);
        detective.StartLookingAt(((PrimerBlob)flippers[3].flipperCharacter).accessories[0].transform);
        yield return new WaitForSeconds(0.5f);
        detective.StopLooking();
        yield return new WaitForSeconds(1.0f);
        detective.StartLookingAt(((PrimerBlob)flippers[5].flipperCharacter).accessories[0].transform);
        yield return new WaitForSeconds(1f);
        detective.StopLooking();
        detective.WalkTo(new Vector3(-9.75f, 0.5f, 5.25f), duration: 1.5f, ease: EaseMode.SmoothStep);
        yield return new WaitForSeconds(1f);
        detective.RotateTo(Quaternion.Euler(0, 125, 0));
        
        foreach (Coroutine cr in crs) {
            yield return cr;
        }
    }
    IEnumerator Talk() {

        yield return new WaitUntilSceneTime(15.5f);
        // ((PrimerBlob)flippers[5].flipperCharacter).StartLookingAt(camRig.transform);
        ((PrimerBlob)flippers[5].flipperCharacter).RotateTo(Quaternion.Euler(0, 155, 0));
        yield return new WaitForSeconds(0.5f);
        ((PrimerBlob)flippers[5].flipperCharacter).animator.SetBool("Sad", true);
        yield return new WaitUntilSceneTime(17.5f);
        ((PrimerBlob)flippers[4].flipperCharacter).RotateTo(Quaternion.Euler(0, 175, 0));
        yield return new WaitForSeconds(0.5f);
        ((PrimerBlob)flippers[4].flipperCharacter).EvilPose();
        // ((PrimerBlob)flippers[5].flipperCharacter).StopLooking();
        

        List<int> headsCounts = new List<int>() {3,2,4,1,5,0}; // I didn't plan ahead to make the target head counts the same as the indices
        List<string> rates = new List<string>() {
            "~3%",
            "~8%",
            "~23%",
            "~47%",
            "~71%",
            "~88%",
        };
        List<PrimerText> cheaterRateTexts = new List<PrimerText>();
        yield return new WaitUntilSceneTime(37.0f);
        for (int i = 0; i < 6; i++) {
            PrimerText cheaterRate = Instantiate(primerTextPrefab);
            cheaterRate.tmpro.text = rates[i];
            // Give a parent
            cheaterRate.transform.parent = flippers[headsCounts.IndexOf(i)].flipperCharacter.transform;
            // Set position
            cheaterRate.transform.localPosition = new Vector3(0, 3f, 0);
            // Set scale to zero
            cheaterRate.transform.localScale = new Vector3(0, 0, 0);
            cheaterRate.SetIntrinsicScale(0.6f);
            cheaterRateTexts.Add(cheaterRate);
        }
        cheaterRateTexts[5].ScaleUpFromZero();
        yield return new WaitUntilSceneTime(41.5f);
        for (int i = 4; i >= 0; i--) {
            cheaterRateTexts[i].ScaleUpFromZero();
            yield return new WaitForSeconds(0.1f);
        }


        yield return new WaitUntilSceneTime(57.75f);
        Time.timeScale = 1;
        fht = Instantiate(primerTextPrefab);
        fht.tmpro.text = "\"Frequentist Hypothesis\nTesting\"";
        // Give a parent
        fht.transform.parent = camRig.transform;
        // Set position
        fht.transform.localPosition = new Vector3(-7.05f, 4.61f, 10);
        fht.transform.localRotation = Quaternion.identity;
        fht.SetIntrinsicScale(0.45f);
        fht.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(60f);
    }
    IEnumerator DefineGoals() {
        voiceOverReference.Pause();
        voiceOverReference.clip = voiceOverReferenceClip;
        voiceOverReference.time = 83.5f;
        voiceOverReference.Play();
        yield return new WaitUntilSceneTime(1, 24);
        foreach (CoinFlipper f in flippers) {
            f.ScaleDownToZero();
            if (flippers.IndexOf(f) % 2 == 1) {
                yield return new WaitForSeconds(0.1f);
            }
        }
        yield return new WaitUntilSceneTime(1, 25);
        camRig.ZoomTo(3);
        camRig.SwivelTo(Quaternion.Euler(17, 0, 0));
        camRig.MoveCenterTo(Vector3.up);
        fht.MoveTo(new Vector3(-5.16f, 3.08f, 10));
        fht.ScaleTo(0.6f);
        detective.MoveTo(new Vector3(-1.55f, -0.2f, 0));
        detective.ScaleTo(0.75f);

        yield return new WaitUntilSceneTime(2, 13);
    }
    IEnumerator Flip5(CoinFlipper f, int numHeads) {
        for (int i = 0; i < 5; i++) {
            int outcome = 0;
            if (i < numHeads) {outcome = 1;}
            yield return f.flipAndRecord(outcome: outcome);
        }
    }
    IEnumerator Stall() {
        yield return new WaitUntilSceneTime(1);
    }
    
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(1f, Appear, flexible: true);
        new SceneBlock(6, FiveFlipsEach, flexible: true);
        new SceneBlock(11, Talk, flexible: true);
        new SceneBlock(1, 23f, DefineGoals, flexible: true);
        // new SceneBlock(1000, Stall);
        // new SceneBlock(43f, Reveal, true);
    }
}
