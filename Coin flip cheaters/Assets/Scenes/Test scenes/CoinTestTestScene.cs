﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

public class CoinTestTestScene : Director
{
    float camAngle = 17;
    PrimerText p = null;
    PrimerText d = null;
    [SerializeField] CoinFlipSimManager flipperManager = null;

    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        camRig.cam.transform.localPosition = new Vector3(0, 0, -15);
        camRig.transform.localPosition = new Vector3(0, 1, 0);
        camRig.transform.localRotation = Quaternion.Euler(camAngle, 0, 0);

        flipperManager.Initialize();
        base.Start();
    }

    //Define event actions
    protected virtual IEnumerator Appear() {
        flipperManager.AddFlipper();
        flipperManager.ShowFlippers();
        // p = Director.instance.NewPrimerText();
        // d = Director.instance.NewPrimerText();

        // List<int> factors = Helpers.GetPrimeFactorsOfFactorial(5);
        // foreach (int f in factors) {
        //     Debug.Log(f);
        // }
        // Debug.Log(Helpers.Choose(30, 7));
        yield return null;
        // yield return new WaitForSeconds(2);
        // flipper.FlipTest();
    }
    protected virtual IEnumerator ProbabilityTest() {
        int numFlips = 5;
        for (int i = 0; i < numFlips; i++) {
            yield return flipperManager.flippers[0].flipAndRecord();
            p.tmpro.text = $"p = {flipperManager.flippers[0].CalcProbThisExtremeIfNull()}";
            d.tmpro.text = $"d = {flipperManager.flippers[0].CalcProbThisExtremeIfAlternative()}";
            if (i == 0) {
                p.transform.localPosition = new Vector3(-7, 4.5f, 30);
                p.ScaleUpFromZero();
                d.transform.localPosition = new Vector3(-7, 3.0f, 30);
                d.ScaleUpFromZero();
            }
        }
        yield return new WaitForSeconds(2);
        // StopWaiting();
    }
    protected virtual IEnumerator MoreFlippers() {
        //Move Camera
        // p.Disappear();
        // d.Disappear();
        yield return new WaitForSeconds(1);

        CoinFlipper toSkip = flipperManager.flippers[0];
        flipperManager.AddFlippers(9);
        flipperManager.AddFlippers(10, headsRate: 0.75f);
        flipperManager.flippers.Shuffle();
        flipperManager.flippers.Remove(toSkip);
        flipperManager.flippers.Insert(0, toSkip);
        flipperManager.ArrangeAsGrid(32, 32, duration: 1f);
        camRig.ZoomTo(140, duration: 1);
        yield return new WaitForSeconds(1);
        flipperManager.ShowFlippers(skip: 1);
        yield return new WaitForSeconds(1);

        // Test!
        yield return flipperManager.testFlippers(26, 18);
        // yield return flipperManager.testFlippers(6, 4);
        yield return new WaitForSeconds(1);
        flipperManager.WrapUp();

        // yield return categorize;

    }
    IEnumerator categorize() {
        PrimerObject tpContainer = new GameObject().MakePrimerObject();
        PrimerObject tnContainer = new GameObject().MakePrimerObject();
        PrimerObject fpContainer = new GameObject().MakePrimerObject();
        PrimerObject fnContainer = new GameObject().MakePrimerObject();
        tpContainer.name = "True Positives";
        tnContainer.name = "True Negatives";
        fpContainer.name = "False Positives";
        fnContainer.name = "False Negatives";
        List<Vector3> tpPositions = Helpers.CalculateGridPositions(flipperManager.truePositives.Count, spacing: 3, plane: "xy");
        List<Vector3> tnPositions = Helpers.CalculateGridPositions(flipperManager.trueNegatives.Count, spacing: 3, plane: "xy");
        List<Vector3> fpPositions = Helpers.CalculateGridPositions(flipperManager.falsePositives.Count, spacing: 3, plane: "xy");
        List<Vector3> fnPositions = Helpers.CalculateGridPositions(flipperManager.falseNegatives.Count, spacing: 3, plane: "xy");
        tpContainer.MoveTo(new Vector3(-15, 10, 0));
        tnContainer.MoveTo(new Vector3(15, -10, 0));
        fpContainer.MoveTo(new Vector3(-15, -10, 0));
        fnContainer.MoveTo(new Vector3(15, 10, 0));
        camRig.RotateTo(Quaternion.identity);
        for (int i = 0; i < flipperManager.truePositives.Count; i++) {
            CoinFlipper f = flipperManager.truePositives[i];
            f.FormOfBlob();
            f.transform.parent = tpContainer.transform;
            f.MoveTo(tpPositions[i]);
        }
        for (int i = 0; i < flipperManager.trueNegatives.Count; i++) {
            CoinFlipper f = flipperManager.trueNegatives[i];
            f.FormOfBlob();
            f.transform.parent = tnContainer.transform;
            f.MoveTo(tnPositions[i]);
        }
        for (int i = 0; i < flipperManager.falsePositives.Count; i++) {
            CoinFlipper f = flipperManager.falsePositives[i];
            f.FormOfBlob();
            f.transform.parent = fpContainer.transform;
            f.MoveTo(fpPositions[i]);
        }
        for (int i = 0; i < flipperManager.falseNegatives.Count; i++) {
            CoinFlipper f = flipperManager.falseNegatives[i];
            f.FormOfBlob();
            f.transform.parent = fnContainer.transform;
            f.MoveTo(fnPositions[i]);
        }
        yield return new WaitForSeconds(1);
    }

    IEnumerator disappear() {
        foreach (CoinFlipper flipper in flipperManager.flippers) {
        flipper.display.transform.parent = null;
            foreach (PrimerObject coin in flipper.displayCoins) {
                coin.Disappear();
            }
            flipper.flipperCharacter.Disappear();
            flipper.floor.Disappear();
        }
        yield return null;
    }
    
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(0, 0f, Appear);
        // new SceneBlock(2, ProbabilityTest, flexible: true);
        new SceneBlock(3, MoreFlippers);
        new SceneBlock(4, disappear);
    }
}
