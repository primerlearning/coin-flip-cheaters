﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

public class GraphTestScene : Director
{
    // float camAngle = 17;
    // Graph graph;

    // protected override void Awake() {
    //     base.Awake();
    // }
    // protected override void Start() {
    //     camRig.cam.transform.localPosition = new Vector3(0, 0, -15);
    //     camRig.transform.localPosition = new Vector3(0, 1, 0);
    //     camRig.transform.localRotation = Quaternion.Euler(camAngle, 0, 0);

    //     base.Start();
    // }

    // //Define event actions
    // protected virtual IEnumerator Appear() {
    //     int numFlips = 1;
    //     float probability = 0.75f;
    //     SetUpGraph(numFlips);
    //     graph.ScaleUpFromZero();
    //     yield return new WaitForSeconds(1);
    //     graph.barData.AnimateBars(BinomialDistribution(numFlips, probability));

    //     // for (int i = 2; i < 5; i++) {
    //     //     yield return new WaitForSeconds(2);
    //     //     numFlips = i;
    //     //     UpdateGraph(numFlips, probability);
    //     // }
    //     numFlips = 10;
    //     UpdateGraph(numFlips, probability);

    //     yield return new WaitForSeconds(2);

    //     PrimerShapesLine line = Resources.Load("PrimerShapesLinePrefab", typeof(PrimerShapesLine)) as PrimerShapesLine;
    //     // primerArrowPrefab = Resources.Load("arrow", typeof(PrimerArrow)) as PrimerArrow;
    //     line = Instantiate(line);
    //     line.transform.parent = graph.transform;
    //     line.ScaleUpFromZero();
    //     yield return new WaitForSeconds(2);
    //     line.MovePoints(new Vector3(1, 1, 0), new Vector3(2, 2, 0));
 
    //     yield return new WaitForSeconds(3000);
    //     yield return null;
    // }
    // void SetUpGraph(int numFlips) {
    //     graph = Instantiate(primerGraphPrefab);
    //     graph.transform.localPosition = Vector3.zero;
    //     graph.Initialize(
    //         manualTicMode: true,
    //         xTicStep: 1,
    //         xAxisLength: 0.5f * (numFlips + 1),
    //         xMax: numFlips + 1.5f,
    //         manualTicsX: BarDataManager.GenerateIntegerCategories(numFlips + 1),

    //         yTicStep: 1,
    //         yMax: 1,
    //         zHidden: true,
    //         yAxisLength: 1,
    //         arrows: "positive",
    //         xAxisLabelPos: "along",
    //         xAxisLabelString: "Number of Heads",
    //         yAxisLabelString: "Probability"
    //     );
    //     List<Color> colors = new List<Color>() {
    //         PrimerColor.Purple,
    //         PrimerColor.Blue,
    //         PrimerColor.Green,
    //         PrimerColor.Yellow,
    //         PrimerColor.Orange,
    //         PrimerColor.Red
    //     };
    //     BarDataManager bd = graph.AddBarData();
    //     graph.barData.SetColors(colors);
    //     graph.SetIntrinsicScale();
    //     graph.transform.localScale = Vector3.zero;
    // }
    // void UpdateGraph(int num, float prob, float duration = 1) {
    //     StartCoroutine(updateGraph(num, prob, duration));
    // }
    // IEnumerator updateGraph(int num, float prob, float duration) {
    //     graph.manualTicsX = BarDataManager.GenerateIntegerCategories(num + 1);
    //     // graph.xAxisLength += 1;
    //     graph.ChangeRangeX(graph.xMin, num + 1.5f, newXLength: 0.5f * (num + 1), duration: duration / 2);
    //     yield return new WaitForSeconds(duration / 2);

    //     List<float> newData = BinomialDistribution(num, prob);
    //     float newYMax = newData.Max();
    //     graph.AutoTicStep(y: true);
    //     graph.ChangeRangeY(graph.yMin, newYMax, duration: duration / 2);
    //     graph.barData.AnimateBars(newData, duration: duration / 2);
    // }
    // List<float> BinomialDistribution(int maxNum, float probability) {
    //     List<float> dist = new List<float>();
    //     for (int i = 0; i <= maxNum; i++) {
    //         dist.Add((float)Helpers.Binomial(maxNum, i, probability));
    //     }
    //     return dist;
    // }
    // //Construct schedule
    // protected override void DefineSchedule() {
    //     new SceneBlock(0, 0f, Appear);
    // }
}