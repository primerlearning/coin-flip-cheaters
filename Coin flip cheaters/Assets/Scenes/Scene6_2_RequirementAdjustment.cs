﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Scene6_2_RequirementAdjustment : Director
{
    // PrimerObject graphParent = null;
    Graph graph1;
    Graph graph2;
    PrimerShapesLine testThresholdLine;
    PrimerShapesLine testThresholdLine2;
    PrimerText tps = null;
    PrimerText fps = null;
    PrimerText tns = null;
    PrimerText fns = null;
    PrimerText tpsNum = null;
    PrimerText fpsNum = null;
    PrimerText tnsNum = null;
    PrimerText fnsNum = null;
    PrimerArrow a1 = null;
    PrimerArrow a2 = null;
    PrimerArrow a3 = null;
    PrimerArrow a4 = null;
    PrimerText fpGraphCheck;
    PrimerText tpGraphCheck;
    PrimerText fpGraphX;
    PrimerText tpGraphX;
    PrimerText fpGoalCheck;
    PrimerText tpGoalCheck;
    PrimerText fpGoalX;
    PrimerText tpGoalX;
    [SerializeField] VideoClip calculationClip = null;
    bool[] goalBools;
    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        base.Start();
        Vector3 startingCamSwivelOrigin = new Vector3(0, 0, 0);
        Quaternion startingCamSwivel = Quaternion.Euler(0, 0, 0);
        float startingCamDistance = 7.5f;
        camRig.SwivelOrigin = startingCamSwivelOrigin;
        camRig.Swivel = startingCamSwivel;
        camRig.Distance = startingCamDistance;

    }
    //Define event actions
    IEnumerator GraphAppear() {
        // Time.timeScale = 50;
        // graphParent = new GameObject().MakePrimerObject();
        // graphParent.gameObject.name = "graphs";
        // graphParent.transform.position = new Vector3(1.3f, 0, -2.9f);
        // graphParent.transform.rotation = camRig.transform.rotation;

        Dictionary<float, string> xTics = new Dictionary<float, string> () {
            {1, 0.ToString()},
            {6, 5.ToString()},
            {11, 10.ToString()},
            {16, 15.ToString()},
            {21, 20.ToString()},
            // {26, 25.ToString()},
        };
        graph1 = Instantiate(primerGraphPrefab);
        graph1.Initialize(
            xAxisLength: 5f,
            xMax: 24.7f,
            manualTicMode: true,
            manualTicsX: xTics,
            yMax: 0.2f,
            yTicStep: 0.1f,
            yAxisLength: 2f,
            thickness: 1.5f,
            zHidden: true,
            xAxisLabelPos: "along",
            xAxisLabelString: "Heads",
            yAxisLabelPos: "along",
            yAxisLabelString: "Fair player probability"
        );
        // graph1.transform.parent = graphParent.transform;
        graph1.transform.localPosition = new Vector3(-5.2f, -2.8f, 0);
        graph1.transform.localRotation = Quaternion.identity;
        graph1.axesHandler.yAxisLabel.transform.localRotation = Quaternion.Euler(0, 0, 90);
        graph1.axesHandler.yAxisLabel.transform.localPosition = new Vector3(-0.75f, 0.6f, 0);
        graph1.ScaleUpFromZero();

        graph2 = Instantiate(primerGraphPrefab);
        graph2.Initialize(
            xAxisLength: 5f,
            xMax: 24.7f,
            manualTicMode: true,
            manualTicsX: xTics,
            yMax: 0.2f,
            yTicStep: 0.1f,
            yAxisLength: 2f,
            thickness: 1.5f,
            zHidden: true,
            xAxisLabelPos: "along",
            xAxisLabelString: "Heads",
            yAxisLabelPos: "along",
            yAxisLabelString: "Cheater probability"
        );
        // graph2.transform.parent = graphParent.transform;
        graph2.transform.localPosition = new Vector3(1.45f, -2.8f, 0);
        graph2.transform.localRotation = Quaternion.identity;
        graph2.axesHandler.yAxisLabel.transform.localRotation = Quaternion.Euler(0, 0, 90);
        graph2.axesHandler.yAxisLabel.transform.localPosition = new Vector3(-0.75f, 0.6f, 0);
        graph2.ScaleUpFromZero();

        yield return new WaitForSeconds(0.5f);
        List<float> probabilities1 = new List<float>();
        List<Color> graph1Colors = new List<Color>();
        List<float> probabilities2 = new List<float>();
        List<Color> graph2Colors = new List<Color>();
        int testThreshold = 16;
        int numFlips = 23;
        for (int i = 0; i <= numFlips; i++) {
            probabilities1.Add((float) Helpers.Binomial(numFlips, i, 0.5f));
            probabilities2.Add((float) Helpers.Binomial(numFlips, i, 0.75f));
            if (i < testThreshold) {
                graph1Colors.Add(PrimerColor.Blue);
                graph2Colors.Add(PrimerColor.Red / Mathf.Sqrt(2));
            }
            else {
                graph1Colors.Add(PrimerColor.Blue / Mathf.Sqrt(2));
                graph2Colors.Add(PrimerColor.Red);
            }
        }
        BarDataManager bd = graph1.AddBarData();
        // bd.showBarNumbers = true;
        graph1.barData.barWidth = 0.9f;
        // graph1.barData.defaultColor = PrimerColor.Blue;
        // graph1.barData.SetColors(PrimerColor.Blue);

        graph1.barData.AnimateBars(probabilities1);
        graph1.barData.SetBarColors(graph1Colors);
        
        BarDataManager bd2 = graph2.AddBarData();
        // bd2.showBarNumbers = true;
        graph2.barData.barWidth = 0.9f;
        // graph2.barData.defaultColor = PrimerColor.Red;

        graph2.barData.AnimateBars(probabilities2);
        graph2.barData.SetBarColors(graph2Colors);

        // yield return new WaitForSeconds(0.5f);

        testThresholdLine = new GameObject().AddComponent<PrimerShapesLine>();
        testThresholdLine.line.Thickness = 0.0f;
        testThresholdLine.line.Dashed = true;
        testThresholdLine.line.DashSpace = Shapes.DashSpace.Meters;
        testThresholdLine.line.DashSize = 0.15f;
        testThresholdLine.line.DashType = Shapes.DashType.Rounded;
        testThresholdLine.line.Geometry = Shapes.LineGeometry.Flat2D;
        testThresholdLine.animatedOffsetSpeed = 0.001f;
        testThresholdLine.transform.parent = graph1.transform;
        testThresholdLine.transform.localPosition = Vector3.zero;
        testThresholdLine.transform.localScale = Vector3.one;
        testThresholdLine.Start = graph1.CoordinateToPosition(16.5f, 0, 0);
        testThresholdLine.End = graph1.CoordinateToPosition(16.5f, 0.2f, 0);

        testThresholdLine.AnimateValue<float>("Thickness", 0.05f);

        testThresholdLine2 = new GameObject().AddComponent<PrimerShapesLine>();
        testThresholdLine2.line.Thickness = 0.0f;
        testThresholdLine2.line.Dashed = true;
        testThresholdLine2.line.DashSpace = Shapes.DashSpace.Meters;
        testThresholdLine2.line.DashSize = 0.15f;
        testThresholdLine2.line.DashType = Shapes.DashType.Rounded;
        testThresholdLine2.line.Geometry = Shapes.LineGeometry.Flat2D;
        testThresholdLine2.animatedOffsetSpeed = 0.001f;
        testThresholdLine2.transform.parent = graph2.transform;
        testThresholdLine2.transform.localPosition = Vector3.zero;
        testThresholdLine2.transform.localScale = Vector3.one;
        testThresholdLine2.Start = graph2.CoordinateToPosition(16.5f, 0, 0);
        testThresholdLine2.End = graph2.CoordinateToPosition(16.5f, 0.2f, 0);

        testThresholdLine2.AnimateValue<float>("Thickness", 0.05f);

        yield return new WaitForSeconds(0.1f);
        //True negative
        tns = Instantiate(primerTextPrefab);
        tns.transform.SetParent(graph1.transform);
        tns.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.9f, 0);
        tns.SetIntrinsicScale(0.15f);
        tns.tmpro.text = "True negatives";
        tns.ScaleUpFromZero();
        yield return new WaitForSeconds(0.1f);
        tnsNum = Instantiate(primerTextPrefab);
        tnsNum.transform.SetParent(graph1.transform);
        tnsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.65f, 0);
        tnsNum.SetIntrinsicScale(0.15f);
        float num = graph1.barData.values.Take(16).Sum() * 100;
        float roundedNum = (float) Math.Round(num, 1);
        string numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        tnsNum.tmpro.text = numString;
        tnsNum.ScaleUpFromZero();
        // a1 = Instantiate(primerArrowPrefab);
        // a1.transform.parent = graph1.transform;
        // a1.SetIntrinsicScale(0.5f);
        // a1.SetFromTo(
        //     new Vector3(0.6f, 1, 0),
        //     new Vector3(0.75f, 0.65f, 0)
        // );
        // a1.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a1.transform.right));
        // a1.ScaleUpFromZero();
        yield return new WaitForSeconds(0.1f);
        Debug.Log(tns.transform.position);

        //False positive
        fps = Instantiate(primerTextPrefab);
        fps.transform.SetParent(graph1.transform);
        fps.tmpro.text = "False positives";
        // fps.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(2.8f, 1.35f, 0);
        fps.AnchoredPosition = new Vector3(2.8f, 1.9f, 0);
        fps.SetIntrinsicScale(0.15f);
        fps.ScaleUpFromZero();
        yield return new WaitForSeconds(0.1f);
        fpsNum = Instantiate(primerTextPrefab);
        fpsNum.transform.SetParent(graph1.transform);
        fpsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(2.8f, 1.65f, 0);
        fpsNum.SetIntrinsicScale(0.15f);
        num = graph1.barData.values.Skip(16).Sum() * 100;
        roundedNum = (float) Math.Round(num, 1);
        numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        fpsNum.tmpro.text = numString;
        fpsNum.ScaleUpFromZero();
        // a2 = Instantiate(primerArrowPrefab);
        // a2.transform.parent = graph1.transform;
        // a2.SetIntrinsicScale(0.5f);
        // a2.SetFromTo(
        //     new Vector3(2.7f, 1.17f, 0),
        //     new Vector3(2.18f, 0.79f, 0)
        // );
        // a2.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a2.transform.right));
        // a2.ScaleUpFromZero();

        yield return new WaitForSeconds(0.1f);
        //False negative
        fns = Instantiate(primerTextPrefab);
        fns.transform.SetParent(graph2.transform);
        fns.tmpro.text = "False negatives";
        fns.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.9f, 0);
        fns.SetIntrinsicScale(0.15f);
        fns.ScaleUpFromZero();
        yield return new WaitForSeconds(0.1f);
        fnsNum = Instantiate(primerTextPrefab);
        fnsNum.transform.SetParent(graph2.transform);
        fnsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.65f, 0);
        fnsNum.SetIntrinsicScale(0.15f);
        num = graph2.barData.values.Take(16).Sum() * 100;
        roundedNum = (float) Math.Round(num, 1);
        numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        fnsNum.tmpro.text = numString;
        fnsNum.ScaleUpFromZero();
        // a4 = Instantiate(primerArrowPrefab);
        // a4.transform.parent = graph2.transform;
        // a4.SetIntrinsicScale(0.5f);
        // a4.SetFromTo(
        //     new Vector3(0.65f, 0.6f, 0),
        //     new Vector3(0.76f, 0.25f, 0)
        // );
        // a4.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a4.transform.right));
        // a4.ScaleUpFromZero();
        // yield return new WaitForSeconds(0.5f);
        // List<Color> cheaterColors = new List<Color>() {
        //     PrimerColor.Red / Mathf.Sqrt(2),
        //     PrimerColor.Red / Mathf.Sqrt(2),
        //     PrimerColor.Red / Mathf.Sqrt(2),
        //     PrimerColor.Red / Mathf.Sqrt(2),
        //     PrimerColor.Red / Mathf.Sqrt(2),
        //     PrimerColor.Red
        // };
        // graph2.barData.AnimateColors(cheaterColors, duration: 0.5f);

        yield return new WaitForSeconds(0.1f);
        //True positive
        tps = Instantiate(primerTextPrefab);
        tps.transform.SetParent(graph2.transform);
        tps.tmpro.text = "True positives";
        tps.AnchoredPosition = new Vector3(2.8f, 1.9f, 0);
        tps.SetIntrinsicScale(0.15f);
        tps.ScaleUpFromZero();
        yield return new WaitForSeconds(0.1f);
        tpsNum = Instantiate(primerTextPrefab);
        tpsNum.transform.SetParent(graph2.transform);
        tpsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(2.8f, 1.65f, 0);
        tpsNum.SetIntrinsicScale(0.15f);
        num = graph2.barData.values.Skip(16).Sum() * 100;
        roundedNum = (float) Math.Round(num, 1);
        numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        tpsNum.tmpro.text = numString;
        tpsNum.ScaleUpFromZero();

        // a3 = Instantiate(primerArrowPrefab);
        // a3.transform.parent = graph2.transform;
        // a3.SetIntrinsicScale(0.5f);
        // a3.SetFromTo(
        //     new Vector3(1.6f, 1.44f, 0),
        //     new Vector3(1.94f, 1.04f, 0)
        // );
        // a3.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a3.transform.right));
        // a3.ScaleUpFromZero();
        // yield return new WaitForSeconds(5);

        // a1.ScaleDownToZero();
        // a2.ScaleDownToZero();
        // a3.ScaleDownToZero();
        // a4.ScaleDownToZero();
        // tns.ScaleDownToZero();
        // fps.ScaleDownToZero();
        // fns.ScaleDownToZero();
        // tps.ScaleDownToZero();
        yield return new WaitForSeconds(0.1f);
        // Prep graph checks and x's
        fpGraphCheck = Instantiate(primerCheckPrefab);
        fpGraphCheck.SetColor(PrimerColor.Green);
        fpGraphCheck.transform.SetParent(graph1.transform);
        fpGraphCheck.transform.localPosition = new Vector3(2f, 1.95f, 0);
        fpGraphCheck.SetIntrinsicScale(0.5f);
        tpGraphCheck = Instantiate(primerCheckPrefab);
        tpGraphCheck.SetColor(PrimerColor.Green);
        tpGraphCheck.transform.SetParent(graph2.transform);
        tpGraphCheck.transform.localPosition = new Vector3(2.05f, 1.95f, 0);
        tpGraphCheck.SetIntrinsicScale(0.5f);
        fpGraphX = Instantiate(primerXPrefab);
        fpGraphX.SetColor(PrimerColor.Red);
        fpGraphX.transform.SetParent(graph1.transform);
        fpGraphX.transform.localPosition = new Vector3(2f, 1.884f, 0);
        fpGraphX.SetIntrinsicScale(0.35f);
        tpGraphX = Instantiate(primerXPrefab);
        tpGraphX.SetColor(PrimerColor.Red);
        tpGraphX.transform.SetParent(graph2.transform);
        tpGraphX.transform.localPosition = new Vector3(2.05f, 1.884f, 0);
        tpGraphX.SetIntrinsicScale(0.35f);

        // Prep goal checks and x's
        fpGoalCheck = Instantiate(primerCheckPrefab);
        fpGoalCheck.SetColor(PrimerColor.Green);
        fpGoalCheck.transform.localPosition = new Vector3(2f, 2.95f, 0);
        fpGoalCheck.SetIntrinsicScale(0.75f);
        tpGoalCheck = Instantiate(primerCheckPrefab);
        tpGoalCheck.SetColor(PrimerColor.Green);
        tpGoalCheck.transform.localPosition = new Vector3(2.05f, 1.95f, 0);
        tpGoalCheck.SetIntrinsicScale(0.75f);
        fpGoalX = Instantiate(primerXPrefab);
        fpGoalX.SetColor(PrimerColor.Red);
        fpGoalX.transform.localPosition = new Vector3(2f, 2.884f, 0);
        fpGoalX.SetIntrinsicScale(0.35f * 1.5f);
        tpGoalX = Instantiate(primerXPrefab);
        tpGoalX.SetColor(PrimerColor.Red);
        tpGoalX.transform.localPosition = new Vector3(2.05f, 1.884f, 0);
        tpGoalX.SetIntrinsicScale(0.35f * 1.5f);

        tpGraphCheck.transform.localScale = Vector3.zero;
        fpGraphCheck.transform.localScale = Vector3.zero;
        tpGraphX.transform.localScale = Vector3.zero;
        fpGraphX.transform.localScale = Vector3.zero;
        tpGoalCheck.transform.localScale = Vector3.zero;
        fpGoalCheck.transform.localScale = Vector3.zero;
        tpGoalX.transform.localScale = Vector3.zero;
        fpGoalX.transform.localScale = Vector3.zero;

        fpGraphX.ScaleUpFromZero();
        fpGoalX.ScaleUpFromZero();
        yield return new WaitForSeconds(0.1f);
        tpGraphCheck.ScaleUpFromZero();
        tpGoalCheck.ScaleUpFromZero();
        yield return new WaitForSeconds(0.1f);

        yield return new WaitUntilSceneTime(16, 59.5f);
        int newThreshold = 18; // Really 18
        SetHeadsThreshold(newThreshold);
        goalBools = CheckGoals(newThreshold, 23, 0.01f, 0.8f);
        StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        UpdateStats(newThreshold);
    }
    IEnumerator RequirementAdjustment1() {
        // yield return new WaitUntilSceneTime(17, 21);
        // int numFlips = 40;
        // int newThreshold = 28; // Really 18
        // yield return SetNumFlips(numFlips);
        // SetHeadsThreshold(newThreshold);
        // goalBools = CheckGoals(newThreshold, numFlips, 0.01f, 0.8f);
        // StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        // UpdateStats(newThreshold);

        // yield return new WaitUntilSceneTime(17, 32);
        // numFlips = 80;
        // newThreshold = 51; // Really 18
        // yield return SetNumFlips(numFlips);
        // SetHeadsThreshold(newThreshold);
        // goalBools = CheckGoals(newThreshold, numFlips, 0.01f, 0.99f);
        // StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        // UpdateStats(newThreshold);
        // 40, 28
        // 80, 51

        // Look for a test that satisfies 1% false positive and 80% true positive
        // Time.timeScale = 50;
        bool searching = true;
        for (int numFlips = 24; numFlips <= 500; numFlips++) {
            yield return SetNumFlips(numFlips);
            // With the bars updated, iterate through line positions (heads thresholds)
            for (int i = numFlips - 1; i >= 0; i--) {
                SetHeadsThreshold(i + 1);
                goalBools = CheckGoals(i + 1, numFlips, 0.01f, 0.8f);
                StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
                UpdateStats(i + 1);

                yield return new WaitForSeconds(0.5f);
                if (!goalBools[0]) {
                    testThresholdLine.AnimateThickness(0);
                    testThresholdLine2.AnimateThickness(0);
                    break;
                }
                if (goalBools[1]) {
                    searching = false;
                    Debug.Log(numFlips);
                    Debug.Log(i + 1);
                    break;
                }
            }
            if (!searching) {
                break;
            }
        }
        Time.timeScale = 1;
        yield return new WaitForSeconds(1);
    }
    IEnumerator RequirementAdjustment2() {
        // Time.timeScale = 50;
        // Look for a test that satisfies 1% false positive and 99% true positive
        bool searching = true;
        for (int numFlips = 40; numFlips <= 500; numFlips++) {
            yield return SetNumFlips(numFlips);
            // With the bars updated, iterate through line positions (heads thresholds)
            for (int i = numFlips - 1; i >= 0; i--) {
                SetHeadsThreshold(i + 1);
                goalBools = CheckGoals(i + 1, numFlips, 0.01f, 0.99f);
                StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
                UpdateStats(i + 1);

                yield return new WaitForSeconds(0.5f);
                if (!goalBools[0]) {
                    testThresholdLine.AnimateThickness(0);
                    testThresholdLine2.AnimateThickness(0);
                    break;
                }
                if (goalBools[1]) {
                    searching = false;
                    Debug.Log(numFlips);
                    Debug.Log(i + 1);
                    break;
                }
            }
            if (!searching) {
                break;
            }
        }
        Time.timeScale = 1;
        yield return new WaitForSeconds(1);
    }
    IEnumerator GoBack() {
        Time.timeScale = 1;
        // yield return new WaitForSeconds(3);
        yield return SetNumFlips(23);
        SetHeadsThreshold(16);
        goalBools = CheckGoals(16, 23, 0.05f, 0.8f);
        StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        UpdateStats(16);
        yield return new WaitForSeconds(0.5f);
        yield return new WaitUntilSceneTime(17, 56);
    }
    IEnumerator SetNumFlips(int numFlips, bool editTicSteps = true, float duration = 0.5f) {
        Dictionary<float, string> xTics = new Dictionary<float, string> () {};
        List<float> probabilities1 = new List<float>();
        List<float> probabilities2 = new List<float>();
        // Recalculate tics
        int xTicStep = 1;
        while (numFlips / xTicStep > 6) {
            if (xTicStep.ToString()[0] == '1') {
                xTicStep *= 2;
            }
            else if (xTicStep.ToString()[0] == '2') {
                xTicStep = Mathf.RoundToInt(xTicStep * 2.5f);
            }
            else if (xTicStep.ToString()[0] == '5') {
                xTicStep *= 2;
            }
        }
        for (int i = 0; i <= numFlips; i++) {
            if (i % xTicStep == 0) {
                xTics.Add(i+1, i.ToString());
            }
            probabilities1.Add((float) Helpers.Binomial(numFlips, i, 0.5f));
            probabilities2.Add((float) Helpers.Binomial(numFlips, i, 0.75f));
        }
        // Change range
        float oldMax = graph1.xMax;
        float newMax = numFlips + 1.7f;
        if (editTicSteps) {
            graph1.manualTicsX = xTics;
        }
        graph1.ChangeRangeX(0, newMax, duration: duration);
        testThresholdLine.MovePoints(
            new Vector3(testThresholdLine.Start.x * oldMax / newMax, testThresholdLine.Start.y, testThresholdLine.Start.z),
                new Vector3(testThresholdLine.End.x * oldMax / newMax, testThresholdLine.End.y, testThresholdLine.End.z),
            duration: duration
        );
        testThresholdLine2.MovePoints(
            new Vector3(testThresholdLine2.Start.x * oldMax / newMax, testThresholdLine2.Start.y, testThresholdLine2.Start.z),
            new Vector3(testThresholdLine2.End.x * oldMax / newMax, testThresholdLine2.End.y, testThresholdLine2.End.z),
            duration: duration
        );
        graph2.manualTicsX = xTics;
        graph2.ChangeRangeX(0, newMax, duration: duration);
        // yield return new WaitForSeconds(duration);
        graph1.barData.defaultColor = PrimerColor.Blue / Mathf.Sqrt(2);
        graph1.barData.AnimateBars(probabilities1, duration: duration);
        graph2.barData.AnimateBars(probabilities2, duration: duration);
        yield return new WaitForSeconds(duration);
    }
    void SetHeadsThreshold(int minHeads) {
        // Set bar colors
        for (int j = 0; j < graph1.barData.BarCount; j++) {
            if (j <= minHeads - 1) {
                graph1.barData.AnimateColor(j, PrimerColor.Blue);
                graph2.barData.AnimateColor(j, PrimerColor.Red / Mathf.Sqrt(2));
            }
            if (j > minHeads - 1) {
                graph1.barData.AnimateColor(j, PrimerColor.Blue / Mathf.Sqrt(2));
                graph2.barData.AnimateColor(j, PrimerColor.Red);
            }
        }
        // Move line
        if (testThresholdLine.Thickness > 0) {
            testThresholdLine.MovePoints(
                graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0),
                graph1.CoordinateToPosition(minHeads + 0.5f, 0.2f, 0)
            );
        }
        else {
            testThresholdLine.Start = graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0);
            testThresholdLine.End = graph1.CoordinateToPosition(minHeads + 0.5f, 0.2f, 0);
            testThresholdLine.AnimateThickness(0.05f);
        }
        if (testThresholdLine2.Thickness > 0) {
            testThresholdLine2.MovePoints(
                graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0),
                graph1.CoordinateToPosition(minHeads + 0.5f, 0.2f, 0)
            );
        }
        else {
            testThresholdLine2.Start = graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0);
            testThresholdLine2.End = graph1.CoordinateToPosition(minHeads + 0.5f, 0.2f, 0);
            testThresholdLine2.AnimateThickness(0.05f);
        }
    }
    bool[] CheckGoals(int minHeads, int numFlips, float maxFPRate, float minTPRate) {
        bool[] goalBools = new bool[2];

        float num1 = graph1.barData.values.Take(minHeads).Sum();
        float num2 = graph2.barData.values.Take(minHeads).Sum();

        if (num1 > 1 - maxFPRate) {goalBools[0] = true;}
        if (num2 < 1 - minTPRate) {goalBools[1] = true;}

        return goalBools;
    }
    IEnumerator updateChecksAndExes(bool goal1Met, bool goal2Met) {
        // Get rid of checkmarks and x marks that aren't true anymore
        if (!goal1Met) {
            fpGoalCheck.ScaleDownToZero();
            fpGraphCheck.ScaleDownToZero();
        }
        if (goal1Met) {
            fpGoalX.ScaleDownToZero();
            fpGraphX.ScaleDownToZero();
        }
        if (!goal2Met) {
            tpGoalCheck.ScaleDownToZero();
            tpGraphCheck.ScaleDownToZero();
        }
        if (goal2Met) {
            tpGoalX.ScaleDownToZero();
            tpGraphX.ScaleDownToZero();
        }
        yield return new WaitForSeconds(0.0f);
        if (!goal1Met && fpGoalX.transform.localScale == Vector3.zero) { 
            fpGraphX.ScaleUpFromZero();
            fpGoalX.ScaleUpFromZero();
        }
        if (goal1Met && fpGoalCheck.transform.localScale == Vector3.zero) {
            fpGraphCheck.ScaleUpFromZero();
            fpGoalCheck.ScaleUpFromZero();
        }
        if (!goal2Met && tpGoalX.transform.localScale == Vector3.zero) {
            tpGraphX.ScaleUpFromZero();
            tpGoalX.ScaleUpFromZero();
        }
        if (goal2Met && tpGoalCheck.transform.localScale == Vector3.zero) {
            tpGraphCheck.ScaleUpFromZero();
            tpGoalCheck.ScaleUpFromZero();
        }
    }
    void UpdateStats(int minHeads) {
        // Number text updates
        float num1 = graph1.barData.values.Take(minHeads).Sum() * 100;
        float num2 = graph2.barData.values.Take(minHeads).Sum() * 100;
        // Fair player numbers (true negative and false positive)
        float roundedNum = (float) Math.Round(num1, 1);
        string numString = roundedNum.ToString() + "%";
        if (num1 != roundedNum) { numString = "~" + numString; }
        tnsNum.tmpro.text = numString;

        numString = Math.Round(100 - num1, 1).ToString() + "%";
        if (num1 != roundedNum) { numString = "~" + numString; }
        fpsNum.tmpro.text = numString;

        // Cheater numbers (false negative and true positive)
        roundedNum = (float) Math.Round(num2, 1);
        numString = Math.Round(100 - num2, 1).ToString() + "%";
        if (num2 != roundedNum) { numString = "~" + numString; }
        tpsNum.tmpro.text = numString;

        numString = roundedNum.ToString() + "%";
        if (num2 != roundedNum) { numString = "~" + numString; }
        fnsNum.tmpro.text = numString;
    }
    IEnumerator Stall() {
        yield return new WaitForSeconds(10000000000);
    }
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(16, 55.75f, GraphAppear, flexible: false);
        new SceneBlock(17, 19.5f, RequirementAdjustment1, flexible: true);
        new SceneBlock(17, 31f, RequirementAdjustment2, flexible: true);
        new SceneBlock(17, 45.7f, GoBack, flexible: false);
        // new SceneBlock(5, UpTo26, flexible: true);
        // new SceneBlock(10000000f, Stall);
    }
}
