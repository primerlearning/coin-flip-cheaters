﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class PatreonThanks : Director
{
    static float totalDuration = 18;
    static float height = 5;
    static float spatialSpacing = 0.75f;
    // static float scrollDuration = 10;
    static int nameCount = 14;
    static float scrollDuration = totalDuration / (1 + nameCount * spatialSpacing / height);
    static float speed = height / scrollDuration;
    float appearTimeSpacing = spatialSpacing / speed;

    // total duration = scroll duration + appear time spacing * num names
    // appear time spacing = spatial spacing / speed
    // speed = height / scroll duration

    // total duration = height / speed + spatial spacing / speed * num names

    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        base.Start();
    }
    //Define event actions
    protected virtual IEnumerator Appear() {
        List<string> names = new List<string>() {
            // "Anthony Eufemio",
            // "Jon Mundle",
            // "Spline",
            // "Zachariah Richard Fournier",
            // "Vladimir Duchenchuk",
            // "Roy & BreAnna Steves",
            // "Shayn Osborn",
            // "Jeremy",
            // "Guguke",
            // "Anders Fjeldvær",
            // "Luc Cedric R.",
            // "Erik Broeders",
            // "Kairui Wang"

            "Anthony Eufemio",
            "Jon Mundle",
            "Zach Richard Fournier",
            "Vladimir Duchenchuk",
            "Spline",
            "Christian Gruber",
            "Kairui Wang",
            "Luc Cedric R.",
            "Erik Broeders",
            "Anders Fjeldvær",
            "Jeremy",
            "Guguke",
            "Shayn Osborn",
            "Brian Cloutier",
        };
        foreach (string name in names) {
            StartCoroutine(scrollName(name));
            yield return new WaitForSeconds(appearTimeSpacing);
        }
        // float scrollDuration = totalDuration - appearTimeSpacing * (names.Count - 1);
        yield return new WaitForSeconds(scrollDuration);
    }
    IEnumerator scrollName(string name) {
        Vector3 startPos = Vector3.down * height;
        Vector3 endPos = Vector3.up * height;

        PrimerText nameObj = Instantiate(primerTextPrefab);
        nameObj.tmpro.text = name;
        nameObj.transform.position = startPos;
        nameObj.ScaleUpFromZero();
        nameObj.MoveTo(endPos, duration: scrollDuration, ease: EaseMode.None);
        yield return new WaitForSeconds(scrollDuration - 0.5f);
        nameObj.Disappear();
        Debug.Log(Time.time);
    }
    IEnumerator Words() {
        PrimerText nameObj = Instantiate(primerTextPrefab);
        nameObj.tmpro.alignment = TextAlignmentOptions.Left;
        // nameObj.tmpro.text = "primerlearning.org";
        // nameObj.tmpro.text = "Where to play:";
        nameObj.tmpro.text = "\"Coin Flip Cheaters\"\n   on Google Play";
        // nameObj.tmpro.text = "Google Play Store";
        // nameObj.transform.position = startPos;
        nameObj.ScaleUpFromZero();
        yield return new WaitForSeconds(1);
        nameObj.ScaleDownToZero();
        yield return new WaitForSeconds(1);
    }
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(1f, Appear);
        // new SceneBlock(1f, Words);
    }
}
