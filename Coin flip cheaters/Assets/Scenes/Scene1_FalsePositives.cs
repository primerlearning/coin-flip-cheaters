﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Scene1_FalsePositives : Director
{
    [SerializeField] CoinFlipper flipperPrefab;
    [SerializeField] CoinFlipSimManager manager;
    public BinaryTree treePrefab;
    public List<BTNode> NodePrefabs;
    CoinFlipper flipper;
    BinaryTree tree;

    public VideoClip probabilitiesClip;
    ReferenceScreen probabilitiesReferenceScreen;

    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        // Time.timeScale = 10;
        manager.Initialize(flipperPrefab);
        base.Start();
    }
    //Define event actions
    IEnumerator Appear() {
        // camRig.transform.position = new Vector3(0, 1, -10);
        Vector3 startingCamSwivelOrigin = new Vector3(0, 1, 0);
        Quaternion startingCamSwivel = Quaternion.Euler(13, 0, 0);
        float startingCamDistance = 7;

        camRig.SwivelOrigin = startingCamSwivelOrigin;
        camRig.Swivel = startingCamSwivel;
        camRig.Distance = startingCamDistance;
        //BinaryTree tree = Instantiate(treePrefab);
        //tree.root = coin(tree);
        //tree.root.SetRight(coin(tree, false).SetLeft(coin(tree)))
        //    .SetLeft(coin(tree, false).SetRight(coin(tree)));

        //PrimerArrow arrowPrefab = Resources.Load<PrimerArrow>("arrowPrefab");
        //PrimerArrow arrow = Instantiate(arrowPrefab);
        //arrow.SetFromTo(new Vector3(5, 5, 5), new Vector3(-2, -2, -2), 0.75f);
        //arrow.ScaleUpFromZero();

        // Show a blob that will flip one coin at a time
        flipper = manager.AddFlipper();
        flipper.resultsDisplayMode = ResultsDisplayMode.Individual;
        flipper.Appear();
        ((PrimerBlob)flipper.flipperCharacter).SetColor(PrimerColor.Blue);
        flipper.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(2, 16);
        flipper.individualOffset = new Vector3(0f, 2.75f, -2.7f);
        flipper.FlipAndRecord(outcome: 1, delay: 2.5f);
        yield return flipper.waitUntilCoinIsStopped();

        // Camera zooms in on blob
        Debug.Log(voiceOverReference.isPlaying);
        Vector3 finalOrigin = new Vector3(-1.83f, 1.46f, 1.81f);
        Quaternion finalRot = Quaternion.Euler(13, 315, 0f);
        float finalZoom = 1;
        yield return new WaitUntilSceneTime(2, 21.5f);
        yield return new WaitUntilSceneTime(2, 38-15.9f);
        camRig.MoveCenterTo(Vector3.Lerp(startingCamSwivelOrigin, finalOrigin, 0.25f));
        camRig.ZoomTo(Mathf.Lerp(startingCamDistance, finalZoom, 0.25f));
        camRig.SwivelTo(Quaternion.Slerp(startingCamSwivel, finalRot, 0.25f));

        // camRig.RotateTo(Quaternion.Euler(new Vector3(4.5f, -35.5f, 0f));
        // camRig.MoveTo(new Vector3(-0.5f, 1.25f, 0.25f));
        yield return new WaitUntilSceneTime(2, 38.5f-15.9f);
        camRig.MoveCenterTo(Vector3.Lerp(startingCamSwivelOrigin, finalOrigin, 0.5f));
        camRig.ZoomTo(Mathf.Lerp(startingCamDistance, finalZoom, 0.5f));
        camRig.SwivelTo(Quaternion.Slerp(startingCamSwivel, finalRot, 0.5f));

        yield return new WaitUntilSceneTime(2, 39.1f-15.9f);
        ((PrimerBlob)flipper.flipperCharacter).StartLookingAt(camRig.transform, correctionVector: Vector3.down * 0.7f);
        camRig.MoveCenterTo(Vector3.Lerp(startingCamSwivelOrigin, finalOrigin, 0.75f));
        camRig.ZoomTo(Mathf.Lerp(startingCamDistance, finalZoom, 0.75f));
        camRig.SwivelTo(Quaternion.Slerp(startingCamSwivel, finalRot, 0.75f));

        yield return new WaitUntilSceneTime(2, 39.7f-15.9f);
        camRig.MoveCenterTo(finalOrigin);
        camRig.SwivelTo(finalRot);
        camRig.ZoomTo(finalZoom);
        ((PrimerBlob)flipper.flipperCharacter).BigEyes();
        yield return new WaitForSeconds(2);
        ((PrimerBlob)flipper.flipperCharacter).NeutralEyes();
        yield return new WaitForSeconds(1);
        ((PrimerBlob)flipper.flipperCharacter).StopLooking();
        camRig.MoveCenterTo(startingCamSwivelOrigin);
        camRig.SwivelTo(startingCamSwivel);
        camRig.ZoomTo(startingCamDistance);


        // Show tree
        yield return new WaitUntilSceneTime(2, 36);
        // probabilitiesReferenceScreen = Instantiate(Resources.Load<ReferenceScreen>("Reference screen prefab"));
        // // probabilitiesReferenceScreen.transform.rotation = camRig.transform.rotation;
        // probabilitiesReferenceScreen.transform.parent = camRig.transform;
        // probabilitiesReferenceScreen.transform.localScale = Vector3.one * 20;
        // probabilitiesReferenceScreen.screen.clip = probabilitiesClip;
        // probabilitiesReferenceScreen.Play();
        // probabilitiesReferenceScreen = MakeReferenceScreen(clip: probabilitiesClip);
        flipper.MoveTo(new Vector3(-3.8f, 0, 0), duration: 1);
        camRig.ZoomTo(10, duration: 1);
        yield return new WaitForSeconds(0.5f);
        yield return new WaitForSeconds(1.5f);
        // probabilitiesReferenceScreen = MakeReferenceScreen(probabilitiesClip);
        // probabilitiesReferenceScreen.transform.localScale = Vector3.one * 70;
        // probabilitiesReferenceScreen.transform.localPosition = new Vector3(20, -10, 40);
        // probabilitiesReferenceScreen.Play();

        // Second flip
        yield return new WaitUntilSceneTime(2, 42);
        flipper.individualOffset = new Vector3(0f, 2.75f, -2.7f);
        flipper.FlipAndRecord(outcome: 1);

        // Tree adjustment and highlighting
        yield return new WaitUntilSceneTime(2, 55);
        flipper.MoveTo(new Vector3(-3.8f, -2, 0), duration: 1);
        tree = BinaryTree.DecisionTree(5, treePrefab, NodePrefabs[1], NodePrefabs[0]);
        tree.transform.position += new Vector3(0, 10.8f, 23.2f);
        tree.root.buffer = 1.75f;
        tree.PlaceNodes(1);
        tree.DisplayToLevel(2);
        yield return new WaitForSeconds(1.0f);
        foreach (BTNode node in tree.GetLevel(1)) { ShowBranchLabel(node); }

        yield return new WaitForSeconds(1.5f);
        tree.root.buffer = 2;
        tree.UpdateNodes(2);
        // tree.MoveTo(new Vector3(0, 10.8f, 23.2f));
        yield return new WaitForSeconds(0.5f);
        tree.DisplayLevel(2);
        yield return new WaitForSeconds(0.5f);
        // tree.UpdateLabels(tree.root);
        foreach (BTNode node in tree.GetLevel(2)) { ShowBranchLabel(node); }
        yield return new WaitUntilSceneTime(3, 2);

        tree.AnimatePathColor(tree.root.Right.Right, PrimerColor.Blue);
        yield return new WaitUntilSceneTime(3, 37);
        tree.AnimatePathColor(tree.root.Right.Right, Color.white);
        yield return new WaitUntilSceneTime(3, 38);
        flipper.FlipAndRecord(outcome: 1);
        yield return new WaitUntilSceneTime(3, 40);
        tree.root.buffer = 2.75f;
        tree.ScaleTo(0.69f);
        tree.UpdateNodes(3);
        yield return new WaitForSeconds(0.5f);
        tree.DisplayLevel(3);
        yield return new WaitForSeconds(0.5f);
        foreach (BTNode node in tree.GetLevel(3)) { ShowBranchLabel(node); }
        tree.AnimatePathColor(tree.root.Right.Right.Right, PrimerColor.Blue);

        yield return new WaitUntilSceneTime(3, 52);
        tree.MoveBy(Vector3.left * 13, duration: 1);
        //yield return new WaitForSeconds(2);
        //tree.SetPathColor(tree.root.Right.Right.Right, Color.red);
        //yield return new WaitForSeconds(1.5f);
        //tree.SetPathColor(tree.root.Right.Right.Right, Color.white);
        yield return new WaitUntilSceneTime(4, 19);
        // voiceOverReference.Pause();
        Time.timeScale = 1;
        flipper.FlipAndRecord(outcome: 1);
        yield return new WaitForSeconds(2f);
        tree.MoveBy(Vector3.right * 13, duration: 1);
        tree.root.buffer = 4.5f;
        tree.ScaleTo(0.5f, duration: 1);
        yield return new WaitForSeconds(0.5f);
        tree.UpdateNodes(4);
        yield return new WaitForSeconds(0.5f);
        tree.DisplayLevel(4);
        yield return new WaitForSeconds(0.5f);
        foreach (BTNode node in tree.GetLevel(4)) { ShowBranchLabel(node); }
        tree.AnimatePathColor(tree.root.Right.Right.Right.Right, PrimerColor.Blue);

        yield return new WaitForSeconds(1.5f);
        //yield return new WaitForSeconds(2);
        //tree.SetPathColor(tree.root.Right.Right.Right.Right, Color.red);
        //yield return new WaitForSeconds(1.5f);
        //tree.SetPathColor(tree.root.Right.Right.Right.Right, Color.white);
        // yield return new WaitUntilSceneTime(4, 22);
        flipper.FlipAndRecord(outcome: 1);
        yield return new WaitForSeconds(2f);
        tree.root.buffer = 5f;
        tree.ScaleTo(0.4f);
        tree.UpdateNodes(5);
        yield return new WaitForSeconds(0.5f);
        tree.DisplayLevel(5);
        yield return new WaitForSeconds(0.5f);
        foreach (BTNode node in tree.GetLevel(5)) { ShowBranchLabel(node); }
        tree.AnimatePathColor(tree.root.Right.Right.Right.Right.Right, PrimerColor.Blue);

        // Show text of a test rule
        // Show a tree diagram (need to make a code class for this)
        // After two flips, show the multiplication of probabilities
        // Bring in the goal list again, add the 5% threshold
        // (May be a separate scene that slides in/out during video editing)

        // Continue until five flips
    }
    IEnumerator End() {
        flipper.Disappear();
        yield return new WaitForSeconds(1f);
        tree.Hide();
        yield return new WaitForSeconds(1f);
    }
    void ShowBranchLabel(BTNode node) {
        if (node != tree.root) {
            node.Label(primerTextPrefab);
            node.PlaceLabel();
            node.label.tmpro.text = "50%";
            node.label.gameObject.SetActive(true);
            node.label.ScaleUpFromZero();
        }
    }
    IEnumerator Test() {
        yield return new WaitForSeconds(1);
    }
    
    //Construct schedule
    protected override void DefineSchedule() {
        // new SceneBlock(1, Test);
        new SceneBlock(2 * 60 + 14, Appear);
        new SceneBlock(4, 37.5f, End);
    }
}
