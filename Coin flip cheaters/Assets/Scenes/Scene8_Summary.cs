using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

public class Scene8_Summary: Director
{
    [SerializeField] PrimerBlob blobPrefab;
    [SerializeField] GameObject coin;

    protected override void Awake()
    {
        base.Awake();
    }
    protected override void Start()
    {
        base.Start();
    }
    //Define event actions
    protected virtual IEnumerator Appear()
    {
        // camRig.SwivelOrigin = new Vector3(0.9f, 1.5f, 0);
        // camRig.Distance = 12;
        // Time.timeScale = 20;
        // camRig.SwivelOrigin = new Vector3(-7, 1.5f, 0);
        camRig.SwivelOrigin = new Vector3(-7.5f, 1.5f, 0);
        camRig.Distance = 10;
        yield return new WaitUntilSceneTime(20, 40.5f);

        PrimerText qText = Instantiate(primerTextPrefab);
        qText.tmpro.text = "Question";
        // qText.transform.parent = camRig.transform;
        qText.transform.position = new Vector3(-7.5f, 5.5f, 0);
        qText.transform.localScale = Vector3.one * 1.25f;
        qText.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(20, 43.5f);
        PrimerBlob blob = Instantiate(blobPrefab);
        blob.SetIntrinsicScale(2);
        blob.ScaleUpFromZero();
        blob.transform.position = new Vector3(-7.5f, -1.77f, 0.5f);
        blob.transform.localRotation = Quaternion.Euler(0, 180, 0);
        blob.SetColor(PrimerColor.Red);
        blob.AddAccessory(accessoryType: AccessoryType.starShades);

        yield return new WaitUntilSceneTime(20, 44.5f);
        PrimerText cheaterText = Instantiate(primerTextPrefab);
        cheaterText.tmpro.text = "Cheater?";
        cheaterText.transform.position = new Vector3(-7.5f, 2.83f, 0);
        cheaterText.SetIntrinsicScale(Vector3.one * 2 / 3);
        cheaterText.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(20, 48);
        // Move Camera
        camRig.MoveCenterTo(new Vector3(-3.25f, 1.5f, 0), duration: 1);
        yield return new WaitUntilSceneTime(20, 48);
        // "Models"
        PrimerText mText = Instantiate(primerTextPrefab);
        mText.tmpro.text = "Models";
        // mText.transform.parent = camRig.transform;
        mText.transform.position = new Vector3(0f, 5.5f, 0);
        mText.transform.localScale = Vector3.one * 1.25f;
        mText.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(20, 52);

        PrimerArrow yesArrow = Instantiate(primerArrowPrefab);
        yesArrow.SetIntrinsicScale(2);
        yesArrow.SetFromTo(new Vector3(-5.5f+0.5f-0.33f, 0.85f, 0), new Vector3(-3.25f+0.5f-0.33f, 1.6f, 0));
        PrimerText yesText = Instantiate(primerTextPrefab);
        yesText.tmpro.text = "Yes";
        yesText.tmpro.fontSize = 8;
        yesText.transform.position = new Vector3(-4.24f-0.33f, 1.74f, 0);
        yesText.SetIntrinsicScale(0.75f);

        yesArrow.ScaleUpFromZero();
        yesText.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);

        PrimerText yesStats = Instantiate(primerTextPrefab);
        yesStats.tmpro.text = "Cheater Model:\nP(   ) = 75%";
        yesStats.tmpro.fontSize = 8;
        GameObject yesCoin = Instantiate(coin);
        yesCoin.transform.position = Vector3.zero;
        yesCoin.GetComponent<Rigidbody>().isKinematic = true;
        yesCoin.transform.parent = yesStats.transform;
        yesStats.transform.position = new Vector3(0, 1.67f, 0);
        yesCoin.transform.localPosition = new Vector3(-1.043f, -0.613f, 0);
        yesCoin.transform.rotation = Quaternion.Euler(Vector3.up * 180);
        yesCoin.transform.localScale = Vector3.one * 0.25f;

        yesStats.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(20, 54);

        PrimerArrow noArrow = Instantiate(primerArrowPrefab);
        noArrow.SetIntrinsicScale(2);
        noArrow.SetFromTo(new Vector3(-5.5f+0.5f-0.33f, -0.85f, 0), new Vector3(-3.25f+0.5f-0.33f, -1.6f, 0));
        PrimerText noText = Instantiate(primerTextPrefab);
        noText.tmpro.text = "No";
        noText.tmpro.fontSize = 8;
        noText.transform.position = new Vector3(-4.24f-0.33f, -1.74f, 0);
        noText.SetIntrinsicScale(0.75f);

        noArrow.ScaleUpFromZero();
        noText.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        PrimerText noStats = Instantiate(primerTextPrefab);
        noStats.tmpro.text = "Fair Model:\nP(   ) = 50%";
        noStats.tmpro.fontSize = 8;
        GameObject noCoin = Instantiate(coin);
        noCoin.transform.position = Vector3.zero;
        noCoin.GetComponent<Rigidbody>().isKinematic = true;
        noCoin.transform.parent = noStats.transform;
        noStats.transform.position = new Vector3(0, -1.67f, 0);
        noCoin.transform.localPosition = new Vector3(-1.043f, -0.613f, 0);
        noCoin.transform.rotation = Quaternion.Euler(Vector3.up * 180);
        noCoin.transform.localScale = Vector3.one * 0.25f;

        noStats.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(20, 55);
        camRig.MoveCenterTo(new Vector3(0.9f, 1.5f, 0), duration: 1);
        camRig.ZoomTo(12, duration: 1);
        yield return new WaitUntilSceneTime(20, 56);
        PrimerText tText = Instantiate(primerTextPrefab);
        tText.tmpro.text = "Test";
        // tText.transform.parent = camRig.transform;
        tText.transform.position = new Vector3(7.5f, 5.5f, 0);
        tText.transform.localScale = Vector3.one * 1.25f;
        tText.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(20, 57);
        PrimerBracket bracket = Instantiate(primerBracketPrefab);
        bracket.SetPoints(
            new Vector3(4.5f, 0, 0),
            new Vector3(3.2f, 2.7f, 0),
            new Vector3(3.2f, -2.7f, 0)
        );
        bracket.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(20, 57.5f);
        PrimerText testText = Instantiate(primerTextPrefab);
        testText.tmpro.alignment = TextAlignmentOptions.Left;
        testText.tmpro.text = "Test goals:\n  Catch >80% of cheaters\n  Accuse <5% of fair players";
        testText.tmpro.fontSize = 10;
        testText.SetIntrinsicScale(0.5f);
        testText.transform.position = new Vector3(6.5f, 1.7f, 0);
        testText.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(20, 58);
        PrimerText testText2 = Instantiate(primerTextPrefab);
        testText2.tmpro.alignment = TextAlignmentOptions.Left;
        testText2.tmpro.text = "Test rule:\n  Flip 23 times. If 16 or more\n  heads, accuse of cheating.";
        testText2.tmpro.fontSize = 10;
        testText2.SetIntrinsicScale(0.5f);
        testText2.transform.position = new Vector3(6.5f, -1.7f, 0);
        testText2.ScaleUpFromZero();
        // PrimerArrow yesTestArrow = Instantiate(primerArrowPrefab);
        // yesTestArrow.transform.localScale = Vector3.one * 2;
        // yesTestArrow.SetFromTo(new Vector3(3.15f, 1.6f, 0), new Vector3(4.75f, 0.85f, 0));
        // PrimerArrow noTestArrow = Instantiate(primerArrowPrefab);
        // noTestArrow.transform.localScale = Vector3.one * 2;
        // noTestArrow.SetFromTo(new Vector3(3.15f, -1.6f, 0), new Vector3(4.75f, -0.85f, 0));
        // yesTestArrow.ScaleUpFromZero();
        // noTestArrow.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(21, 12);
        camRig.ZoomTo(20, duration: 1);
        camRig.MoveCenterTo(new Vector3(-5.75f, -3.6f, 0), duration: 1);
        yield return new WaitUntilSceneTime(21, 14.5f);
        PrimerText fht = Instantiate(primerTextPrefab);
        // fht.tmpro.alignment = TextAlignmentOptions.Left;
        fht.tmpro.text = "Frequentist\nHypothesis\nTesting";
        // fht.tmpro.fontSize = 10;
        fht.SetIntrinsicScale(1.5f);
        fht.transform.position = new Vector3(-18.25f, 1.75f, 0);
        fht.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(21, 18.0f);
        PrimerText bht = Instantiate(primerTextPrefab);
        // bht.tmpro.alignment = TextAlignmentOptions.Left;
        bht.tmpro.text = "Bayesian\nHypothesis\nTesting";
        // bht.tmpro.fontSize = 10;
        bht.SetIntrinsicScale(1.5f);
        bht.transform.position = new Vector3(-18.25f, -8.7f, 0);
        bht.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(21, 19.5f);
        Time.timeScale = 1;
        PrimerBlob detective = Instantiate(blobPrefab);
        detective.transform.localScale = Vector3.one * 2;
        detective.SetColor(PrimerColor.Blue);
        detective.AddAccessory(AccessoryType.detectiveHat);
        // detective.AddAccessory(AccessoryType.monocle);
        detective.transform.position = new Vector3(17f, -10.8f, 0);
        detective.transform.rotation = Quaternion.Euler(0, 270, 0);
        detective.WalkTo(new Vector3(0, -10.8f, 0), duration: 2.0f, ease: EaseMode.SmoothOut);
        yield return new WaitForSeconds(2);
        detective.RotateTo(Quaternion.Euler(0, 180, 0));
        // detective.StartLookingAt(camRig.transform);
        yield return new WaitUntilSceneTime(21, 22.5f);
        // detective.StopLooking();
        PauseVoiceOverAtTime(21,23);
        detective.Wave(duration: 100);

        List<PrimerObject> dispers = new List<PrimerObject>() {
            testText, testText2, bracket, tText,
            noStats, noText, noArrow,
            yesStats, yesText, yesArrow, mText,
            blob, cheaterText, qText,
            fht, bht
        };
        foreach (PrimerObject p in dispers) {
            p.Disappear();
            yield return new WaitForSeconds(1f/16);
        };
        yield return new WaitForSeconds(7f/8);
        detective.Disappear();
        yield return new WaitForSeconds(0.5f);
    }

    //Construct schedule
    protected override void DefineSchedule()
    {
        new SceneBlock(20, 38.5f, Appear);
    }
}
