﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Scene6_CombinedGroupTest : Director
{
    // bool simple = true;
    bool simple = false;
    CoinFlipSimManager simManager = null;
    [SerializeField] PrimerObject floorPrefab = null;
    PrimerObject fnContainer;
    PrimerObject tpContainer;
    PrimerObject tnContainer;
    PrimerObject fpContainer;

    // vars for text
    PrimerText truth = null;
    PrimerText tFair = null;
    PrimerText tCheater = null;
    PrimerText labels = null;
    PrimerText lFair = null;
    PrimerText lCheater = null;
    PrimerText vocabTime = null;
    PrimerText positives = null;
    PrimerText negatives = null;
    PrimerText tps = null;
    PrimerText fps = null;
    PrimerText tns = null;
    PrimerText fns = null;

    PrimerText goalsPreview = null;
    PrimerText calculationPreview = null;
    PrimerText checkMark = null;
    PrimerText xMark = null;

    PrimerArrow a1 = null;
    PrimerArrow a2 = null;
    PrimerArrow a3 = null;
    PrimerArrow a4 = null;

    PrimerObject parent1000;
    PrimerObject table;
    public VideoClip ruleClip;
    ReferenceScreen ruleReferenceScreen;
    protected override void Awake() {
        Time.timeScale = 1;
        base.Awake();
        primerArrowPrefab = Resources.Load<PrimerArrow>("arrowPrefab");

    }
    protected override void Start() {
        base.Start();
        // When designing the coins, I set gravity to 2x for some reason (the reason is laziness!)
        Physics.gravity = new Vector3(0, -9.81f * 2, 0);

        camRig.Distance = 7;
        camRig.SwivelOrigin = Vector3.up;
        camRig.Swivel = Quaternion.Euler(17, 0, 0);
        
        truth = Instantiate(primerTextPrefab);
        truth.tmpro.text = "The truth";
        truth.transform.position = new Vector3(-56, 7.5f, 0);
        truth.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        truth.SetIntrinsicScale(3);
        truth.transform.localScale = Vector3.zero;
        tFair = Instantiate(primerTextPrefab);
        tFair.tmpro.text = "Fair";
        tFair.transform.position = new Vector3(-41.5f, 16.7f, 0);
        tFair.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        tFair.SetIntrinsicScale(2);
        tFair.transform.localScale = Vector3.zero;
        tCheater = Instantiate(primerTextPrefab);
        tCheater.tmpro.text = "Cheater";
        tCheater.transform.position = new Vector3(-47f, 0f, 0);
        tCheater.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        tCheater.SetIntrinsicScale(2);
        tCheater.transform.localScale = Vector3.zero;

        labels = Instantiate(primerTextPrefab);
        labels.tmpro.text = "Test results";
        labels.transform.position = new Vector3(0, 31, 0);
        labels.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        labels.SetIntrinsicScale(3);
        labels.transform.localScale = Vector3.zero;
        lFair = Instantiate(primerTextPrefab);
        lFair.tmpro.text = "Fair";
        lFair.transform.position = new Vector3(-13.6f, 24.3f, 0);
        lFair.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        lFair.SetIntrinsicScale(2);
        lFair.transform.localScale = Vector3.zero;
        lCheater = Instantiate(primerTextPrefab);
        lCheater.tmpro.text = "Cheater";
        lCheater.transform.position = new Vector3(13.6f, 24.3f, 0);
        lCheater.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        lCheater.SetIntrinsicScale(2);
        lCheater.transform.localScale = Vector3.zero;

        // Initialize manager
        simManager = new GameObject().AddComponent<CoinFlipSimManager>();
        simManager.gameObject.name = "Coin Sim Mananger";
        // simManager.savingNewParameters = true;
        // simManager.savingNewParameters = false;
        // simManager.refiningOldParameters = true;
        simManager.Initialize(Resources.Load<CoinFlipper>("FlipperPrefabBlob"));
        int seed = 4;
        // 472522218 (maybe)
        // 22671359
        simManager.SimSeed = seed;
        sceneRandom = new System.Random(seed);
        // // This is for purely visual stuff so messing with that doesn't affect rng state
        // // Doesn't matter unless you set it in the Simulator class, but it's created here.
        sceneRandom2 = new System.Random(seed);
    }
    //Define event actions
    IEnumerator Appear() {
        // ruleReferenceScreen = MakeReferenceScreen(ruleClip);
        // ruleReferenceScreen.transform.localScale = Vector3.one * 400;
        // ruleReferenceScreen.transform.localPosition = new Vector3(0, 150, 337);
        // ruleReferenceScreen.Play();
        // Show 1 blob
        // CoinFlipper firstFlipper = simManager.AddFlipper();

        // firstFlipper.flipperCharacter = Instantiate(firstFlipper.flipperCharacterPrefab);
        // firstFlipper.flipperCharacter.SetColor(PrimerColor.Blue);
        // simManager.ShowFlippers();
        // yield return new WaitUntilSceneTime(4, 48f);

        // Add 999 other blobs
        int maxBlobs = 1000;
        // if (simple) { maxBlobs = 500; }
        simManager.AddFlippers(maxBlobs / 2);
        simManager.AddFlippers(maxBlobs / 2, headsRate: 0.75f);
        // Hax to position the existing one within the list
        // CoinFlipper toSkip = simManager.flippers[0];
        // simManager.flippers.Remove(toSkip);
        // simManager.flippers.Insert(maxBlobs - 20, toSkip);
        simManager.ArrangeAsGrid(maxBlobs / 40, 40, gridOriginIndexX: 19.5f, gridOriginIndexY: maxBlobs / 40 - 1);

        yield return new WaitForSeconds(1);
        foreach (CoinFlipper flipper in simManager.flippers) {
            if (flipper.flipperCharacter == null) {
                flipper.flipperCharacter = Instantiate(flipper.flipperCharacterPrefab);
                flipper.flipperCharacter.transform.localScale = Vector3.zero;
                ((PrimerBlob)flipper.flipperCharacter).RandomizeColorAndAccessory();
            }
        }

        parent1000 = new GameObject().MakePrimerObject();
        parent1000.transform.position = Vector3.zero;
        foreach (CoinFlipper c in simManager.flippers) {
            c.transform.parent = parent1000.transform;
        }

        // camRig.ZoomTo(75, duration: 15);
        // camRig.SwivelTo(Quaternion.Euler(32, 0, 0), duration: 15);
        // camRig.MoveCenterTo(Vector3.up * 42.5f, duration: 15);
        float mainDur = 5;
        camRig.ZoomTo(95, duration: mainDur);
        float ydelay = 1;
        yield return new WaitForSeconds(ydelay);
        simManager.ShowFlippersCascade(CascadeDelegate);
        camRig.SwivelTo(Quaternion.Euler(45, 0, 0), duration: mainDur - ydelay);
        camRig.MoveCenterTo(Vector3.up * 90f, duration: mainDur - ydelay);
        parent1000.MoveTo(new Vector3(0, -8, 0), duration: mainDur - ydelay);
        // camRig.ZoomTo(20, duration: 5);
        yield return new WaitForSeconds(5f);
        PauseVoiceOverAtTime(16, 29);
        // Have them each do five flips
        int flipNum = 23;
        int threshold = 16;
        if (simple) {
            flipNum = 1;
            threshold = 1;
        }
        yield return StartCoroutine(simManager.testFlippers(flipNum, threshold));
        simManager.WrapUp();
    }
    bool CascadeDelegate(Vector3 pos, float startTime) {
        float speed = 12;
        float timeSoFar = Time.time - startTime;
        // if ((Mathf.Abs(pos.x) + Mathf.Abs(pos.z)) < timeSoFar * speed) {
        if (Mathf.Sqrt(pos.x * pos.x + pos.z * pos.z) < timeSoFar * timeSoFar * speed) {
            return true;
        }
        return false;
    }
    IEnumerator Flips() {
        // Have them each do five flips
        int flipNum = 5;
        if (simple) { flipNum = 1; }
        yield return StartCoroutine(simManager.testFlippers(flipNum, flipNum));
    }
    IEnumerator MakeTable() {
        // Put the blobs in the appropriate category lists
        simManager.CategorizeTestedFlippers();
        // parent1000.RotateTo(Quaternion.Euler(10, 0, 0), duration: 1);

        // Create planes to contain the blobs
        Vector3 upBack = new Vector3(0, 12, 13);
        float xDist = 17;
        tnContainer = Instantiate(floorPrefab);
        tnContainer.transform.position = Vector3.left * xDist + upBack;
        tnContainer.SetIntrinsicScale(15);
        tnContainer.transform.localScale = Vector3.zero;

        fpContainer = Instantiate(floorPrefab);
        fpContainer.transform.position = Vector3.right * xDist + upBack;
        fpContainer.SetIntrinsicScale(15);
        fpContainer.transform.localScale = Vector3.zero;

        fnContainer = Instantiate(floorPrefab);
        fnContainer.transform.position = Vector3.left * xDist;
        fnContainer.SetIntrinsicScale(15);
        fnContainer.transform.localScale = Vector3.zero;

        tpContainer = Instantiate(floorPrefab);
        tpContainer.transform.position = Vector3.right * xDist;
        tpContainer.SetIntrinsicScale(15);
        tpContainer.transform.localScale = Vector3.zero;
        
        camRig.SwivelTo(Quaternion.Euler(17, 0, 0), duration: 1);
        camRig.MoveCenterTo(new Vector3(0, 1, 0), duration: 1);
        camRig.ZoomTo(69, duration: 1.5f);
        PrimerObject nextParent1000 = new GameObject().MakePrimerObject();
        nextParent1000.gameObject.name = "JudgementStaging";
        nextParent1000.transform.position = new Vector3(0, -8, -28f);
        foreach (CoinFlipper c in simManager.flippers) {
            c.FormOfBlob();
            c.transform.parent = nextParent1000.transform;
            c.MoveTo(Helpers.GenerateNonCollidingPositionOnPlane(
                    tnContainer.GetComponentsInChildren<Transform>().ToList(), 
                    rangeX: 30, 
                    rangeZ: 7.5f, 
                    maxDistance: 0.1f, 
                    maxTries: 30),
                duration: 1
            );
        }
        yield return new WaitForSeconds(0.5f);
        tnContainer.ScaleUpFromZero();
        fpContainer.ScaleUpFromZero();
        fnContainer.ScaleUpFromZero();
        tpContainer.ScaleUpFromZero();
        truth.ScaleUpFromZero();
        tFair.ScaleUpFromZero();
        tCheater.ScaleUpFromZero();
        labels.ScaleUpFromZero();
        lFair.ScaleUpFromZero();
        lCheater.ScaleUpFromZero();
        yield return new WaitForSeconds(1.5f);

        // Move blobs from staging area to categories
        // if(true) bc I want to collapse it
        if (true) {
            // yield return new WaitUntilSceneTime(5, 18);
            PoissonDiscPointSet tnSet = new PoissonDiscPointSet(0.1f, Vector2.one * 1.6f, overflowMode: PoissonDiscOverflowMode.Force);
            tnSet.AddPoints(simManager.trueNegatives.Count);
            List<Vector2> tnCentered = tnSet.GetCenteredPoints();
            for (int i = 0; i < simManager.trueNegatives.Count; i++)
            {
                CoinFlipper c = simManager.trueNegatives[i];
                c.transform.parent = tnContainer.transform;
                Vector2 point2D = tnCentered[i];
                Vector3 point3D = new Vector3(point2D.x, 0, point2D.y);
                c.MoveTo(point3D);
                ((PrimerBlob) c.flipperCharacter).animator.SetTrigger("MouthSmile");
            }
            // yield return new WaitUntilSceneTime(5, 20.5f);
            PoissonDiscPointSet fpSet = new PoissonDiscPointSet(0.1f, Vector2.one * 1.6f, overflowMode: PoissonDiscOverflowMode.Force);
            fpSet.AddPoints(simManager.falsePositives.Count);
            List<Vector2> fpCentered = fpSet.GetCenteredPoints();
            for (int i = 0; i < simManager.falsePositives.Count; i++)
            {
                CoinFlipper c = simManager.falsePositives[i];
                c.transform.parent = fpContainer.transform;
                Vector2 point2D = fpCentered[i];
                Vector3 point3D = new Vector3(point2D.x, 0, point2D.y);
                c.MoveTo(point3D);
                ((PrimerBlob) c.flipperCharacter).animator.SetBool("Sad", true);
            }
            // yield return new WaitUntilSceneTime(5, 23.0f);
            PoissonDiscPointSet fnSet = new PoissonDiscPointSet(0.1f, Vector2.one * 1.6f, overflowMode: PoissonDiscOverflowMode.Force);
            fnSet.AddPoints(simManager.falseNegatives.Count);
            List<Vector2> fnCentered = fnSet.GetCenteredPoints();
            for (int i = 0; i < simManager.falseNegatives.Count; i++)
            {
                CoinFlipper c = simManager.falseNegatives[i];
                c.transform.parent = fnContainer.transform;
                Vector2 point2D = fnCentered[i];
                Vector3 point3D = new Vector3(point2D.x, 0, point2D.y);
                c.MoveTo(point3D);
                // ((PrimerBlob) c.flipperCharacter).animator.SetBool("Victory", true);
                ((PrimerBlob) c.flipperCharacter).EvilPose();
            }
            // yield return new WaitUntilSceneTime(5, 25.0f);
            PoissonDiscPointSet tpSet = new PoissonDiscPointSet(0.1f, Vector2.one * 1.6f, overflowMode: PoissonDiscOverflowMode.Force);
            tpSet.AddPoints(simManager.truePositives.Count);
            List<Vector2> tpCentered = tpSet.GetCenteredPoints();
            for (int i = 0; i < simManager.truePositives.Count; i++)
            {
                CoinFlipper c = simManager.truePositives[i];
                c.transform.parent = tpContainer.transform;
                Vector2 point2D = tpCentered[i];
                Vector3 point3D = new Vector3(point2D.x, 0, point2D.y);
                c.MoveTo(point3D);
                ((PrimerBlob) c.flipperCharacter).animator.SetBool("Sad", true);
            }
            // yield return new WaitForSeconds(0.5f);
        }
    }
    IEnumerator CalculationPlaceholder() {
        calculationPreview = Instantiate(primerTextPrefab);
        calculationPreview.tmpro.text = $"{simManager.falsePositives.Count} of 500 = {(float)simManager.falsePositives.Count/5}% of\nfair players accused"; 
        calculationPreview.transform.position = new Vector3(45.8f, 14.6f, -3.6f);
        calculationPreview.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        calculationPreview.SetIntrinsicScale(2.25f);
        calculationPreview.ScaleUpFromZero();

        // yield return new WaitForSeconds(1);

        // goalsPreview = Instantiate(primerTextPrefab);
        // goalsPreview.tmpro.text = "Goal 1: Accuse fewer than 5% of players"; 
        // goalsPreview.transform.position = new Vector3(0, -34.5f, 0);
        // goalsPreview.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        // goalsPreview.SetIntrinsicScale(3);
        // goalsPreview.ScaleUpFromZero();

        yield return new WaitForSeconds(1);

        checkMark = Instantiate(primerCheckPrefab);
        checkMark.SetColor(PrimerColor.Green);
        checkMark.transform.parent = camRig.transform;
        checkMark.transform.localRotation = Quaternion.Euler(0, 180, 0);
        checkMark.transform.localPosition = new Vector3(-36, -26, 80);
        // checkMark.transform.rotation = Quaternion.Euler(-camRig.transform.rotation.eulerAngles.x, 180, 0);
        // checkMark.transform.position = new Vector3(-36, -34, 0);
        checkMark.SetIntrinsicScale(7);
        checkMark.ScaleUpFromZero();
        
        yield return new WaitUntilSceneTime(16, 37.5f);

        PrimerText calculationPreview2 = Instantiate(primerTextPrefab);
        calculationPreview2.tmpro.text = $"{simManager.truePositives.Count} of 500 = {(float)simManager.truePositives.Count/5}%\nof cheaters caught"; 
        // calculationPreview2.tmpro.text = "413 of 500 = 82.6%\nof cheaters caught"; 
        calculationPreview2.transform.position = new Vector3(45.8f, 3.2f, -7.1f);
        calculationPreview2.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        calculationPreview2.SetIntrinsicScale(2.25f);
        calculationPreview2.ScaleUpFromZero();

        yield return new WaitForSeconds(1);

        xMark = Instantiate(primerCheckPrefab);
        xMark.SetColor(PrimerColor.Green);
        xMark.transform.parent = camRig.transform;
        xMark.transform.localRotation = Quaternion.Euler(0, 180, 0);
        xMark.transform.localPosition = new Vector3(-36, -33, 80);
        xMark.SetIntrinsicScale(7);
        xMark.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(16, 53.5f);
        checkMark.ScaleDownToZero();
        yield return new WaitForSeconds(0.5f);
        checkMark = Instantiate(primerXPrefab);
        checkMark.SetColor(PrimerColor.Red);
        checkMark.transform.parent = camRig.transform;
        checkMark.transform.localRotation = Quaternion.Euler(0, 180, 0);
        checkMark.transform.localPosition = new Vector3(-36, -26, 80);
        checkMark.SetIntrinsicScale(7);
        checkMark.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(16, 55f);
    }
    IEnumerator flashAndWiggle(PrimerText pt, Color newColor, float angle, float period, float duration) {
        float startTime = Time.time;
        Color oldColor = pt.tmpro.color;
        Quaternion oldRotation = pt.transform.localRotation;
        Quaternion newRotation1 = oldRotation * Quaternion.Euler(0, 0, angle);
        Quaternion newRotation2 = oldRotation * Quaternion.Euler(0, 0, -angle);
        while (Time.time - startTime < duration) {
            pt.RotateTo(newRotation1, duration: period / 2);
            pt.ChangeColor(newColor, duration: period / 4); 
            yield return new WaitForSeconds(period / 4);
            pt.ChangeColor(oldColor, duration: period / 4);
            yield return new WaitForSeconds(period / 4);

            pt.RotateTo(newRotation2, duration: period / 2);
            pt.ChangeColor(newColor, duration: period / 4); 
            yield return new WaitForSeconds(period / 4);
            pt.ChangeColor(oldColor, duration: period / 4);
            yield return new WaitForSeconds(period / 4);
        }
        pt.RotateTo(oldRotation, duration: period / 4);
    }
    IEnumerator VocabTime() {
        vocabTime = Instantiate(primerTextPrefab);
        vocabTime.transform.parent = camRig.transform;
        vocabTime.tmpro.text = "Vocab Time";
        vocabTime.transform.localRotation = Quaternion.identity;
        vocabTime.transform.localPosition = new Vector3 (16.5f, 10, 20);
        vocabTime.ScaleUpFromZero();
        StartCoroutine(flashAndWiggle(vocabTime, PrimerColor.Orange, 5, 2, 200));

        yield return new WaitUntilSceneTime(6, 14);
        //Negative
        negatives = Instantiate(primerTextPrefab);
        negatives.tmpro.text = "(Negative)";
        negatives.transform.position = new Vector3(-13.6f, 22, 0);
        negatives.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        negatives.ScaleUpFromZero();
        negatives.TempColorChange(PrimerColor.Orange, duration: 4);

        yield return new WaitUntilSceneTime(6, 20);
        //Positive
        positives = Instantiate(primerTextPrefab);
        positives.tmpro.text = "(Positive)";
        positives.transform.position = new Vector3(13.6f, 22, 0);
        positives.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        positives.ScaleUpFromZero();
        positives.TempColorChange(PrimerColor.Orange, duration: 18.5f);


        //True negative
        yield return new WaitUntilSceneTime(6, 45.5f);
        tFair.TempColorChange(PrimerColor.Orange, duration: 12.5f);

        yield return new WaitUntilSceneTime(6, 47.5f);
        negatives.TempColorChange(PrimerColor.Orange, duration: 5.5f);
        lFair.TempColorChange(PrimerColor.Orange, duration: 5.5f);

        yield return new WaitUntilSceneTime(6, 51.2f);
        tns = Instantiate(primerTextPrefab);
        tns.tmpro.text = "True negatives";
        tns.transform.position = new Vector3(-40, 26, 0);
        tns.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        tns.SetIntrinsicScale(3);
        tns.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        a1 = Instantiate(primerArrowPrefab);
        a1.SetIntrinsicScale(8);
        a1.SetFromTo(
            new Vector3(-33, 22, 0),
            new Vector3(-24.7f, 16.8f, 0)
        );
        a1.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a1.transform.right));
        a1.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(6, 54f);
        positives.TempColorChange(PrimerColor.Orange, duration: 4f);
        lCheater.TempColorChange(PrimerColor.Orange, duration: 4f);

        yield return new WaitUntilSceneTime(6, 56.2f);
        //False positive
        fps = Instantiate(primerTextPrefab);
        fps.tmpro.text = "False positives";
        fps.transform.position = new Vector3(40, 26, 0);
        fps.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        fps.SetIntrinsicScale(3);
        fps.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        a2 = Instantiate(primerArrowPrefab);
        a2.SetIntrinsicScale(8);
        a2.SetFromTo(
            new Vector3(33, 22, 0),
            new Vector3(24.7f, 16.8f, 0)
        );
        a2.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a2.transform.right));
        a2.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(6, 58f);
        tCheater.TempColorChange(PrimerColor.Orange, duration: 13.5f);
        yield return new WaitUntilSceneTime(7, 0f);
        positives.TempColorChange(PrimerColor.Orange, duration: 5f);
        lCheater.TempColorChange(PrimerColor.Orange, duration: 5f);

        yield return new WaitUntilSceneTime(7, 3.2f);
        //True positive
        tps = Instantiate(primerTextPrefab);
        tps.tmpro.text = "True positives";
        tps.transform.position = new Vector3(53, -16, 0);
        tps.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        tps.SetIntrinsicScale(3);
        tps.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        a3 = Instantiate(primerArrowPrefab);
        a3.SetIntrinsicScale(8);
        a3.SetFromTo(
            new Vector3(49, -13, 0),
            new Vector3(38f, -9, 0)
        );
        a3.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a3.transform.right));
        a3.ScaleUpFromZero();
        
        yield return new WaitUntilSceneTime(7, 7f);
        negatives.TempColorChange(PrimerColor.Orange, duration: 4f);
        lFair.TempColorChange(PrimerColor.Orange, duration: 4f);

        yield return new WaitUntilSceneTime(7, 8.5f);
        //False negative
        fns = Instantiate(primerTextPrefab);
        fns.tmpro.text = "False negatives";
        fns.transform.position = new Vector3(-53, -16, 0);
        fns.transform.rotation = Quaternion.Euler(camRig.transform.rotation.eulerAngles.x, 0, 0);
        fns.SetIntrinsicScale(3);
        fns.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        a4 = Instantiate(primerArrowPrefab);
        a4.SetIntrinsicScale(8);
        a4.SetFromTo(
            new Vector3(-49, -13, 0),
            new Vector3(-38f, -9f, 0)
        );
        a4.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a4.transform.right));
        a4.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
    }
    IEnumerator GoalWording() {
        tpContainer.Pulse(factor: 1.1f, duration: 3.5f);
        fpContainer.Pulse(factor: 1.1f, duration: 3.5f);

        yield return new WaitUntilSceneTime(7, 31.75f);
        tpContainer.Pulse(factor: 1.1f, duration: 3.0f);
        fpContainer.Pulse(factor: 1.1f, duration: 3.0f);
        tnContainer.Pulse(factor: 1.1f, duration: 3.0f);
        fnContainer.Pulse(factor: 1.1f, duration: 3.0f);

        yield return new WaitUntilSceneTime(7, 36.0f);
        fpContainer.Pulse(factor: 1.1f, duration: 13.0f);
        tnContainer.Pulse(factor: 1.1f, duration: 13.0f);

        yield return new WaitUntilSceneTime(7, 54.5f);
    }
    //Construct schedule
    protected override void DefineSchedule() {
        // new SceneBlock(4, 45f, Appear, flexible: true)
        // new SceneBlock(5, 8f, Flips, flexible: true);
        new SceneBlock(16, 18f, Appear, flexible: true);
        new SceneBlock(16, 31f, MakeTable, flexible: false);
        new SceneBlock(16, 34, CalculationPlaceholder, flexible: false);
        // new SceneBlock(6, 0, VocabTime, flexible: false);
        // new SceneBlock(7, 27, GoalWording);
    }
}
