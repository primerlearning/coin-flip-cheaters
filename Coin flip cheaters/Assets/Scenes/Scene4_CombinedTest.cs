﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;

// Change the class name to whatever you want.
// Make sure it matches the file name, or Unity will get real mad
public class Scene4_CombinedTest : Director
{
    // PrimerObject graphParent = null;
    BinaryTree tree;
    Graph graph1;
    Graph graph2;
    public List<BTNode> NodePrefabs;
    PrimerShapesLine testThresholdLine;
    PrimerShapesLine testThresholdLine2;
    PrimerText tps = null;
    PrimerText fps = null;
    PrimerText tns = null;
    PrimerText fns = null;
    PrimerText tpsNum = null;
    PrimerText fpsNum = null;
    PrimerText tnsNum = null;
    PrimerText fnsNum = null;
    PrimerArrow a1 = null;
    PrimerArrow a2 = null;
    PrimerArrow a3 = null;
    PrimerArrow a4 = null;
    PrimerText fpGraphCheck;
    PrimerText tpGraphCheck;
    PrimerText fpGraphX;
    PrimerText tpGraphX;
    PrimerText fpGoalCheck;
    PrimerText tpGoalCheck;
    PrimerText fpGoalX;
    PrimerText tpGoalX;
    [SerializeField] VideoClip calculationClip = null;
    // ReferenceScreen referenceScreen;
    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        base.Start();
        // Time.timeScale = 50;
        Vector3 startingCamSwivelOrigin = new Vector3(0, 0, 0);
        Quaternion startingCamSwivel = Quaternion.Euler(0, 0, 0);
        float startingCamDistance = 7.5f;
        camRig.SwivelOrigin = startingCamSwivelOrigin;
        camRig.Swivel = startingCamSwivel;
        camRig.Distance = startingCamDistance;
    }
    //Define event actions
    IEnumerator TreeAppear() {
        // Build the scene sketched in Notion
        // Starting with two flips

        tree = BinaryTree.DecisionTree(5, NodePrefabs[1], NodePrefabs[0]);
        tree.SetRootNodeText("Start");
        tree.transform.position = new Vector3(0, 6, 10);
        // // Abandoned Hax to manipulate position of nodes
        // tree = DecisionTree(3);
        // tree.transform.position += new Vector3(0, 6, 10);

        // for (int i = 0; i <= 2; i++) {
        //     List<BTNode> level = tree.GetLevel(i);
        //     for (int j = 0; j < level.Count; j++) {
        //         tree.PlaceNode(level[j]);
        //     }
        // }
        tree.PlaceNodes(2);
        tree.DisplayToLevel(3);
        yield return new WaitUntilSceneTime(11, 10.75f);
        tree.ShowBranchLabels("50%", "50%", 0.1f, maxLevel: 2);
        // yield return new WaitForSeconds(2);
        yield return new WaitUntilSceneTime(11, 11.5f);
        tree.MoveTo(new Vector3(0, 3.25f, 0), duration: 1);
        tree.ScaleTo(0.25f, duration: 1);
    }
    IEnumerator GraphAppear() {
        // ReferenceScreen rs = MakeReferenceScreen(clip: calculationClip);
        // graphParent = new GameObject().MakePrimerObject();
        // graphParent.gameObject.name = "graphs";
        // graphParent.transform.position = new Vector3(1.3f, 0, -2.9f);
        // graphParent.transform.rotation = camRig.transform.rotation;

        Dictionary<float, string> xTics = new Dictionary<float, string> () {
            {1, 0.ToString()},
            {2, 1.ToString()},
            {3, 2.ToString()},
        };
        graph1 = Instantiate(primerGraphPrefab);
        graph1.Initialize(
            xAxisLength: 5f,
            xMax: 3.7f,
            manualTicMode: true,
            manualTicsX: xTics,
            yMax: 0.5f,
            yTicStep: 0.25f,
            yAxisLength: 2f,
            thickness: 1.5f,
            zHidden: true,
            xAxisLabelPos: "along",
            xAxisLabelString: "Heads",
            yAxisLabelPos: "along",
            yAxisLabelString: "Probability (50/50 coin)"
        );
        // graph1.transform.parent = graphParent.transform;
        graph1.transform.localPosition = new Vector3(-1.9f, -2.8f, 0);
        graph1.transform.localRotation = Quaternion.identity;
        graph1.axesHandler.yAxisLabel.transform.localRotation = Quaternion.Euler(0, 0, 90);
        graph1.axesHandler.yAxisLabel.transform.localPosition = new Vector3(-0.75f, 0.6f, 0);
        graph1.ScaleUpFromZero();
        // Time.timeScale = 1;
        yield return new WaitForSeconds(0.5f);
        List<float> probabilities1 = new List<float>();
        // int testThreshold = 18;
        int numFlips = 2;
        for (int i = 0; i <= numFlips; i++) {
            probabilities1.Add((float) Helpers.Binomial(2, i, 0.5f));
            // if (i < testThreshold) {
            // }
            // else {
            //     graph1Colors.Add(PrimerColor.Blue / Mathf.Sqrt(2));
            // }
        }
        BarDataManager bd = graph1.AddBarData();
        // bd.showBarNumbers = true;
        graph1.barData.barWidth = 0.9f;
        graph1.barData.defaultColor = PrimerColor.Blue;
        // graph1.barData.SetColors(PrimerColor.Blue);

        List<List<BTNode>> nodeMap1 = new List<List<BTNode>>() {
            new List<BTNode>() {tree.GetLevel(2)[0]},
            new List<BTNode>() {tree.GetLevel(2)[1], tree.GetLevel(2)[2]},
            new List<BTNode>() {tree.GetLevel(2)[3]}
        };

        List<float> cues = new List<float>() {
            11 * 60 + 14,
            11 * 60 + 16,
            11 * 60 + 19
        };
        for (int i = 0; i < probabilities1.Count; i++) {
            List<float> shownProbabilities = new List<float>();
            for (int j = 0; j < probabilities1.Count; j++) {
                if (j <= i) {
                    shownProbabilities.Add(probabilities1[j]);
                }
                else {
                    shownProbabilities.Add(0);
                }
            }
            yield return new WaitUntilSceneTime(cues[i]);
            graph1.barData.AnimateBars(shownProbabilities);
            foreach (BTNode node in nodeMap1[i]) {
                tree.HighlightPath(node, PrimerColor.Blue, totalDuration: 2, changeDuration: 0.33f);
                // tree.SetPathColor(node, PrimerColor.Blue);
                // yield return new WaitForSeconds(1.5f);
                // tree.SetPathColor(node, Color.white);
            }
        }
        yield return new WaitForSeconds(1);

        yield return new WaitUntilSceneTime(11, 22);
        tree.HideAllLabels(0.1f);
        yield return new WaitForSeconds(1);
        tree.ShowBranchLabels("25%", "75%", 0.1f, maxLevel: 2);


        graph1.MoveTo(new Vector3(-5.2f, -2.8f, 0));
        yield return new WaitForSeconds(0.5f);
        graph2 = Instantiate(primerGraphPrefab);
        graph2.Initialize(
            xAxisLength: 5f,
            xMax: 3.7f,
            manualTicMode: true,
            manualTicsX: xTics,
            yMax: 0.5f,
            yTicStep: 0.25f,
            yAxisLength: 2f,
            thickness: 1.5f,
            zHidden: true,
            xAxisLabelPos: "along",
            xAxisLabelString: "Heads",
            yAxisLabelPos: "along",
            yAxisLabelString: "Probability (25/75 coin)"
        );
        // graph2.transform.parent = graphParent.transform;
        graph2.transform.localPosition = new Vector3(1.45f, -2.8f, 0);
        graph2.transform.localRotation = Quaternion.identity;
        graph2.axesHandler.yAxisLabel.transform.localRotation = Quaternion.Euler(0, 0, 90);
        graph2.axesHandler.yAxisLabel.transform.localPosition = new Vector3(-0.75f, 0.6f, 0);
        graph2.ScaleUpFromZero();

        yield return new WaitForSeconds(0.5f);
        List<float> probabilities2 = new List<float>();
        // int testThreshold = 18;
        for (int i = 0; i <= numFlips; i++) {
            probabilities2.Add((float) Helpers.Binomial(2, i, 0.75f));
        }
        BarDataManager bd2 = graph2.AddBarData();
        // bd2.showBarNumbers = true;
        graph2.barData.barWidth = 0.9f;
        graph2.barData.defaultColor = PrimerColor.Red;

        for (int i = 0; i < probabilities2.Count; i++) {
            List<float> shownProbabilities = new List<float>();
            for (int j = 0; j < probabilities2.Count; j++) {
                if (j <= i) {
                    shownProbabilities.Add(probabilities2[j]);
                }
                else {
                    shownProbabilities.Add(0);
                }
            }
            graph2.barData.AnimateBars(shownProbabilities);
            foreach (BTNode node in nodeMap1[i])
            {
                tree.HighlightPath(node, PrimerColor.Red, totalDuration: 1.5f, changeDuration: 0.33f);
                // tree.HighlightPath(node, PrimerColor.Blue, totalDuration: 2, changeDuration: 0.33f);
                // tree.SetPathColor(node, PrimerColor.Red);
                // yield return new WaitForSeconds(1.5f);
                // tree.SetPathColor(node, Color.white);
            }
            yield return new WaitForSeconds(1.5f);
        }
        voiceOverReference.Pause();
        InsertSceneTime(3);
    }
    IEnumerator ThreeFlips() {
        // Clear graphs in preparation for three flips
        // yield return new WaitUntilSceneTime(11, 26.5f);
        graph1.barData.AnimateBars(new List<float>() {0, 0, 0});
        graph2.barData.AnimateBars(new List<float>() {0, 0, 0});

        yield return new WaitForSeconds(0.5f);
        // yield return new WaitForSeconds(1);
        // InsertSceneTime(1000);
        // yield return new WaitForSeconds(1000);

        // Add a third layer to the tree
        tree.UpdateNodes(3);
        tree.ScaleTo(0.18f);

        Dictionary<float, string> xTics = new Dictionary<float, string> () {
            {1, 0.ToString()},
            {2, 1.ToString()},
            {3, 2.ToString()},
            {4, 3.ToString()},
        };
        graph1.manualTicsX = xTics;
        graph1.ChangeRangeX(0, 4.7f);
        graph2.manualTicsX = xTics;
        graph2.ChangeRangeX(0, 4.7f);
        tree.HideAllLabels(0.1f);
        yield return new WaitForSeconds(0.5f);
        tree.DisplayLevel(3);
        yield return new WaitForSeconds(0.5f);
        tree.ShowBranchLabels("50%", "50%", 0.1f, maxLevel: 3);
        yield return new WaitForSeconds(1.0f);
        List<float> probabilities1 = new List<float>();
        List<float> probabilities2 = new List<float>();

        int numFlips = 3;
        for (int i = 0; i <= numFlips; i++) {
            probabilities1.Add((float) Helpers.Binomial(numFlips, i, 0.5f));
            probabilities2.Add((float) Helpers.Binomial(numFlips, i, 0.75f));
        }

        List<List<BTNode>> nodeMap1 = new List<List<BTNode>>() {
            new List<BTNode>() {tree.GetLevel(3)[0]},
            new List<BTNode>() {tree.GetLevel(3)[1], tree.GetLevel(3)[2], tree.GetLevel(3)[4]},
            new List<BTNode>() {tree.GetLevel(3)[3], tree.GetLevel(3)[5], tree.GetLevel(3)[6]},
            new List<BTNode>() {tree.GetLevel(3)[7]}
        };
        for (int i = 0; i < probabilities1.Count; i++) {
            List<float> shownProbabilities1 = new List<float>();
            for (int j = 0; j < probabilities1.Count; j++) {
                if (j <= i) {
                    shownProbabilities1.Add(probabilities1[j]);
                }
                else {
                    shownProbabilities1.Add(0);
                }
            }
            graph1.barData.AnimateBars(shownProbabilities1);
            foreach (BTNode node in nodeMap1[i])
            {
                tree.HighlightPath(node, PrimerColor.Blue, totalDuration: 2, changeDuration: 0.33f);
                // tree.AnimatePathColor(node, PrimerColor.Blue);
                // yield return new WaitForSeconds(1.0f);
                // tree.AnimatePathColor(node, Color.white);
            }
            yield return new WaitForSeconds(2);
        }
        voiceOverReference.Pause();

        tree.HideAllLabels(0.1f);
        yield return new WaitForSeconds(0.5f);
        tree.ShowBranchLabels("25%", "75%", 0.1f, maxLevel: 3);
        yield return new WaitForSeconds(1.0f);

        for (int i = 0; i < probabilities2.Count; i++) {
            List<float> shownProbabilities2 = new List<float>();
            for (int j = 0; j < probabilities2.Count; j++) {
                if (j <= i) {
                    shownProbabilities2.Add(probabilities2[j]);
                }
                else {
                    shownProbabilities2.Add(0);
                }
            }
            graph2.barData.AnimateBars(shownProbabilities2);
            foreach (BTNode node in nodeMap1[i])
            {
                tree.HighlightPath(node, PrimerColor.Red, totalDuration: 2, changeDuration: 0.33f);
                // tree.AnimatePathColor(node, PrimerColor.Red);
                // yield return new WaitForSeconds(1.0f);
                // tree.AnimatePathColor(node, Color.white);
            }
            yield return new WaitForSeconds(2);
        }
        // graph1.barData.AnimateBars(probabilities1, duration: 0.5f);
        // graph2.barData.AnimateBars(probabilities1, duration: 0.5f);
    }
    IEnumerator MoreFlips() {
        // Clear graphs in preparation for four flips
        graph1.barData.AnimateBars(new List<float>() {0, 0, 0, 0});
        graph2.barData.AnimateBars(new List<float>() {0, 0, 0, 0});


        // Add a layer to the tree
        tree.UpdateNodes(4);
        tree.ScaleTo(0.135f);

        Dictionary<float, string> xTics = new Dictionary<float, string> () {
            {1, 0.ToString()},
            {2, 1.ToString()},
            {3, 2.ToString()},
            {4, 3.ToString()},
            {5, 4.ToString()},
        };
        graph1.manualTicsX = xTics;
        graph1.ChangeRangeX(0, 5.7f);
        graph2.manualTicsX = xTics;
        graph2.ChangeRangeX(0, 5.7f);
        tree.HideAllLabels(0.05f);
        yield return new WaitForSeconds(0.5f);
        tree.DisplayLevel(4);
        tree.ShowBranchLabels("50%", "50%", 0.05f, maxLevel: 4);
        yield return new WaitForSeconds(0.5f);
        List<float> probabilities1 = new List<float>();
        List<float> probabilities2 = new List<float>();
        int numFlips = 4;
        for (int i = 0; i <= numFlips; i++) {
            probabilities1.Add((float) Helpers.Binomial(numFlips, i, 0.5f));
            probabilities2.Add((float) Helpers.Binomial(numFlips, i, 0.75f));
        }
        List<List<BTNode>> nodeMap1 = new List<List<BTNode>>() {
            new List<BTNode>() {tree.GetLevel(4)[0]},
            new List<BTNode>() {tree.GetLevel(4)[15 - 14], tree.GetLevel(4)[15 - 13], tree.GetLevel(4)[15 - 11], tree.GetLevel(4)[15 - 7]},
            // new List<BTNode>() {tree.GetLevel(4)[15 - 12], tree.GetLevel(4)[15 - 10], tree.GetLevel(4)[15 - 9], tree.GetLevel(4)[15 - 6], tree.GetLevel(4)[15 - 5], tree.GetLevel(4)[15 - 3]},
            new List<BTNode>() {tree.GetLevel(4)[12], tree.GetLevel(4)[10], tree.GetLevel(4)[9], tree.GetLevel(4)[6], tree.GetLevel(4)[5], tree.GetLevel(4)[3]},
            new List<BTNode>() {tree.GetLevel(4)[14], tree.GetLevel(4)[13], tree.GetLevel(4)[11], tree.GetLevel(4)[7]},
            new List<BTNode>() {tree.GetLevel(4)[15]}
        };
        for (int i = 0; i < probabilities1.Count; i++) {
            List<float> shownProbabilities1 = new List<float>();
            for (int j = 0; j < probabilities1.Count; j++) {
                if (j <= i) {
                    shownProbabilities1.Add(probabilities1[j]);
                }
                else {
                    shownProbabilities1.Add(0);
                }
            }
            graph1.barData.AnimateBars(shownProbabilities1, duration: 0.25f);
            foreach (BTNode node in nodeMap1[i])
            {
                tree.HighlightPath(node, PrimerColor.Blue, totalDuration: 0.25f, changeDuration: 0);
                // tree.AnimatePathColor(node, PrimerColor.Blue);
                // yield return new WaitForSeconds(1.0f);
                // tree.AnimatePathColor(node, Color.white);
            }
            yield return new WaitForSeconds(0.25f);
        }
        // voiceOverReference.Pause();
        tree.HideAllLabels(0.05f);
        yield return new WaitForSeconds(0.5f);
        tree.ShowBranchLabels("25%", "75%", 0.05f, maxLevel: 4);
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < probabilities2.Count; i++) {
            List<float> shownProbabilities2 = new List<float>();
            for (int j = 0; j < probabilities2.Count; j++) {
                if (j <= i) {
                    shownProbabilities2.Add(probabilities2[j]);
                }
                else {
                    shownProbabilities2.Add(0);
                }
            }
            graph2.barData.AnimateBars(shownProbabilities2, duration: 0.25f);
            foreach (BTNode node in nodeMap1[i])
            {
                tree.HighlightPath(node, PrimerColor.Red, totalDuration: 0.25f, changeDuration: 0);
            }
            yield return new WaitForSeconds(0.25f);
        }

        // Now 5

        graph1.barData.AnimateBars(new List<float>() {0, 0, 0, 0});
        graph2.barData.AnimateBars(new List<float>() {0, 0, 0, 0});


        // Add a layer to the tree
        tree.UpdateNodes(5);
        tree.ScaleTo(0.09f);

        xTics = new Dictionary<float, string> () {
            {1, 0.ToString()},
            {2, 1.ToString()},
            {3, 2.ToString()},
            {4, 3.ToString()},
            {5, 4.ToString()},
            {6, 5.ToString()},
        };
        graph1.manualTicsX = xTics;
        graph1.ChangeRangeX(0, 6.7f);
        graph2.manualTicsX = xTics;
        graph2.ChangeRangeX(0, 6.7f);
        tree.HideAllLabels(0.05f);
        yield return new WaitForSeconds(0.5f);
        tree.DisplayLevel(5);
        tree.ShowBranchLabels("50%", "50%", 0.05f, maxLevel: 5);
        yield return new WaitForSeconds(0.5f);
        probabilities1 = new List<float>();
        probabilities2 = new List<float>();
        numFlips = 5;
        for (int i = 0; i <= numFlips; i++) {
            probabilities1.Add((float) Helpers.Binomial(numFlips, i, 0.5f));
            probabilities2.Add((float) Helpers.Binomial(numFlips, i, 0.75f));
        }
        nodeMap1 = new List<List<BTNode>>() {
            new List<BTNode>() {tree.GetLevel(5)[31 - 31]},
            new List<BTNode>() {tree.GetLevel(5)[31 - 30], tree.GetLevel(5)[31 - 29], tree.GetLevel(5)[31 - 27], tree.GetLevel(5)[31 - 23], tree.GetLevel(5)[31 - 15]},
            new List<BTNode>() { tree.GetLevel(5)[31 - 28], tree.GetLevel(5)[31 - 26], tree.GetLevel(5)[31 - 25], tree.GetLevel(5)[31 - 22], tree.GetLevel(5)[31 - 21], tree.GetLevel(5)[31 - 19], tree.GetLevel(5)[31 - 14], tree.GetLevel(5)[31 - 13], tree.GetLevel(5)[31 - 11], tree.GetLevel(5)[31 - 7]},
            new List<BTNode>() { tree.GetLevel(5)[28], tree.GetLevel(5)[26], tree.GetLevel(5)[25], tree.GetLevel(5)[22], tree.GetLevel(5)[21], tree.GetLevel(5)[19], tree.GetLevel(5)[14], tree.GetLevel(5)[13], tree.GetLevel(5)[11], tree.GetLevel(5)[7]},
            new List<BTNode>() {tree.GetLevel(5)[30], tree.GetLevel(5)[29], tree.GetLevel(5)[27], tree.GetLevel(5)[23], tree.GetLevel(5)[15]},
            new List<BTNode>() {tree.GetLevel(5)[31]},
        };
        for (int i = 0; i < probabilities1.Count; i++) {
            List<float> shownProbabilities1 = new List<float>();
            for (int j = 0; j < probabilities1.Count; j++) {
                if (j <= i) {
                    shownProbabilities1.Add(probabilities1[j]);
                }
                else {
                    shownProbabilities1.Add(0);
                }
            }
            graph1.barData.AnimateBars(shownProbabilities1, duration: 0.25f);
            foreach (BTNode node in nodeMap1[i])
            {
                tree.HighlightPath(node, PrimerColor.Blue, totalDuration: 0.25f, changeDuration: 0);
                // tree.AnimatePathColor(node, PrimerColor.Blue);
                // yield return new WaitForSeconds(1.0f);
                // tree.AnimatePathColor(node, Color.white);
            }
            yield return new WaitForSeconds(0.25f);
        }
        // voiceOverReference.Pause();
        tree.HideAllLabels(0.05f);
        yield return new WaitForSeconds(0.5f);
        tree.ShowBranchLabels("25%", "75%", 0.05f, maxLevel: 5);
        yield return new WaitForSeconds(0.5f);
        for (int i = 0; i < probabilities2.Count; i++) {
            List<float> shownProbabilities2 = new List<float>();
            for (int j = 0; j < probabilities2.Count; j++) {
                if (j <= i) {
                    shownProbabilities2.Add(probabilities2[j]);
                }
                else {
                    shownProbabilities2.Add(0);
                }
            }
            graph2.barData.AnimateBars(shownProbabilities2, duration: 0.25f);
            foreach (BTNode node in nodeMap1[i])
            {
                tree.HighlightPath(node, PrimerColor.Red, totalDuration: 0.25f, changeDuration: 0);
            }
            yield return new WaitForSeconds(0.25f);
        }
        
        yield return new WaitUntilSceneTime(11, 43.5f);
        tree.Hide(totalDuration: 0.5f);
    }
    IEnumerator TestLine() {

        // PrimerText testText = Instantiate(primerTextPrefab);
        // testText.tmpro.text = "Flip five coins. If they \nare all heads, accuse\nthe player of cheating";
        // testText.tmpro.alignment = TextAlignmentOptions.Left;
        // testText.transform.position = new Vector3(-5.2f, 2, 0);
        // testText.SetIntrinsicScale(0.45f);
        // testText.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(12, 11);
        testThresholdLine = new GameObject().AddComponent<PrimerShapesLine>();
        testThresholdLine.line.Thickness = 0.0f;
        testThresholdLine.line.Dashed = true;
        testThresholdLine.line.DashSpace = Shapes.DashSpace.Meters;
        testThresholdLine.line.DashSize = 0.15f;
        testThresholdLine.line.DashType = Shapes.DashType.Rounded;
        testThresholdLine.line.Geometry = Shapes.LineGeometry.Flat2D;
        testThresholdLine.animatedOffsetSpeed = 0.001f;
        testThresholdLine.transform.parent = graph1.transform;
        testThresholdLine.transform.localPosition = Vector3.zero;
        testThresholdLine.transform.localScale = Vector3.one;
        testThresholdLine.Start = graph1.CoordinateToPosition(5.5f, 0, 0);
        testThresholdLine.End = graph1.CoordinateToPosition(5.5f, 0.5f, 0);

        testThresholdLine.AnimateValue<float>("Thickness", 0.05f);

        testThresholdLine2 = new GameObject().AddComponent<PrimerShapesLine>();
        testThresholdLine2.line.Thickness = 0.0f;
        testThresholdLine2.line.Dashed = true;
        testThresholdLine2.line.DashSpace = Shapes.DashSpace.Meters;
        testThresholdLine2.line.DashSize = 0.15f;
        testThresholdLine2.line.DashType = Shapes.DashType.Rounded;
        testThresholdLine2.line.Geometry = Shapes.LineGeometry.Flat2D;
        testThresholdLine2.animatedOffsetSpeed = 0.001f;
        testThresholdLine2.transform.parent = graph2.transform;
        testThresholdLine2.transform.localPosition = Vector3.zero;
        testThresholdLine2.transform.localScale = Vector3.one;
        testThresholdLine2.Start = graph2.CoordinateToPosition(5.5f, 0, 0);
        testThresholdLine2.End = graph2.CoordinateToPosition(5.5f, 0.5f, 0);

        testThresholdLine2.AnimateValue<float>("Thickness", 0.05f);
    }
    IEnumerator Vocab() {
        //True negative
        tns = Instantiate(primerTextPrefab);
        tns.transform.SetParent(graph1.transform);
        tns.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.9f, 0);
        tns.SetIntrinsicScale(0.15f);
        tns.tmpro.text = "True negatives";
        tns.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        tnsNum = Instantiate(primerTextPrefab);
        tnsNum.transform.SetParent(graph1.transform);
        tnsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.65f, 0);
        tnsNum.SetIntrinsicScale(0.15f);
        float num = graph1.barData.values.Take(5).Sum() * 100;
        float roundedNum = (float) Math.Round(num, 1);
        string numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        tnsNum.tmpro.text = numString;
        tnsNum.ScaleUpFromZero();
        // a1 = Instantiate(primerArrowPrefab);
        // a1.transform.parent = graph1.transform;
        // a1.SetIntrinsicScale(0.5f);
        // a1.SetFromTo(
        //     new Vector3(0.6f, 1, 0),
        //     new Vector3(0.75f, 0.65f, 0)
        // );
        // a1.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a1.transform.right));
        // a1.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(12, 25.5f);

        //False positive
        fps = Instantiate(primerTextPrefab);
        fps.transform.SetParent(graph1.transform);
        fps.tmpro.text = "False positives";
        // fps.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(2.8f, 1.35f, 0);
        fps.AnchoredPosition = new Vector3(2.8f, 1.9f, 0);
        fps.SetIntrinsicScale(0.15f);
        fps.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        fpsNum = Instantiate(primerTextPrefab);
        fpsNum.transform.SetParent(graph1.transform);
        fpsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(2.8f, 1.65f, 0);
        fpsNum.SetIntrinsicScale(0.15f);
        num = graph1.barData.values.Skip(5).Sum() * 100;
        roundedNum = (float) Math.Round(num, 1);
        numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        fpsNum.tmpro.text = numString;
        fpsNum.ScaleUpFromZero();
        // a2 = Instantiate(primerArrowPrefab);
        // a2.transform.parent = graph1.transform;
        // a2.SetIntrinsicScale(0.5f);
        // a2.SetFromTo(
        //     new Vector3(2.7f, 1.17f, 0),
        //     new Vector3(2.18f, 0.79f, 0)
        // );
        // a2.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a2.transform.right));
        // a2.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        List<Color> fairColors = new List<Color>() {
            PrimerColor.Blue,
            PrimerColor.Blue,
            PrimerColor.Blue,
            PrimerColor.Blue,
            PrimerColor.Blue,
            PrimerColor.Blue / Mathf.Sqrt(2)
        };
        graph1.barData.AnimateColors(fairColors, duration: 0.5f);

        yield return new WaitUntilSceneTime(12, 33f);
        //False negative
        fns = Instantiate(primerTextPrefab);
        fns.transform.SetParent(graph2.transform);
        fns.tmpro.text = "False negatives";
        fns.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.9f, 0);
        fns.SetIntrinsicScale(0.15f);
        fns.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        fnsNum = Instantiate(primerTextPrefab);
        fnsNum.transform.SetParent(graph2.transform);
        fnsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(0.66f, 1.65f, 0);
        fnsNum.SetIntrinsicScale(0.15f);
        num = graph2.barData.values.Take(5).Sum() * 100;
        roundedNum = (float) Math.Round(num, 1);
        numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        fnsNum.tmpro.text = numString;
        fnsNum.ScaleUpFromZero();
        // a4 = Instantiate(primerArrowPrefab);
        // a4.transform.parent = graph2.transform;
        // a4.SetIntrinsicScale(0.5f);
        // a4.SetFromTo(
        //     new Vector3(0.65f, 0.6f, 0),
        //     new Vector3(0.76f, 0.25f, 0)
        // );
        // a4.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a4.transform.right));
        // a4.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        List<Color> cheaterColors = new List<Color>() {
            PrimerColor.Red / Mathf.Sqrt(2),
            PrimerColor.Red / Mathf.Sqrt(2),
            PrimerColor.Red / Mathf.Sqrt(2),
            PrimerColor.Red / Mathf.Sqrt(2),
            PrimerColor.Red / Mathf.Sqrt(2),
            PrimerColor.Red
        };
        graph2.barData.AnimateColors(cheaterColors, duration: 0.5f);

        yield return new WaitUntilSceneTime(12, 37.5f);
        //True positive
        tps = Instantiate(primerTextPrefab);
        tps.transform.SetParent(graph2.transform);
        tps.tmpro.text = "True positives";
        tps.AnchoredPosition = new Vector3(2.8f, 1.9f, 0);
        tps.SetIntrinsicScale(0.15f);
        tps.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        tpsNum = Instantiate(primerTextPrefab);
        tpsNum.transform.SetParent(graph2.transform);
        tpsNum.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(2.8f, 1.65f, 0);
        tpsNum.SetIntrinsicScale(0.15f);
        num = graph2.barData.values.Skip(5).Sum() * 100;
        roundedNum = (float) Math.Round(num, 1);
        numString = roundedNum.ToString() + "%";
        if (num != roundedNum) { numString = "~" + numString; }
        tpsNum.tmpro.text = numString;
        tpsNum.ScaleUpFromZero();

        // a3 = Instantiate(primerArrowPrefab);
        // a3.transform.parent = graph2.transform;
        // a3.SetIntrinsicScale(0.5f);
        // a3.SetFromTo(
        //     new Vector3(1.6f, 1.44f, 0),
        //     new Vector3(1.94f, 1.04f, 0)
        // );
        // a3.transform.rotation = Quaternion.LookRotation(camRig.transform.forward, Vector3.Cross(camRig.transform.forward, a3.transform.right));
        // a3.ScaleUpFromZero();
        // yield return new WaitForSeconds(5);

        // a1.ScaleDownToZero();
        // a2.ScaleDownToZero();
        // a3.ScaleDownToZero();
        // a4.ScaleDownToZero();
        // tns.ScaleDownToZero();
        // fps.ScaleDownToZero();
        // fns.ScaleDownToZero();
        // tps.ScaleDownToZero();

    }
    IEnumerator UpTo23() {
        PauseVoiceOverAtTime(13, 54);
        // Prep graph checks and x's
        fpGraphCheck = Instantiate(primerCheckPrefab);
        fpGraphCheck.SetColor(PrimerColor.Green);
        fpGraphCheck.transform.SetParent(graph1.transform);
        fpGraphCheck.transform.localPosition = new Vector3(2f, 1.95f, 0);
        fpGraphCheck.SetIntrinsicScale(0.5f);
        tpGraphCheck = Instantiate(primerCheckPrefab);
        tpGraphCheck.SetColor(PrimerColor.Green);
        tpGraphCheck.transform.SetParent(graph2.transform);
        tpGraphCheck.transform.localPosition = new Vector3(2.05f, 1.95f, 0);
        tpGraphCheck.SetIntrinsicScale(0.5f);
        fpGraphX = Instantiate(primerXPrefab);
        fpGraphX.SetColor(PrimerColor.Red);
        fpGraphX.transform.SetParent(graph1.transform);
        fpGraphX.transform.localPosition = new Vector3(2f, 1.884f, 0);
        fpGraphX.SetIntrinsicScale(0.35f);
        tpGraphX = Instantiate(primerXPrefab);
        tpGraphX.SetColor(PrimerColor.Red);
        tpGraphX.transform.SetParent(graph2.transform);
        tpGraphX.transform.localPosition = new Vector3(2.05f, 1.884f, 0);
        tpGraphX.SetIntrinsicScale(0.35f);

        // Prep goal checks and x's
        fpGoalCheck = Instantiate(primerCheckPrefab);
        fpGoalCheck.SetColor(PrimerColor.Green);
        fpGoalCheck.transform.localPosition = new Vector3(2f, 2.95f, 0);
        fpGoalCheck.SetIntrinsicScale(0.75f);
        tpGoalCheck = Instantiate(primerCheckPrefab);
        tpGoalCheck.SetColor(PrimerColor.Green);
        tpGoalCheck.transform.localPosition = new Vector3(2.05f, 1.95f, 0);
        tpGoalCheck.SetIntrinsicScale(0.75f);
        fpGoalX = Instantiate(primerXPrefab);
        fpGoalX.SetColor(PrimerColor.Red);
        fpGoalX.transform.localPosition = new Vector3(2f, 2.884f, 0);
        fpGoalX.SetIntrinsicScale(0.35f * 1.5f);
        tpGoalX = Instantiate(primerXPrefab);
        tpGoalX.SetColor(PrimerColor.Red);
        tpGoalX.transform.localPosition = new Vector3(2.05f, 1.884f, 0);
        tpGoalX.SetIntrinsicScale(0.35f * 1.5f);

        tpGraphCheck.transform.localScale = Vector3.zero;
        tpGraphX.transform.localScale = Vector3.zero;
        fpGraphX.transform.localScale = Vector3.zero;
        tpGoalCheck.transform.localScale = Vector3.zero;
        tpGoalX.transform.localScale = Vector3.zero;
        fpGoalX.transform.localScale = Vector3.zero;

        yield return new WaitUntilSceneTime(12, 44.5f);
        fpsNum.TempColorChange(PrimerColor.Orange, duration: 4.5f);
        fpGraphCheck.ScaleUpFromZero();
        fpGoalCheck.ScaleUpFromZero();
        yield return new WaitUntilSceneTime(12, 51.25f);
        tpsNum.TempColorChange(PrimerColor.Orange, duration: 4f);
        tpGraphX.ScaleUpFromZero();
        tpGoalX.ScaleUpFromZero();

        yield return new WaitUntilSceneTime(13, 0.5f);
        SetHeadsThreshold(4);
        bool[] goalBools = CheckGoals(4, 5, 0.05f, 0.8f);
        StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        UpdateStats(4);
        yield return new WaitUntilSceneTime(13, 5.5f);
        SetHeadsThreshold(3);
        goalBools = CheckGoals(3, 5, 0.05f, 0.8f);
        StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        UpdateStats(3);

        

        yield return new WaitUntilSceneTime(13, 20.5f);
        yield return GraphOverlapSubscene();

        // yield return new WaitForSeconds(1);
        // SetHeadsThreshold(2);
        // goalBools = CheckGoals(2, 5, 0.05f, 0.8f);
        // StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        // UpdateStats(2);


        yield return new WaitUntilSceneTime(13, 50f);
        Time.timeScale = 1;
        bool searching = true;
        for (int numFlips = 5; numFlips <= 500; numFlips++) {
            // Debug.Log("arstoienarstoienarst");
            yield return SetNumFlips(numFlips);
            // With the bars updated, iterate through line positions (heads thresholds)
            for (int i = numFlips - 1; i >= 0; i--) {
                SetHeadsThreshold(i + 1);
                goalBools = CheckGoals(i + 1, numFlips, 0.05f, 0.8f);
                StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
                UpdateStats(i + 1);

                yield return new WaitForSeconds(0.5f);
                if (!goalBools[0]) {
                    testThresholdLine.AnimateThickness(0);
                    testThresholdLine2.AnimateThickness(0);
                    break;
                }
                if (goalBools[1]) { 
                    // Time.timeScale = 0; 
                    searching = false;
                    Debug.Log(numFlips);
                    Debug.Log(i);
                    break;
                }
            }
            if (!searching) {
                break;
            }
        }
        // voiceOverReference.time = 13 * 60 + 56.5f;
        // voiceOverReference.Play();
        // yield return new WaitForSeconds(17.5f);
    }
    IEnumerator SetNumFlips(int numFlips, bool editTicSteps = true, float duration = 0.5f) {
        Dictionary<float, string> xTics = new Dictionary<float, string> () {};
        List<float> probabilities1 = new List<float>();
        List<float> probabilities2 = new List<float>();
        // Recalculate tics
        int xTicStep = 1;
        while (numFlips / xTicStep > 6) {
            if (xTicStep.ToString()[0] == '1') {
                xTicStep *= 2;
            }
            else if (xTicStep.ToString()[0] == '2') {
                xTicStep = Mathf.RoundToInt(xTicStep * 2.5f);
            }
            else if (xTicStep.ToString()[0] == '5') {
                xTicStep *= 2;
            }
        }
        for (int i = 0; i <= numFlips; i++) {
            if (i % xTicStep == 0) {
                xTics.Add(i+1, i.ToString());
            }
            probabilities1.Add((float) Helpers.Binomial(numFlips, i, 0.5f));
            probabilities2.Add((float) Helpers.Binomial(numFlips, i, 0.75f));
        }
        // Change range
        float oldMax = graph1.xMax;
        float newMax = numFlips + 1.7f;
        if (editTicSteps) {
            graph1.manualTicsX = xTics;
        }
        graph1.ChangeRangeX(0, newMax, duration: duration);
        testThresholdLine.MovePoints(
            new Vector3(testThresholdLine.Start.x * oldMax / newMax, testThresholdLine.Start.y, testThresholdLine.Start.z),
                new Vector3(testThresholdLine.End.x * oldMax / newMax, testThresholdLine.End.y, testThresholdLine.End.z),
            duration: duration
        );
        testThresholdLine2.MovePoints(
            new Vector3(testThresholdLine2.Start.x * oldMax / newMax, testThresholdLine2.Start.y, testThresholdLine2.Start.z),
            new Vector3(testThresholdLine2.End.x * oldMax / newMax, testThresholdLine2.End.y, testThresholdLine2.End.z),
            duration: duration
        );
        graph2.manualTicsX = xTics;
        graph2.ChangeRangeX(0, newMax, duration: duration);
        // yield return new WaitForSeconds(duration);
        graph1.barData.defaultColor = PrimerColor.Blue / Mathf.Sqrt(2);
        graph1.barData.AnimateBars(probabilities1, duration: duration);
        graph2.barData.AnimateBars(probabilities2, duration: duration);
        yield return new WaitForSeconds(duration);
    }
    void SetHeadsThreshold(int minHeads) {
        // Set bar colors
        for (int j = 0; j < graph1.barData.BarCount; j++) {
            if (j <= minHeads - 1) {
                graph1.barData.AnimateColor(j, PrimerColor.Blue);
                graph2.barData.AnimateColor(j, PrimerColor.Red / Mathf.Sqrt(2));
            }
            if (j > minHeads - 1) {
                graph1.barData.AnimateColor(j, PrimerColor.Blue / Mathf.Sqrt(2));
                graph2.barData.AnimateColor(j, PrimerColor.Red);
            }
        }
        // Move line
        if (testThresholdLine.Thickness > 0) {
            testThresholdLine.MovePoints(
                graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0),
                graph1.CoordinateToPosition(minHeads + 0.5f, 0.5f, 0)
            );
        }
        else {
            testThresholdLine.Start = graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0);
            testThresholdLine.End = graph1.CoordinateToPosition(minHeads + 0.5f, 0.5f, 0);
            testThresholdLine.AnimateThickness(0.05f);
        }
        if (testThresholdLine2.Thickness > 0) {
            testThresholdLine2.MovePoints(
                graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0),
                graph1.CoordinateToPosition(minHeads + 0.5f, 0.5f, 0)
            );
        }
        else {
            testThresholdLine2.Start = graph1.CoordinateToPosition(minHeads + 0.5f, 0, 0);
            testThresholdLine2.End = graph1.CoordinateToPosition(minHeads + 0.5f, 0.5f, 0);
            testThresholdLine2.AnimateThickness(0.05f);
        }
    }
    bool[] CheckGoals(int minHeads, int numFlips, float maxFPRate, float minTPRate) {
        bool[] goalBools = new bool[2];

        float num1 = graph1.barData.values.Take(minHeads).Sum();
        float num2 = graph2.barData.values.Take(minHeads).Sum();

        if (num1 > 1 - maxFPRate) {goalBools[0] = true;}
        if (num2 < 1 - minTPRate) {goalBools[1] = true;}

        return goalBools;
    }
    IEnumerator updateChecksAndExes(bool goal1Met, bool goal2Met) {
        // Get rid of checkmarks and x marks that aren't true anymore
        if (!goal1Met) {
            fpGoalCheck.ScaleDownToZero();
            fpGraphCheck.ScaleDownToZero();
        }
        if (goal1Met) {
            fpGoalX.ScaleDownToZero();
            fpGraphX.ScaleDownToZero();
        }
        if (!goal2Met) {
            tpGoalCheck.ScaleDownToZero();
            tpGraphCheck.ScaleDownToZero();
        }
        if (goal2Met) {
            tpGoalX.ScaleDownToZero();
            tpGraphX.ScaleDownToZero();
        }
        yield return new WaitForSeconds(0.0f);
        if (!goal1Met && fpGoalX.transform.localScale == Vector3.zero) { 
            fpGraphX.ScaleUpFromZero();
            fpGoalX.ScaleUpFromZero();
        }
        if (goal1Met && fpGoalCheck.transform.localScale == Vector3.zero) {
            fpGraphCheck.ScaleUpFromZero();
            fpGoalCheck.ScaleUpFromZero();
        }
        if (!goal2Met && tpGoalX.transform.localScale == Vector3.zero) {
            tpGraphX.ScaleUpFromZero();
            tpGoalX.ScaleUpFromZero();
        }
        if (goal2Met && tpGoalCheck.transform.localScale == Vector3.zero) {
            tpGraphCheck.ScaleUpFromZero();
            tpGoalCheck.ScaleUpFromZero();
        }
    }
    void UpdateStats(int minHeads) {
        // Number text updates
        float num1 = graph1.barData.values.Take(minHeads).Sum() * 100;
        float num2 = graph2.barData.values.Take(minHeads).Sum() * 100;
        // Fair player numbers (true negative and false positive)
        float roundedNum = (float) Math.Round(num1, 1);
        string numString = roundedNum.ToString() + "%";
        if (num1 != roundedNum) { numString = "~" + numString; }
        tnsNum.tmpro.text = numString;

        numString = Math.Round(100 - num1, 1).ToString() + "%";
        if (num1 != roundedNum) { numString = "~" + numString; }
        fpsNum.tmpro.text = numString;

        // Cheater numbers (false negative and true positive)
        roundedNum = (float) Math.Round(num2, 1);
        numString = Math.Round(100 - num2, 1).ToString() + "%";
        if (num2 != roundedNum) { numString = "~" + numString; }
        tpsNum.tmpro.text = numString;

        numString = roundedNum.ToString() + "%";
        if (num2 != roundedNum) { numString = "~" + numString; }
        fnsNum.tmpro.text = numString;
    }
    IEnumerator GraphOverlapSubscene() {
        // Move the graph and camera, fix labels temporarily
        float moveDuration = 1;
        Vector3 graph1StartingPosition = graph1.transform.position;
        graph1.MoveTo(graph2.transform.position + new Vector3(-0.1f, 0.1f, 0.1f), duration: moveDuration);

        Vector3 oldCamCenter = camRig.SwivelOrigin;
        float oldCamDistance = camRig.Distance;
        camRig.MoveCenterTo(new Vector3(3.37f, -1.65f, 0), duration: moveDuration);
        camRig.ZoomTo(4, duration: moveDuration);
        
        List<PrimerObject> toDownShift = new List<PrimerObject>() {
            tps,
            tpGraphCheck,
            tpGraphX,
            tpsNum,
            fns,
            fnsNum,
        };
        foreach (PrimerObject po in toDownShift) {
            po.MoveBy(new Vector3(0.1f, -0.45f, 0), duration: moveDuration);
        }
        graph1.axesHandler.yAxisLabel.MoveBy(Vector3.left * 0.25f, duration: moveDuration);
        graph1.axesHandler.yAxisLabel.ChangeColor(PrimerColor.Blue, duration: moveDuration);
        graph2.axesHandler.yAxisLabel.ChangeColor(PrimerColor.Red, duration: moveDuration);

        List<PrimerObject> labelsToHide = new List<PrimerObject>();
        labelsToHide.AddRange(graph1.axesHandler.xTics);
        labelsToHide.AddRange(graph1.axesHandler.yTics);
        labelsToHide.Add(graph1.axesHandler.xAxisLabel);
        foreach (PrimerObject po in labelsToHide) {
            po.ScaleDownToZero(duration: moveDuration);
        }
        yield return new WaitUntilSceneTime(13, 32.75f);
        
        // Increase to 100 flips
        // graph1.xTicStep = 1000;

        // PrimerShapesLine oldLine = testThresholdLine;

        // testThresholdLine = new GameObject().AddComponent<PrimerShapesLine>();
        // testThresholdLine.line.Thickness = 0.0f;
        // testThresholdLine.line.Dashed = true;
        // testThresholdLine.line.DashSpace = Shapes.DashSpace.Meters;
        // testThresholdLine.line.DashSize = 0.15f;
        // testThresholdLine.line.DashType = Shapes.DashType.Rounded;
        // testThresholdLine.line.Geometry = Shapes.LineGeometry.Flat2D;
        // testThresholdLine.animatedOffsetSpeed = 0.001f;
        // testThresholdLine.line.DashOffset = oldLine.line.DashOffset;
        // testThresholdLine.transform.parent = graph1.transform;
        // testThresholdLine.Start = oldLine.Start;
        // testThresholdLine.End = oldLine.End;
        // testThresholdLine.Start = graph1.CoordinateToPosition(3.5f, 0, 0);
        // testThresholdLine.End = graph1.CoordinateToPosition(3.5f, 0.5f, 0);
        // testThresholdLine.AnimateValue<float>("Thickness", 0.05f);
        // oldLine.AnimateThickness(0);
        yield return SetNumFlips(100, editTicSteps: false, duration: 1);
        bool[] goalBools = CheckGoals(3, 100, 0.05f, 0.8f);
        StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        UpdateStats(3);

        yield return new WaitUntilSceneTime(13, 37.5f);
        // int t = 70;
        // SetHeadsThreshold(t);
        // goalBools = CheckGoals(t, 100, 0.05f, 0.8f);
        // StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        // UpdateStats(t);
        // yield return new WaitForSeconds(1);

        int t = 63;
        SetHeadsThreshold(t);
        yield return new WaitForSeconds(0.5f);
        goalBools = CheckGoals(t, 100, 0.05f, 0.8f);
        StartCoroutine(updateChecksAndExes(goalBools[0], goalBools[1]));
        UpdateStats(t);

        // Decrease back to 5 flips
        // Put everything back
        yield return new WaitUntilSceneTime(13, 48f);
        graph1.MoveTo(graph1StartingPosition);
        camRig.MoveCenterTo(oldCamCenter);
        camRig.ZoomTo(oldCamDistance);
        foreach (PrimerObject po in toDownShift) {
            po.MoveBy(- new Vector3(0.1f, -0.45f, 0));
        }
        graph1.axesHandler.yAxisLabel.MoveBy(Vector3.right * 0.25f);
        graph1.axesHandler.yAxisLabel.ChangeColor(Color.white);
        graph2.axesHandler.yAxisLabel.ChangeColor(Color.white);
        foreach (PrimerObject po in labelsToHide) {
            po.ScaleUpFromZero();
        }

        yield return SetNumFlips(5);
        SetHeadsThreshold(5);
    }
    IEnumerator Stall() {
        yield return new WaitUntilSceneTime(14, 14);
    }
    //Construct schedule
    protected override void DefineSchedule() {
        new SceneBlock(11, 5.5f, TreeAppear, flexible: false);
        new SceneBlock(11, 12.75f, GraphAppear, flexible: true);
        new SceneBlock(11, 26.5f, ThreeFlips, flexible: true);
        new SceneBlock(11, 32.5f, MoreFlips, flexible: false);
        new SceneBlock(12, 3.5f, TestLine, flexible: false);
        new SceneBlock(12, 20.75f, Vocab, flexible: false);
        new SceneBlock(12, 44.5f, UpTo23, flexible: true);
        new SceneBlock(13, 56.5f, Stall);
    }
}