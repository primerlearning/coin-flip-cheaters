from manim import *

PRIMER_GRAY = "#2f3336"
PRIMER_WHITE = "#ffffff"
PRIMER_BLUE = "#3e7ea0"
PRIMER_ORANGE = "#ff9400"
PRIMER_YELLOW = "#e7e247"
PRIMER_RED = "#d63b50"
PRIMER_GREEN = "#698f3f"
PRIMER_PURPLE = "#db5aba"


class Test(Scene):
    def construct(self):
        paragraph = Text(r"If the first flip is\nheads, accuse the\nplayer of cheating")
        self.add(paragraph)
        self.wait()
        self.play(Transform(paragraph[0], Text("IT WORKED").move_to(paragraph[0])))
        self.wait()

class GoalIntro(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        goals = Group(Tex("Goals:", tex_template = myTemplate))
        goals.add(Tex("1. Avoid accusing fair", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5))
        goals.add(Tex("players of cheating", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.add(Tex("2. Catch as many cheaters", tex_template = myTemplate).next_to(goals[1], 4 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("as possible", tex_template = myTemplate).next_to(goals[3], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.add(Tex("3. Make the test as brief", tex_template = myTemplate).next_to(goals[3], 4 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("as possible", tex_template = myTemplate).next_to(goals[5], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.move_to(LEFT * 3, LEFT)

        self.play(Write(goals[0]))
        for i in range(1, 6, 2):
            self.play(Write(goals[i]))
            self.play(Write(goals[i+1]))
            self.wait()
        disappear = AnimationGroup(
            Unwrite(goals[0]),
            Unwrite(goals[1]),
            Unwrite(goals[2]),
            Unwrite(goals[3]),
            Unwrite(goals[4]),
            Unwrite(goals[5]),
            Unwrite(goals[6]),
        )
        self.play(disappear)
        self.wait()

class GoalScene1(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        goals = Group(Tex("Goals:", tex_template = myTemplate))
        goals.add(Tex("1. Avoid accusing fair", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5))
        goals.add(Tex("players of cheating", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.add(Tex("2. Catch as many cheaters", tex_template = myTemplate).next_to(goals[1], 4 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("as possible", tex_template = myTemplate).next_to(goals[3], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.add(Tex("3. Make the test as brief", tex_template = myTemplate).next_to(goals[3], 4 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("as possible", tex_template = myTemplate).next_to(goals[5], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.move_to(LEFT * 3, LEFT)

        appear = AnimationGroup(
            Write(goals[0]),
            Write(goals[1]),
            Write(goals[2]),
            Write(goals[3]),
            Write(goals[4]),
            Write(goals[5]),
            Write(goals[6]),
        )
        self.play(appear)
        self.wait()

        new1 = Tex("1. Accuse fewer than 5\\%", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5)
        new2 = Tex("of fair players", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6)
        morph = AnimationGroup(
            Transform(goals[1], new1),
            Transform(goals[2], new2)
        )
        self.play(morph)

        self.wait()
        disappear = AnimationGroup(
            Unwrite(goals[0]),
            Unwrite(goals[1]),
            Unwrite(goals[2]),
            Unwrite(goals[3]),
            Unwrite(goals[4]),
            Unwrite(goals[5]),
            Unwrite(goals[6]),
        )
        self.play(disappear)
        self.wait()
class GoalScene2(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        goals = Group(Tex("Goals:", tex_template = myTemplate))
        goals.add(Tex("1. Accuse fewer than 5\\%", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5))
        goals.add(Tex("of fair players", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.add(Tex("2. Catch as many cheaters", tex_template = myTemplate).next_to(goals[1], 4 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("as possible", tex_template = myTemplate).next_to(goals[3], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.add(Tex("3. Make the test as brief", tex_template = myTemplate).next_to(goals[3], 4 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("as possible", tex_template = myTemplate).next_to(goals[5], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6))
        goals.move_to(LEFT * 3, LEFT)

        appear = AnimationGroup(
            Write(goals[0]),
            Write(goals[1]),
            Write(goals[2]),
            Write(goals[3]),
            Write(goals[4]),
            Write(goals[5]),
            Write(goals[6]),
        )
        self.play(appear)
        self.wait()

        new1 = Tex("1. False positive rate", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5)
        new2 = Tex("below 5\\%", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6)
        morph = AnimationGroup(
            Transform(goals[1], new1),
            Transform(goals[2], new2)
        )
        self.play(morph)
        self.wait()

        new1 = Tex("1. Accuse fewer than 5\\%", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5)
        new2 = Tex("of fair players", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6)
        morph = AnimationGroup(
            Transform(goals[1], new1),
            Transform(goals[2], new2)
        )
        self.play(morph)
        self.wait()

        new1 = Tex("2. Catch more than 80\\%", tex_template = myTemplate).next_to(goals[1], 4 * DOWN, aligned_edge=LEFT)
        new2 = Tex("of cheaters", tex_template = myTemplate).next_to(goals[3], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6)
        morph = AnimationGroup(
            Transform(goals[3], new1),
            Transform(goals[4], new2)
        )
        self.play(morph)

        self.wait()
        disappear = AnimationGroup(
            Unwrite(goals[0]),
            Unwrite(goals[1]),
            Unwrite(goals[2]),
            Unwrite(goals[3]),
            Unwrite(goals[4]),
            Unwrite(goals[5]),
            Unwrite(goals[6]),
        )
        self.play(disappear)
        self.wait()
class GoalScene4(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        # myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage{beton}")
        # myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        goals = Group(Tex("Goals:", tex_template = myTemplate))
        goals.add(Tex("1. Accuse fewer than 5\\% of fair players", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5))
        goals.add(Tex("2. Catch more than 80\\% of cheaters", tex_template = myTemplate).next_to(goals[1], 2 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("3. Make the test as brief as possible", tex_template = myTemplate).next_to(goals[2], 2 * DOWN, aligned_edge=LEFT))
        goals.move_to(LEFT * 3, LEFT)

        appear = AnimationGroup(
            Write(goals[0]),
            Write(goals[1]),
            Write(goals[2]),
            Write(goals[3]),
        )
        self.play(appear)
        self.wait()

        disappear = AnimationGroup(
            Unwrite(goals[0]),
            Unwrite(goals[1]),
            Unwrite(goals[2]),
            Unwrite(goals[3]),
        )
        self.play(disappear)
        self.wait()
class GoalScene6(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        # myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage{beton}")
        # myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        goals = Group(Tex("Goals:", tex_template = myTemplate))
        goals.add(Tex("1. Accuse fewer than 5\\% of fair players", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5))
        goals.add(Tex("2. Catch more than 80\\% of cheaters", tex_template = myTemplate).next_to(goals[1], 2 * DOWN, aligned_edge=LEFT))
        goals.add(Tex("3. Make the test as brief as possible", tex_template = myTemplate).next_to(goals[2], 2 * DOWN, aligned_edge=LEFT))
        goals.move_to(LEFT * 3, LEFT)

        appear = AnimationGroup(
            Write(goals[0]),
            Write(goals[1]),
            Write(goals[2]),
            Write(goals[3]),
        )
        self.play(appear)
        self.wait()

        new1 = Tex("1. Accuse fewer than 1\\% of fair players", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5)
        # new2 = Tex("of fair players", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6)
        morph = AnimationGroup(
            Transform(goals[1], new1),
            # Transform(goals[2], new2)
        )
        self.play(morph)
        self.wait()

        new1 = Tex("2. Catch more than 99\\% of cheaters", tex_template = myTemplate).next_to(goals[1], 2 * DOWN, aligned_edge=LEFT)
        # new2 = Tex("of fair players", tex_template = myTemplate).next_to(goals[1], DOWN / 2, aligned_edge=LEFT).shift(RIGHT * 0.6)
        morph = AnimationGroup(
            Transform(goals[2], new1),
            # Transform(goals[2], new2)
        )
        self.play(morph)
        self.wait()

        new1 = Tex("1. Accuse fewer than 5\\% of fair players", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5)
        new2 = Tex("2. Catch more than 80\\% of cheaters", tex_template = myTemplate).next_to(goals[1], 2 * DOWN, aligned_edge=LEFT)
        morph = AnimationGroup(
            Transform(goals[1], new1),
            Transform(goals[2], new2)
        )
        self.play(morph)
        self.wait()

        disappear = AnimationGroup(
            Unwrite(goals[0]),
            Unwrite(goals[1]),
            Unwrite(goals[2]),
            Unwrite(goals[3]),
        )
        self.play(disappear)
        self.wait()

class Goals(Scene):
    def construct(self):
        goals = Group(Tex("Goals:"))
        goals.add(Tex(r"1. Avoid accusing fair players of cheating").next_to(goals[0], DOWN, aligned_edge=LEFT).shift(
            RIGHT * 0.5))
        goals.add(Tex("2. Catch as many cheaters as possible").next_to(goals[1], DOWN, aligned_edge=LEFT))
        goals.add(Tex("3. Make the test as brief as possible").next_to(goals[2], DOWN, aligned_edge=LEFT))
        goals.move_to(LEFT * 3, LEFT)
        for i in range(4):
            self.play(Write(goals[i]))
            self.wait()
        self.wait()
        for x in [{1: Tex(r"1. Accuse fewer than 5\% of fair players")},
                  {1: Tex(r"1. False-positive rate below 5\%")},
                  {1: Tex(r"1. Accuse fewer than 5\% of fair players")},
                  {2: Tex(r"2. Catch more than 80\% of cheaters")},
                  {1: Tex(r"1. False-positive rate below 5\%"),
                   2: Tex(r"2. True-positive rate above 80\%")},
                  {1: Tex(r"1. Accuse fewer than 5\% of fair players"),
                   2: Tex(r"2. Catch more than 80\% of cheaters")},
                  {1: Tex(r"1. Accuse fewer than 1\% of fair players")},
                  {2: Tex(r"2. Catch as many cheaters as possible"),
                   3: Tex(r"3. Use 26 coin flips")},
                  {2: Tex(r"2. Catch more than 99\% of cheaters"),
                   3: Tex(r"3. Make the test as brief as possible")},
                  {1: Tex(r"1. Accuse fewer than 5\% of fair players"),
                   2: Tex(r"2. Catch more than 80\% of cheaters"),
                   3: Tex("3. Make the test as brief as possible")}]:
            self.play(*(Transform(goals[i], tex.move_to(goals[i], LEFT)) for i, tex in x.items()))
            self.wait()

class RulesScene1and2(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        label = Tex("Test rule:", tex_template = myTemplate)
        rules = Group(  # TODO paragraphs
            # Tex(r"\begin{flushleft}If the first flip is\\heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first two flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first three flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first four flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first five flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets five\\out of five heads, accuse\\the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            # *(Tex(r"\begin{flushleft}Flip " + str(i) + r" coins. If " + str(j) +
            #       r" or\\more are heads, accuse\\the player of cheating\end{flushleft}", tex_template = myTemplate)
            #   for i, j in ((2, 1), (10, 8), (26, 18)))
        )

        self.play(Write(label))
        self.play(Write(rules[0]))
        self.wait()
        disappear = AnimationGroup(
            Unwrite(label),
            Unwrite(rules[0])
        )
        self.play(disappear)
        # curr = rules[0]
        # for i in range(1, len(rules)):
        #     self.play(TransformMatchingTex(curr, rules[i]))
        #     curr = rules[i]
        #     self.wait()
class RulesScene3(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        label = Tex("Test rule:", tex_template = myTemplate)
        rules = Group(  # TODO paragraphs
            # Tex(r"\begin{flushleft}If the first flip is\\heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first two flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first three flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first four flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first five flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets five\\out of five heads, accuse\\the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets seven or\\more heads out of ten flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            # *(Tex(r"\begin{flushleft}Flip " + str(i) + r" coins. If " + str(j) +
            #       r" or\\more are heads, accuse\\the player of cheating\end{flushleft}", tex_template = myTemplate)
            #   for i, j in ((2, 1), (10, 8), (26, 18)))
        )

        self.play(Write(label))
        self.play(Write(rules[0]))

        curr = rules[0]
        for i in range(1, len(rules)):
            self.play(Transform(curr, rules[i]))
            curr = rules[i]
            self.wait()
        self.wait()
        disappear = AnimationGroup(
            Unwrite(label),
            Unwrite(rules[0])
        )
        self.play(disappear)
class RulesScene4(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        label = Tex("Test rule:", tex_template = myTemplate)
        rules = Group(  # TODO paragraphs
            # Tex(r"\begin{flushleft}If the first flip is\\heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first two flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first three flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first four flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            # Tex(r"\begin{flushleft}If the first five flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets five\\out of five heads, accuse\\the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets four or\\more heads out of five flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets three or\\more heads out of five flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets ? or\\more heads out of ? flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets 16 or\\more heads out of 23 flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets 18 or\\more heads out of 23 flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets ? or\\more heads out of ? flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets 28 or\\more heads out of 40 flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets ? or\\more heads out of ? flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets 51 or\\more heads out of 80 flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            Tex(r"\begin{flushleft}If a player gets 16 or\\more heads out of 23 flips,\\accuse the player of cheating\end{flushleft}", tex_template = myTemplate).next_to(label, 2 * DOWN, aligned_edge=LEFT).shift(RIGHT * 0.5),
            # *(Tex(r"\begin{flushleft}Flip " + str(i) + r" coins. If " + str(j) +
            #       r" or\\more are heads, accuse\\the player of cheating\end{flushleft}", tex_template = myTemplate)
            #   for i, j in ((2, 1), (10, 8), (26, 18)))
        )

        self.play(Write(label))
        self.play(Write(rules[0]))

        curr = rules[0]
        for i in range(1, len(rules)):
            self.play(Transform(curr, rules[i]))
            # curr = rules[i]
            self.wait()
            if (i == 4):
                self.wait() # Hax to get the timing to fit an older version
        disappear = AnimationGroup(
            Unwrite(label),
            Unwrite(rules[0])
        )
        self.play(disappear)
        self.wait()
class Rules(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        rules = Group(  # TODO paragraphs
            Tex(r"\begin{flushleft}If the first flip is\\heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            Tex(r"\begin{flushleft}If the first two flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            Tex(r"\begin{flushleft}If the first three flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            Tex(r"\begin{flushleft}If the first four flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            Tex(r"\begin{flushleft}If the first five flips\\are heads, accuse the\\player of cheating\end{flushleft}", tex_template = myTemplate),
            *(Tex(r"\begin{flushleft}Flip " + str(i) + r" coins. If " + str(j) +
                  r" or\\more are heads, accuse\\the player of cheating\end{flushleft}", tex_template = myTemplate)
              for i, j in ((2, 1), (10, 8), (26, 18))))
        self.play(Write(rules[0]))
        self.wait()
        curr = rules[0]
        for i in range(1, len(rules)):
            self.play(TransformMatchingTex(curr, rules[i]))
            curr = rules[i]
            self.wait()

class MistakeScene(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        # myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage{beton}")
        # myTemplate.add_to_preamble("\\usepackage{concmath}")
        myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        goals = Group(Tex("Assumed cheater heads probability = 75\\%", tex_template = myTemplate))
        goals.add(Tex("Actual cheater heads probability = 60\\%", tex_template = myTemplate).next_to(goals[0], 2 * DOWN, aligned_edge=LEFT))
        goals.move_to(LEFT * 3, LEFT)

        self.play(Write(goals[0]))
        self.wait()
        self.play(Write(goals[1]))
        self.wait()

        disappear = AnimationGroup(
            Unwrite(goals[0]),
            Unwrite(goals[1]),
        )
        self.play(disappear)
        self.wait()

class Probs(Scene):
    def construct(self):
        myTemplate = TexTemplate()
        # myTemplate.add_to_preamble("\\usepackage{beton}")
        # myTemplate.add_to_preamble("\\usepackage{euler}")
        # myTemplate.add_to_preamble("\\usepackage{lipsum}")
        # myTemplate.add_to_preamble("\\usepackage{ccfonts}")
        # myTemplate.add_to_preamble("\\usepackage{concmath}")
        # myTemplate.add_to_preamble("\\usepackage[T1]{fontenc}")
        prefix = MathTex("P(", tex_template = myTemplate)
        heads = ImageMobject("assets/blob_heads.png").scale_to_fit_height(prefix.height)
        tails = ImageMobject("assets/blob_tails.png").scale_to_fit_height(prefix.height)
        suffix = MathTex(r") = \frac{1}{2} \cdot \frac{1}{2} = \frac{1}{4}", tex_template = myTemplate)
        suffix.next_to(tails, RIGHT)
        all = Group(prefix, heads, tails, suffix)
        all.center()
        calcs = [Group(prefix.copy())]
        calcs[0].add(heads.copy().next_to(calcs[0], RIGHT, buff=-0.15))
        calcs[0].add(MathTex(r") = \frac{1}{2} = 50\%", substrings_to_isolate="=", tex_template = myTemplate).next_to(calcs[0], RIGHT, buff=-0.15))
        calcs.append(Group(prefix.copy()))
        calcs[1].add(heads.copy().next_to(calcs[1], RIGHT, buff=-0.15))
        calcs[1].add(
            MathTex(r"\times 2) = \frac{1}{2} \cdot \frac{1}{2} = \frac{1}{4}", substrings_to_isolate="=", tex_template = myTemplate).next_to(
                calcs[1],
                RIGHT,
                buff=-0.15))
        calcs[1].add(MathTex(r"= 25\%", tex_template = myTemplate).next_to(calcs[1][2][3], DOWN, aligned_edge=LEFT, buff=0.75))
        for i in range(2, 5):
            calcs.append(Group(prefix.copy()))
            calcs[i].add(heads.copy().next_to(calcs[i], RIGHT, buff=-0.15))
            calcs[i].add(MathTex(r"\times " + str(i + 1) + r") = \left(\frac{1}{2}\right)^" + str(i + 1) +
                                 r"= \frac{1}{" + str(2 ** (i + 1)) + r"}", substrings_to_isolate="=", tex_template = myTemplate).next_to(calcs[i],
                                                                                                               RIGHT,
                                                                                                               buff=-0.15))
            calcs[i].add(
                MathTex("=" + str(100 / (2 ** (i + 1))) + r"\%", tex_template = myTemplate).next_to(calcs[i][2][3], DOWN, aligned_edge=LEFT,
                                                                         buff=0.75))
        for calc in calcs:
            calc.center()
        calcs[0].add(calcs[0][-1].copy())
        self.play(Write(calcs[0][0]), Write(calcs[0][-2]), FadeIn(calcs[0][1:-2]))  # highlight orange
        self.wait()
        for i in range(4):
            self.play(ReplacementTransform(calcs[i][0], calcs[i + 1][0]),
                      *(ReplacementTransform(calcs[i][-2][j], calcs[i + 1][-2][j]) for j in range(5)),
                      ReplacementTransform(calcs[i][-1], calcs[i + 1][-1]),
                      calcs[i][1].animate.move_to(calcs[i + 1][1]))
            self.add(calcs[i + 1][1])
            self.remove(calcs[i][1])
            self.wait()
        
        disappear = AnimationGroup(
            Unwrite(calcs[4][0]),
            Unwrite(calcs[4][-1]),
            Unwrite(calcs[4][-2]),
            FadeOut(calcs[4][1:-2]),
            # Unwrite(calcs[1][0]),
            # Unwrite(calcs[1][-2]),
            # FadeOut(calcs[1][1:-2]),
            # Unwrite(calcs[2][0]),
            # Unwrite(calcs[2][-2]),
            # FadeOut(calcs[2][1:-2]),
            # Unwrite(calcs[3][0]),
            # Unwrite(calcs[3][-2]),
            # FadeOut(calcs[3][1:-2]),
        )
        self.play(disappear)
        self.wait()

class WeightedProbs(Scene):
    def construct(self):
        prefix = MathTex("P(")
        heads = ImageMobject("assets/blob_heads.png").scale_to_fit_height(prefix.height)
        one = Group(prefix.copy())
        one.add(heads.copy().next_to(one[0], RIGHT, buff=-0.15))
        one.add(MathTex(r") = 0.75 = 75\%").next_to(one[1], RIGHT, buff=-0.15))
        five = Group(prefix.copy())
        five.add(heads.copy().next_to(five[0], RIGHT, buff=-0.15))
        five.add(MathTex(r"\times 5) = \left(0.75\right)^5 \approx 24\%").next_to(five[1], RIGHT, buff=-0.15))
        five.next_to(one, DOWN, buff=0.4)
        Group(five, one).center()
        self.play(Write(one[0]), Write(one[2]), FadeIn(one[1]))
        self.wait()
        self.play(Write(five[0]), Write(five[2]), FadeIn(five[1]))
        self.wait()

class Binomial(Scene):
    def construct(self):
        b = MathTex(r"P(h)={n \choose h}{p^h}{(1-p)^{n-h}}")
        self.play(Write(b))
        self.wait()
        self.play(Unwrite(b))
        self.wait()