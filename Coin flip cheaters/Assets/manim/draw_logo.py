import os
from primer_constants import *
from manim import *

class DrawLogo(Scene):
    def construct(self):
        text = Text("darksouls")
        for s in text.submobjects:
            s.set_z_index(-1)

        # dirname = os.path.dirname(__file__)
        logo_path = os.path.join(PRIMER_RESOURCE_DIR, 'PrimerLogo_PathsOnly_med.svg')
        logo = SVGMobject(logo_path)

        logo.submobjects[0].set_fill(PRIMER_BLUE)
        logo.submobjects[0].set_stroke(PRIMER_BLUE)
        for s in logo.submobjects[1:]:
            s.set_fill(PRIMER_WHITE)
            s.set_stroke(PRIMER_WHITE)
            s.set_z_index(1)

        self.wait(1)
        self.play(Write(logo), run_time = 3)
        # self.play(Write(text))
        # self.play(Transform(text, logo))

        # self.wait(1)

        # self.play(ApplyMethod(logo.submobjects[0].set_fill, PRIMER_WHITE), run_time = 0.1)
        # self.play(ApplyMethod(logo.submobjects[1].set_fill, PRIMER_RED), run_time = 0.1)
        # self.play(ApplyMethod(logo.submobjects[2].set_fill, PRIMER_ORANGE), run_time = 0.1)
        # self.play(ApplyMethod(logo.submobjects[3].set_fill, PRIMER_YELLOW), run_time = 0.1)
        # self.play(ApplyMethod(logo.submobjects[4].set_fill, PRIMER_GREEN), run_time = 0.1)
        # self.play(ApplyMethod(logo.submobjects[5].set_fill, PRIMER_BLUE), run_time = 0.1)
        # self.play(ApplyMethod(logo.submobjects[6].set_fill, PRIMER_PURPLE), run_time = 0.1)

        # for i in range(1, 7):
        #     self.play(ApplyMethod(logo.submobjects[i].scale, 1.2), run_time = 0.1)
        #     self.play(ApplyMethod(logo.submobjects[i].scale, 1 / 1.2), run_time = 0.1)

class PText(Text):
    def __init__(self, text, **kwargs):
        super().__init__(text, font = "Kurier", **kwargs)

