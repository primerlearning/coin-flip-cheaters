using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreboardEntry : PrimerObject
{
    [SerializeField] TextMeshProUGUI nameText;
    [SerializeField] TextMeshProUGUI scoreText;
    [SerializeField] GameObject blobImage;
    [SerializeField] PrimerBlob blobPrefab;
    PrimerBlob blob;
    Camera cam;
    internal int index;
    int score = 0;
    public int Score {
        get { return score; }
        set { score = value; }
    }
    internal SessionResult session;
    internal void ShowInfo() {
        nameText.text = session.name;
        AnimateValue<int>("Score", session.score, onFrameFunc: UpdateScoreText);
        // GenerateBlobAndCam();
        while (blob.accessories.Count > 0) {
            BlobAccessory ba = blob.accessories[0];
            Destroy(ba.gameObject);
            blob.accessories.Remove(ba);
            // ba.Disappear();
        }
        blob.SetColor(session.avatar.color);
        AccessoryType aType = session.avatar.aType;
        if (aType != AccessoryType.none) {
            bool colorMatch = true;
            if (aType == AccessoryType.beard || aType == AccessoryType.eyePatch) { colorMatch = false; }
            blob.AddAccessory(aType, colorMatch: colorMatch, animate: false);
        }
    }
    internal void MakeNullSession() {
        session = new SessionResult();
        session.name = "None yet!";
        session.score = 0;
        session.avatar = new Avatar();
        session.avatar.color = PrimerColor.Blue;
        session.avatar.aType = AccessoryType.none;
    }
    void UpdateScoreText() {
        scoreText.text = $"{score}";
    }
    internal void GenerateBlobAndCam() {
        Vector3 pos = new Vector3(100 + 2 * index, 0, -100);
        blob = Instantiate(blobPrefab);
        blob.transform.position = pos;
        blob.transform.rotation = Quaternion.Euler(0, 160, 0);
        if (session != null) {
            blob.SetColor(session.avatar.color);
            blob.AddAccessory(session.avatar.aType);
        }
        else {
            blob.RandomizeColorAndAccessory();
        }
        // Set up camera
        RenderTexture rt = new RenderTexture(70, 70, 24); // The image object is 70x70 px on the canvas
        cam = new GameObject().AddComponent<Camera>();
        cam.targetTexture = rt;
        blobImage.GetComponent<RawImage>().texture = rt;
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.backgroundColor = PrimerColor.Gray;

        cam.fieldOfView = 35;
        cam.transform.position = pos + new Vector3(0, 1.4f, -2);

        // RenderTexture.active = rt;
        // image = new Texture2D(resWidth, resHeight, TextureFormat.RGBA32, false);
        
        // // Render to image texture
        // cam.Render();
        // image.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
    }
    internal void GoAway() {
        if (blob != null) {
            blob.Disappear();
        }
        if (cam != null) {
            Destroy(cam.gameObject);
        }
        Destroy(this.gameObject);
    }
}
