using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
// using UnityEditor;

public class TestPlayer : MonoBehaviour
{
    [SerializeField] Strategy strategy;
    [SerializeField] CoinFlipGame game;
    Dictionary<Tuple<int, int>, PlayerType> cachedDecisions = new Dictionary<Tuple<int, int>, PlayerType>();
    double oddsRatio = 1;
    bool looksInfinite = false;
    int recursionDepth = 10;
    // Start is called before the first frame update
    void Start()
    {   
        Time.captureFramerate = 1;
        game.animationToggle.isOn = false;
        StartCoroutine(playOnce());
        // StartCoroutine(playTest());
    }
    IEnumerator playOnce(int maxCoins = 2000) {
        bool finished = false;
        while (!finished) {
            if (game.gameState == GameState.Playing && game.TakingInputs() && game.flipsRemaining >= maxCoins) {
                looksInfinite = true;
                yield return game.end();
                game.gameState = GameState.EndScreen;
                break;
            }
            if (game.gameState == GameState.Playing && game.TakingInputs()) {
                if (game.flipsRemaining > 0 && game.flipsRemaining < maxCoins) {
                    switch (Decide()) 
                    {
                        case PlayerType.Unknown:
                            game.flipNumSlider.value = 1;
                            game.Flip(1);
                            break;
                        case PlayerType.Cheater:
                            // yield return new WaitForSeconds(0.5f);
                            game.Accuse();
                            break;
                        case PlayerType.Fair:
                            // yield return new WaitForSeconds(0.5f);
                            game.Exonerate();
                            break;
                        default:
                            break;
                    }
                }
                else {
                    // Debug.Log(game.Score);
                    yield return game.end();
                    game.gameState = GameState.EndScreen;
                }
            }
            else if (game.gameState == GameState.EndScreen && game.TakingInputs()) {
                yield return game.reset();
                // game.SwitchToGameView();
                game.gameState = GameState.Playing;
                finished = true;
            }
            yield return null;
        }
    }
    IEnumerator playTest() {
        // Notes so far
        // Infinites:
        // 2, 23
        // 3, 41
        // 4, 60+
        // Multiplier will always be higher for a higher ratio, so we don't have to reset the multiplier


        yield return new WaitForSeconds(1);
        // int maxMultiplier = 20;
        // int multiplierStep = 9;
        // int numRuns = 20;
        // float maxOddsRatio = 5;
        // float oddsRatioStep = 1f;
        int maxRuns = 10;
        
        // Iterate through multipliers
        int ratio = 3;
        int multiplier = 40;
        while (ratio <= 20) {
            ratio++;
            CoinFlipGame.fnPenaltyBase = ratio;
            CoinFlipGame.fpPenaltyBase = ratio;
            // Iterate through multipliers
            looksInfinite = false;
            while (!looksInfinite) {
                multiplier+=10;
                game.Multiplier = multiplier;
                Debug.Log($"Beginning ratio {CoinFlipGame.fnPenaltyBase} and multiplier {game.Multiplier}");
                // Empty the cache before runs with new settings
                cachedDecisions = new Dictionary<Tuple<int, int>, PlayerType>();
                for (int i = 0; i < maxRuns; i++) {
                    yield return playOnce(maxCoins: 100 + 100 * multiplier);
                    if (looksInfinite) {
                        Debug.Log($"Looks infinite at ratio {ratio} and multiplier {multiplier}");
                        break;
                    }
                }
            }
        }
        
        // // Iterate through multipliers
        // for (int i = 1; i <= maxMultiplier; i+= multiplierStep) {
        //     game.Multiplier = i;
        //     // Iterate through odds ratios
        //     for (double j = 1; j <= maxOddsRatio; j += oddsRatioStep) {
        //         // Do all the runs
        //         oddsRatio = j;
        //         List<int> results = new List<int>();
        //         int k = 0;
        //         int maxScore = 0;
        //         while (k < numRuns) {
        //             if (game.gameState == GameState.Playing && game.TakingInputs()) {
        //                 if (game.flipsRemaining > 0 && game.flipsRemaining < 20000) {
        //                     switch (Decide()) 
        //                     {
        //                         case PlayerType.Unknown:
        //                             game.flipNumSlider.value = 1;
        //                             game.Flip();
        //                             break;
        //                         case PlayerType.Cheater:
        //                             // yield return new WaitForSeconds(0.5f);
        //                             game.Accuse();
        //                             break;
        //                         case PlayerType.Fair:
        //                             // yield return new WaitForSeconds(0.5f);
        //                             game.Exonerate();
        //                             break;
        //                         default:
        //                             break;
        //                     }
        //                 }
        //                 else {
        //                     k++;
        //                     if (game.flipsRemaining > 20000) {
        //                         Debug.Log($"Run {k+1} hit 20,000 coins.");
        //                     }
        //                     results.Add(game.Score);
        //                     yield return game.end();
        //                     game.gameState = GameState.EndScreen;
        //                 }
        //             }
        //             else if (game.gameState == GameState.EndScreen && game.TakingInputs()) {
        //                 yield return game.reset();
        //                 // game.SwitchToGameView();
        //                 game.gameState = GameState.Playing;
        //             }
        //             yield return null;
        //         }
        //         Debug.Log($"Multiplier: {game.Multiplier}\n OddsRatio: {oddsRatio}\n Max{results.Max()}\n Avg:{results.Average()}");
        //     }
        // }
        // EditorApplication.isPlaying = false;
    }
    
    PlayerType Decide() {
        List<int> results = game.currentFlipper.results;
        switch (strategy)
        {   
            case Strategy.JustOne:
                if (results.Sum() >= 1) {
                    return PlayerType.Cheater;
                }
                if (results.Count() - results.Sum() >= 1) {
                    return PlayerType.Fair;
                }
                return PlayerType.Unknown;
            case Strategy.FractionThresholds:
                // float ratio = Mathf.Log(2) / Mathf.Log(3); // This works out to be
                // float range = 0.2f;
                float upperThreshold = 0.75f;
                float lowerThreshold = 0.5f;
                if (results.Count() >= 2) {
                    if ((float) results.Sum() / results.Count() > upperThreshold) {
                        return PlayerType.Cheater;
                    }
                    if ((float) results.Sum() / results.Count() < lowerThreshold) {
                        return PlayerType.Fair;
                    }
                }
                return PlayerType.Unknown;
            case Strategy.Odds:
                double pCheater = Helpers.BayesProbabilityOfCheater(results.Count(), results.Sum(), game.cheaterHeadsRate);
                double oddsCheater = pCheater / (1 - pCheater);
                if (game.fpPenalty > 0 && oddsCheater > (float) game.fpPenalty/game.tpReward) {
                    // Debug.Log(results.Count());
                    // Debug.Log(pCheater);
                    // Debug.Log(oddsCheater);
                    // Debug.Break();
                    return PlayerType.Cheater;
                }
                if (game.tnReward > 0 && oddsCheater < (float) game.tnReward/game.fnPenalty) {
                    return PlayerType.Fair;
                }
                // else if (oddsCheater < 0.99) {
                //     return PlayerType.Fair;
                // }
                return PlayerType.Unknown;
            case Strategy.ExtraOdds:
                double ratio = 3;
                double pCheater2 = Helpers.BayesProbabilityOfCheater(results.Count(), results.Sum(), game.cheaterHeadsRate);
                // Debug.Log(pCheater2);
                double oddsCheater2 = pCheater2 / (1 - pCheater2);
                // Debug.Log(oddsCheater2);
                if (game.fpPenalty > 0 && oddsCheater2/ratio > (float) game.fpPenalty/game.tpReward) {
                    return PlayerType.Cheater;
                }
                if (game.tnReward > 0 && oddsCheater2*ratio < (float) game.tnReward/game.fnPenalty) {
                    return PlayerType.Fair;
                }
                // else if (oddsCheater2 < 0.99) {
                //     return PlayerType.Fair;
                // }
                return PlayerType.Unknown;
            case Strategy.EVAttempt:
                double currentPCheater = Helpers.BayesProbabilityOfCheater(results.Count(), results.Sum(), game.cheaterHeadsRate);

                // Loop through numbers of additional flips, start by going up to 50, but there might be a smarter cutoff.
                double maxEV = EVOfBestChoice(results.Count(), results.Sum(), game.cheaterHeadsRate);
                Debug.Log($"EV of deciding now: {maxEV}");
                int flipCountWithHighestEV = 0;

                int additionalFlips = 1;
                while (additionalFlips < 50) {
                    // Calculate EV of flipping that many and then assigning
                    // Do this by looping through possible outcomes of additional flips
                    // in each case adding the ev of the best choice given that outcome,
                    // weighted by the probability of that outcome given current info
                    double ev = 0;
                    for (int i = 0; i <= additionalFlips; i++) {
                        double cheaterBranchPOfIMoreHeads = currentPCheater * Helpers.Binomial(additionalFlips, i, game.cheaterHeadsRate);
                        // if (Helpers.Binomial(i, additionalFlips, game.cheaterHeadsRate) > 1) {Debug.LogError("arstoien");}
                        double contribution = cheaterBranchPOfIMoreHeads * EVOfBestChoice(results.Count() + additionalFlips, results.Sum() + i, game.cheaterHeadsRate);
                        Debug.Log(contribution);
                        ev += contribution;
                        // If it's a cheater, this is the contribution to the ev of getting i heads in the additional flips
                        double fairBranchPOfIMoreHeads = (1-currentPCheater) * Helpers.Binomial(additionalFlips, i, 0.5);
                        // if (fairBranchPOfIMoreHeads > 1) {Debug.LogError("arstoien");}
                        contribution = fairBranchPOfIMoreHeads * EVOfBestChoice(results.Count() + additionalFlips, results.Sum() + i, game.cheaterHeadsRate);
                        Debug.Log(contribution);
                        ev += contribution;
                    }
                    ev -= additionalFlips; // Gotta account for the flips spent
                    Debug.Log($"EV of {additionalFlips} additional flips: {ev}");
                    if (ev > maxEV) {
                        maxEV = ev;
                        flipCountWithHighestEV = additionalFlips;
                    }
                    additionalFlips++;
                }
                // EditorApplication.isPlaying = false;
                return PlayerType.Unknown;
            case Strategy.RecursiveEV:
                // Tuple<int, int> key = new Tuple<int, int>(results.Count(), results.Sum());
                // Debug.Log($"Contins key with {key.Item1}, {key.Item2}: {cachedDecisions.ContainsKey(key)}");
                return RecursiveEVDecision(results.Count(), results.Sum(), game.cheaterHeadsRate, 5, recursionDepth);
            default:
                Debug.LogError("Test player does not have a strategy");
                return PlayerType.Unknown;
        }
    }
    double ExpectedValueOfAssignment(PlayerType assignment, int totalFlips, int heads, double cheaterHeadsRate) {
        double pCheater = Helpers.BayesProbabilityOfCheater(totalFlips, heads, cheaterHeadsRate);
        // Debug.Log(pCheater);
        // double oddsCheater = pCheater / (1 - pCheater);
        if (assignment == PlayerType.Cheater) {
            return pCheater * game.tpReward - (1 - pCheater) * game.fpPenalty;
        }
        if (assignment == PlayerType.Fair) {
            return (1 - pCheater) * game.tnReward - pCheater * game.fnPenalty;
        }
        else {
            Debug.LogError("Player type not valid for EV calculation");
            return -1;
        }
    }
    double EVOfBestChoice(int totalFlips, int heads, double cheaterHeadsRate) {
        return System.Math.Max(
            ExpectedValueOfAssignment(PlayerType.Cheater, totalFlips, heads, cheaterHeadsRate),
            ExpectedValueOfAssignment(PlayerType.Fair, totalFlips, heads, cheaterHeadsRate)
        );
    }
    PlayerType RecursiveEVDecision(int flipsSoFar, int headsSoFar, double cheaterHeadsRate, int minDepth, int maxDepth) {
        Tuple<int, int> key = new Tuple<int, int>(flipsSoFar, headsSoFar);
        if (cachedDecisions.ContainsKey(key)) {
            // Debug.Log("Used cached result");
            return cachedDecisions[key];
        }
        double evAccuse = ExpectedValueOfAssignment(PlayerType.Cheater, flipsSoFar, headsSoFar, cheaterHeadsRate);
        double evExonerate = ExpectedValueOfAssignment(PlayerType.Fair, flipsSoFar, headsSoFar, cheaterHeadsRate);
        double evWait = RecursiveEVCalculation(flipsSoFar, headsSoFar, cheaterHeadsRate, minDepth, maxDepth);

        // Debug.Log(evAccuse);
        // Debug.Log(evExonerate);
        // Debug.Log(evWait);
        // Debug.Log("--------------------");
        if (evWait > evExonerate && evWait > evAccuse) {
            // cachedDecisions.Add(key, PlayerType.Unknown);
            AddToCachedDecisions(key, PlayerType.Unknown);
            return PlayerType.Unknown; // Means we do another flip
        }
        if (evExonerate > evAccuse) {
            // cachedDecisions.Add(key, PlayerType.Fair);
            for (int i = headsSoFar; i >= 0; i--) {
                Tuple<int, int> fairerKey = new Tuple<int, int>(flipsSoFar, i);
                if (!AddToCachedDecisions(fairerKey, PlayerType.Fair)) {
                    break;
                }
            }
            return PlayerType.Fair;
        }
        // cachedDecisions.Add(key, PlayerType.Cheater);
        for (int i = headsSoFar; i <= flipsSoFar; i++) {
            Tuple<int, int> cheatererKey = new Tuple<int, int>(flipsSoFar, i);
            if (!AddToCachedDecisions(cheatererKey, PlayerType.Cheater)) {
                break;
            }
        }
        return PlayerType.Cheater;
    }
    double RecursiveEVCalculation(int flipsSoFar, int headsSoFar, double cheaterHeadsRate, int minDepth, int maxDepth, int penalty = 1) {
        double evAccuse = ExpectedValueOfAssignment(PlayerType.Cheater, flipsSoFar, headsSoFar, cheaterHeadsRate);
        double evExonerate = ExpectedValueOfAssignment(PlayerType.Fair, flipsSoFar, headsSoFar, cheaterHeadsRate);
        double justPickEV = System.Math.Max(evAccuse, evExonerate);
        // double justPickEV = EVOfBestChoice(flipsSoFar, headsSoFar, cheaterHeadsRate);
        if (maxDepth <= 0) {// || (minDepth <= 0 && justPickEV > penalty)) {
            return justPickEV - penalty;
        }
        double pCheater = Helpers.BayesProbabilityOfCheater(flipsSoFar, headsSoFar, cheaterHeadsRate);
        double evWait = 
            // Cases with another flip and another heads
            (pCheater * cheaterHeadsRate + (1-pCheater) * 0.5) * RecursiveEVCalculation(flipsSoFar + 1, headsSoFar + 1, cheaterHeadsRate, minDepth - 1, maxDepth - 1, penalty: penalty + 1) +
            // Cases with another flip but no more heads
            (pCheater * (1-cheaterHeadsRate) + (1-pCheater) * 0.5) * RecursiveEVCalculation(flipsSoFar + 1, headsSoFar, cheaterHeadsRate, minDepth - 1, maxDepth - 1, penalty: penalty + 1)
        ;
        return System.Math.Max(justPickEV, evWait) - penalty;
    }
    bool AddToCachedDecisions(int total, int heads, PlayerType decision) {
        return AddToCachedDecisions(new Tuple<int, int>(total, heads), decision);
    }
    bool AddToCachedDecisions(Tuple<int, int> key, PlayerType decision) {
        if (!cachedDecisions.ContainsKey(key)) {
            // Debug.Log($"Adding {key.Item1}, {key.Item2}, {decision}");
            cachedDecisions.Add(key, decision);
            return true;
        }
        return false;
    }
}
public enum Strategy {
    JustOne,
    FractionThresholds,
    Odds,
    ExtraOdds,
    EVAttempt,
    RecursiveEV
}
