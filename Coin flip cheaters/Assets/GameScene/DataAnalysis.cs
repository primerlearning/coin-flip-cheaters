﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Video;
using UnityEditor;
using TMPro;
using System;
using System.IO;

public class DataAnalysis : Director
{
    int highScoreThreshold = 3000;
    int actualTopScore = 4541;
    // List<SessionResult> allSessions = new List<SessionResult>();
    protected override void Awake() {
        base.Awake();
    }
    protected override void Start() {
        base.Start();
    }
    protected override void DefineSchedule() {
        new SceneBlock(1f, Appear);
    }
    //Define event actions
    protected virtual IEnumerator Appear() {
        Parse();
        ResultsContainer allSessions = Parse();

        int numFlips = 5;
        for (int i = 0; i <= numFlips; i++) {
            List<FlipperData> flippers = FindFlippersWithExactResult(numFlips, i, allSessions);
            string output = $"{numFlips} flips with {i} heads\n";
            output += $"{flippers.Count} total\n";
            output += $"General\n";
            output += $"True cheater rate: {FractionCheaters(flippers)}\n";
            output += $"Accusasion rate: {FractionAccused(flippers)}\n";
            output += $"Correct rate: {FractionCorrect(flippers)}\n";
            output += "\n";
            output += $"Leet\n";
            flippers = FindFlippersWithExactResult(numFlips, i, SelectOptimalRuns(allSessions));
            output += $"True cheater rate: {FractionCheaters(flippers)}\n";
            output += $"Accusasion rate: {FractionAccused(flippers)}\n";
            output += $"Correct rate: {FractionCorrect(flippers)}\n";
            output += "\n";
            output += $"NonLeet\n";
            flippers = FindFlippersWithExactResult(numFlips, i, SelectNonOptimalRuns(allSessions));
            output += $"True cheater rate: {FractionCheaters(flippers)}\n";
            output += $"Accusasion rate: {FractionAccused(flippers)}\n";
            output += $"Correct rate: {FractionCorrect(flippers)}";
            Debug.Log(output);
        }
        yield return null;
        Debug.Log("done");
    }
    // Data processing fns
    ResultsContainer Parse() {
        string filePath = Path.Combine(Application.dataPath, "logs.json");
        string json = File.ReadAllText(filePath);
        ResultsContainer results = JsonUtility.FromJson<ResultsContainer>(json);
        return results;
    }
    ResultsContainer SelectOptimalPlayers(ResultsContainer allSessions) {
        List<string> leet_IDs = new List<string>();
        foreach (SingleContainer cont in allSessions.all) {
            if (cont.data.score >= highScoreThreshold && cont.data.score <= actualTopScore && !leet_IDs.Contains(cont.user_id)) {
                leet_IDs.Add(cont.user_id);
            }
        }
        ResultsContainer leetSessions = new ResultsContainer();
        leetSessions.all = new List<SingleContainer>();
        foreach (SingleContainer cont in allSessions.all) {
            if (leet_IDs.Contains(cont.user_id)) {
                SingleContainer newCont = new SingleContainer();
                newCont.data = cont.data;
                leetSessions.all.Add(newCont);
            }
        }
        return leetSessions;
    }
    ResultsContainer SelectNonOptimalPlayers(ResultsContainer allSessions) {
        List<string> leet_IDs = new List<string>();
        foreach (SingleContainer cont in allSessions.all) {
            if (cont.data.score >= highScoreThreshold && cont.data.score <= actualTopScore && !leet_IDs.Contains(cont.user_id)) {
                leet_IDs.Add(cont.user_id);
            }
        }
        ResultsContainer nonLeetSessions = new ResultsContainer();
        nonLeetSessions.all = new List<SingleContainer>();
        foreach (SingleContainer cont in allSessions.all) {
            if (!leet_IDs.Contains(cont.user_id)) {
                SingleContainer newCont = new SingleContainer();
                newCont.data = cont.data;
                nonLeetSessions.all.Add(newCont);
            }
        }
        return nonLeetSessions;
    }
    ResultsContainer SelectOptimalRuns(ResultsContainer allSessions) {
        ResultsContainer leetSessions = new ResultsContainer();
        leetSessions.all = new List<SingleContainer>();
        foreach (SingleContainer cont in allSessions.all) {
            if (cont.data.score >= highScoreThreshold && cont.data.score <= actualTopScore) {
                SingleContainer newCont = new SingleContainer();
                newCont.data = cont.data;
                leetSessions.all.Add(newCont);
            }
        }
        return leetSessions;
    }
    ResultsContainer SelectNonOptimalRuns(ResultsContainer allSessions) {
        ResultsContainer nonLeetSessions = new ResultsContainer();
        nonLeetSessions.all = new List<SingleContainer>();
        foreach (SingleContainer cont in allSessions.all) {
            if (!(cont.data.score >= highScoreThreshold && cont.data.score <= actualTopScore)) {
                SingleContainer newCont = new SingleContainer();
                newCont.data = cont.data;
                nonLeetSessions.all.Add(newCont);
            }
        }
        return nonLeetSessions;
    }
    List<FlipperData> FindFlippersWithResult(int firstNFlips, int numHeads, ResultsContainer sessionsSet) {
        List<FlipperData> selectedFlippers = new List<FlipperData>();
        foreach (SingleContainer cont in sessionsSet.all) {
            foreach (FlipperData thisFlipper in cont.data.flipperData) {
                if (thisFlipper.results.Count >= firstNFlips && thisFlipper.results.Take(firstNFlips).Sum() == numHeads) {
                    selectedFlippers.Add(thisFlipper);
                }
            }
        }
        return selectedFlippers;
    }
    List<FlipperData> FindFlippersWithExactResult(int firstNFlips, int numHeads, ResultsContainer sessionsSet) {
        List<FlipperData> selectedFlippers = new List<FlipperData>();
        foreach (SingleContainer cont in sessionsSet.all) {
            foreach (FlipperData thisFlipper in cont.data.flipperData) {
                if (thisFlipper.results.Count == firstNFlips && thisFlipper.results.Sum() == numHeads) {
                    selectedFlippers.Add(thisFlipper);
                }
            }
        }
        return selectedFlippers;
    }
    double FractionCheaters(List<FlipperData> flippers) {
        int numCheaters = 0;
        foreach (FlipperData flipper in flippers) {
            if (flipper.trueType == PlayerType.Cheater) {
                numCheaters++;
            }
        }
        return (double) numCheaters / flippers.Count;
    }
    double FractionAccused(List<FlipperData> flippers) {
        int numCheaters = 0;
        foreach (FlipperData flipper in flippers) {
            if (flipper.labeledType == PlayerType.Cheater) {
                numCheaters++;
            }
        }
        return (double) numCheaters / flippers.Count;
    }
    double FractionCorrect(List<FlipperData> flippers) {
        int numCheaters = 0;
        foreach (FlipperData flipper in flippers) {
            if (flipper.labeledType == flipper.trueType) {
                numCheaters++;
            }
        }
        return (double) numCheaters / flippers.Count;
    }


    // Emails
    void PrintTopScorerEmails(int num, ResultsContainer allSessions) {
        List<SingleContainer> resultsByScore = allSessions.all.OrderByDescending(x=>x.data.score).ToList();
        int maxIndex = num;
        for (int i = 0; i < maxIndex; i++) {
            SessionResult sesh = resultsByScore[i].data;
            if (sesh.score > 4542) { maxIndex++; }
            else {
                Debug.Log($"{sesh.name}, {sesh.score}, {sesh.email}");
            }
        }
    }
    void PrintRandomEmails(ResultsContainer allSessions) {
        List<string> emails = new List<string>();
        foreach (SingleContainer cont in allSessions.all) {
            if (cont.data.email != null && !emails.Contains(cont.data.email)) {
            // if (cont.data.email.Contains("rocoko")) {
                emails.Add(cont.data.email);
                Debug.Log(cont.data.email);
            }
        }
        // emails.Shuffle();
        // for (int i = 0; i < 5; i++) {
        //     Debug.Log(emails[i]);
        // }
        // Debug.Log(emails);
    }
    void CheckUserIDs(ResultsContainer allSessions) {
        List<string> candidateWinners = new List<string> () {
            "thorgeengelmann@gmail.com ",
            "justinchauccy@gmail.com",
            "jacobzhang719@gmail.com",
            "rroccokko@gmail.com",
            "emilkris33@gmail.com"
        };
        List<string> candidateIDs = new List<string>();

        foreach (SingleContainer cont in allSessions.all) {
            if (candidateWinners.Contains(cont.data.email)) {
                Debug.Log($"{cont.user_id}, {cont.data.email}");
                candidateIDs.Add(cont.user_id);
            }
        }
        Debug.Log("::::::::::::::::::::::::::");
        foreach (SingleContainer cont in allSessions.all) {
            if (candidateIDs.Contains(cont.user_id)) {
                Debug.Log($"{cont.user_id}, {cont.data.email}");
            }
        }
    }
}
[Serializable]
public class ResultsContainer {
    // public SingleContainer[] all;
    public List<SingleContainer> all;
}
[Serializable]
public class SingleContainer {
    public SessionResult data;
    public string submitted_from_ip;
    public string submitted;
    public string user_id;
}