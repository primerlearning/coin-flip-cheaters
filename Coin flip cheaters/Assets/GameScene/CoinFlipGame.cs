﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Unity.Services.Analytics;
using Unity.Services.Core;
using TMPro;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization.Formatters.Binary;

/*
To do
// Improvements
- Reset submit button on reset
- Long name centering
HMM things
- Best way to handle optional naming? For people who want to enter a name,
I can just send data after they put their name. But if someone closes the window,
I still want the anonymous play data.
- Merch giveaway methods?
*/
public enum GameState {
    Playing,
    EndScreen
}
public enum UIState {
    Game,
    Scoreboard,
    Info
}
public enum ScoreboardState {
    None,
    Personal,
    Global
}
public class CoinFlipGame : SceneManager
{   
    // Test
    [Header("Test Options")]
    [SerializeField] bool testingEndScreen = false;
    List<SessionResult> parsedResults = new List<SessionResult>();
    // List<string> storedJsonResults = new List<string>();
    
    // Scoreboard
    [Header("Scoreboard references")]
    [SerializeField] ScoreboardEntry scoreboardEntryPrefab;
    [SerializeField] VerticalLayoutGroup scoresContainer;
    List<SessionResult> globalTopTen = new List<SessionResult>();
    List<SessionResult> personalTopTen = new List<SessionResult>();
    bool talkingToServer = false;
    ScoreboardState scoreboardState = ScoreboardState.None;

    // Containers
    [Header("Flipper container references")]
    [SerializeField] CoinFlipSimManager flipSimManager = null;
    internal CoinFlipper currentFlipper = null;
    PrimerObject trueNegativeContainer = null;
    PrimerObject truePositiveContainer = null;
    PrimerObject falseNegativeContainer;
    PrimerObject falsePositiveContainer;
    [SerializeField] PrimerObject floorPrefab = null;
    // UI
    internal GameState gameState = GameState.Playing;
    UIState uiState = UIState.Game;
    // Canvas views
    [Header("View containers")]
    [SerializeField] PrimerObject gameView;
    [SerializeField] PrimerObject scoreboardView;
    [SerializeField] PrimerObject infoView;
    [SerializeField] GameObject BlurVolume;
    // gameView subviews
    [SerializeField] PrimerObject playingView;
    [SerializeField] PrimerObject endScreenView;
    // Nav buttons
    [Header("Nav buttons")]
    [SerializeField] PrimerObject gameNavButton = null;
    [SerializeField] PrimerObject infoNavButton = null;
    [SerializeField] PrimerObject leaderboardNavButton = null;
    [SerializeField] RectTransform navContainer = null;

    [Header("Play mode UI")]
    [SerializeField] TextMeshProUGUI scoreDisplay;
    [SerializeField] TextMeshProUGUI flipsRemainingDisplay;
    [SerializeField] Button flipButton;
    [SerializeField] Button flip5Button;
    [SerializeField] TextMeshProUGUI flipButtonText;
    public Slider flipNumSlider;
    [SerializeField] Button endButton;
    [SerializeField] TextMeshProUGUI endButtonText;
    public Toggle animationToggle;
    [SerializeField] PrimerObject instruction1 = null;
    [SerializeField] PrimerObject instruction2 = null;
    [SerializeField] PrimerObject instruction3 = null;
    [SerializeField] PrimerObject gameOverText;

    [Header("End screen UI")]
    string attemptID;
    [SerializeField] Button submitNameButton = null;
    [SerializeField] TextMeshProUGUI submitNameButtonText = null;
    [SerializeField] PrimerObject finalScoreText = null;
    PrimerBlob submissionPreviewBlob;
    Camera submissionPreviewCamera;
    [SerializeField] GameObject submissionPreviewWindow;
    [SerializeField] TMP_Dropdown colorDropDown;
    [SerializeField] TMP_Dropdown accessoryDropDown;
    [SerializeField] TMP_InputField nameForScoreBoard;
    [SerializeField] TextMeshProUGUI nameForScoreBoardPlaceHolder;
    [SerializeField] TMP_InputField emailSubmission;
    [SerializeField] TextMeshProUGUI emailSubmissionPlaceHolder;
    private TouchScreenKeyboard keyboard;

    Vector3 camCenterFlipping = 0.2f * Vector3.up;
    Quaternion camRotationFlipping = Quaternion.Euler(17, 0, 0);
    float camZoomFlipping = 14;
    // Vector3 camCenterEndScreen = new Vector3(-1.2f, -1.5f, 0);
    Vector3 camCenterEndScreen = -4.5f * Vector3.up;
    // Quaternion camRotationEndScreen = Quaternion.Euler(23, 0, 0);
    Quaternion camRotationEndScreen = Quaternion.Euler(23, 0, 0);
    // float camZoomEndScreen = 19;
    float camZoomEndScreen = 28f;
    IEnumerator currentUIAnim = null;

    // Game parameters
    int startingFlipPool = 100;
    internal int flipsRemaining = 1; // Start at one so the button doesn't get disabled for a frame
    public int FlipsRemaining {
        get { return flipsRemaining; }
        set { flipsRemaining = value; }
    }
    float fractionCheaters = 0.5f; // How many cheaters there are
    internal float cheaterHeadsRate = 0.75f; // Effect size
    int startingScore = 0;
    int score = 0;
    public int Score {
        get { return score; }
        set { score = value; }
    }
    
    internal int correctPointReward = 1;
    static int multiplier = 15;
    // internal int tpReward = 1 * multiplier;
    // internal int fpPenalty = 16 * multiplier;
    // internal int tnReward = 4 * multiplier;
    // internal int fnPenalty = 19 * multiplier;
    internal static int tpRewardBase = 1;
    internal static int fpPenaltyBase = 2;
    internal static int tnRewardBase = 1;
    internal static int fnPenaltyBase = 2;
    internal int tpReward = tnRewardBase * multiplier;
    internal int fpPenalty = fpPenaltyBase * multiplier;
    internal int tnReward = tnRewardBase * multiplier;
    internal int fnPenalty = fnPenaltyBase * multiplier;
    public int Multiplier {
        get { return multiplier; }
        set {
            tpReward = tpRewardBase * value;
            fpPenalty = fpPenaltyBase * value;
            tnReward = tnRewardBase * value;
            fnPenalty = fnPenaltyBase * value;
            multiplier = value;
        }
    }
    // internal int tpReward = 5;
    // internal int fpPenalty = 80;
    // internal int tnReward = 20;
    // internal int fnPenalty = 95;
    // int numFlipsPerPress = 1;

    // Params for animating to a new flipper
    bool animatingFlipperChange = true;
    bool movingFlipperToBin = false;

    // [SerializeField] InputValueGrabber valueGrabber = null;
    List<CoinFlipper> accused = new List<CoinFlipper>();
    List<CoinFlipper> exonerated = new List<CoinFlipper>();

    PoissonDiscPointSet nSet = new PoissonDiscPointSet(0.25f, new Vector2(1.6f, 1.6f), circular: false, overflowMode: PoissonDiscOverflowMode.Force);
    PoissonDiscPointSet pSet = new PoissonDiscPointSet(0.25f, new Vector2(1.6f, 1.6f), circular: false, overflowMode: PoissonDiscOverflowMode.Force);
    // Label prep
    PrimerText truth;
    PrimerText tFair;
    PrimerText tCheater;

    PrimerText labels;
    PrimerText lFair;
    PrimerText lCheater;
    PrimerText fpLabel;
    PrimerText tpLabel;
    PrimerText avgLabel;
    PrimerText fpRateLabel;
    PrimerText fnRateLabel;
    
    // Setup
    protected override void Awake() {
        base.Awake();
        // instruction1.transform.localScale = Vector3.zero;
        // instruction2.transform.localScale = Vector3.zero;
        // instruction3.transform.localScale = Vector3.zero;
    }
    async void Start()
    {
        // PlayerPrefs.DeleteAll();

        // Start Unity Analytics
        await UnityServices.InitializeAsync();

        // Initialize cookie jar
        CookieContainer cookieContainer;
        if (PlayerPrefs.HasKey("Cookie")) {
            Debug.Log("Setting");
            // Debug.Log(PlayerPrefs.GetString("Cookie"));
            // cookieContainer = JsonUtility.FromJson<CookieContainer>(PlayerPrefs.GetString("Cookie"));

            BinaryFormatter bf = new BinaryFormatter();

            byte[] decodedObject = Convert.FromBase64String(PlayerPrefs.GetString("Cookie"));
            MemoryStream stream = new MemoryStream(decodedObject);

            cookieContainer = bf.Deserialize(stream) as CookieContainer;
        } else {
            // Debug.Log("AHHHHHHHHHH");
            cookieContainer = new CookieContainer();
        }
        clientHandler = new HttpClientHandler {
            AllowAutoRedirect = true,
            UseCookies = true,
            CookieContainer = cookieContainer
        };
        client = new HttpClient(clientHandler);

        camRig.SwivelOrigin = camCenterFlipping;
        camRig.Swivel = camRotationFlipping;
        camRig.Distance = camZoomFlipping;
        
        SetUp();

        truth = Instantiate(primerTextPrefab);
        truth.tmpro.text = "The truth";
        truth.transform.localScale = Vector3.zero;
        tFair = Instantiate(primerTextPrefab);
        tFair.tmpro.text = "Fair";
        tFair.transform.localScale = Vector3.zero;
        tCheater = Instantiate(primerTextPrefab);
        tCheater.tmpro.text = "Cheater";
        tCheater.transform.localScale = Vector3.zero;

        labels = Instantiate(primerTextPrefab);
        labels.tmpro.text = "Your labels";
        labels.transform.localScale = Vector3.zero;
        lFair = Instantiate(primerTextPrefab);
        lFair.tmpro.text = "Fair";
        lFair.transform.localScale = Vector3.zero;
        lCheater = Instantiate(primerTextPrefab);
        lCheater.tmpro.text = "Cheater";
        lCheater.transform.localScale = Vector3.zero;
        fpRateLabel = Instantiate(primerTextPrefab);
        fpRateLabel.tmpro.text = "False-positive rate:";
        fpRateLabel.transform.localScale = Vector3.zero;
        fnRateLabel = Instantiate(primerTextPrefab);
        fnRateLabel.tmpro.text = "False-negative rate:";
        fnRateLabel.transform.localScale = Vector3.zero;

        // PlayerPrefs.SetString("Cookie", "session=eyJjb2luX2ZsaXBfdXNlcl9pZCI6ImUzODJiZDc0LWNkMjMtNGE5Yy05NGQ5LTM1YThlY2JlYmUyNiJ9.YkzbMQ.LXpAQq8pzHCpP-jylH5yzidOc0I");
        // PlayerPrefs.SetString("Cookie", "session=eyJjb2luX2ZsaXBfdXNlcl9pZCI6ImUzODJiZDc0LWNkMjMtNGE5Yy05NGQ5LTM1YThlY2JlYmUyNiJ9.YkzbMQ.LXpAQq8pzHCpP-jylH5yzidOc0D");
        // PlayerPrefs.SetString("Cookie", "session=eyJjb2luX2ZsaXBfdXNlcl9pZCI6IjRiNDhmZDgxLWQxOTgtNDFmNS1iNjVmLTI0MjUyNTU3NTVjOSJ9.YlBglA.e56b1fsJUIdYN9blHyr-tc6jVEU");
    }
    void UpdateFlipsRemainingDisplay() {
        flipsRemainingDisplay.text = $"{flipsRemaining} Flips left";
        if (flipsRemaining <= 0) {
            flipButton.interactable = false;
        }
        else {
            flipButton.interactable = true;
        }
        if (flipsRemaining <= 4) {
            flip5Button.interactable = false;
        }
        else {
            flip5Button.interactable = true;
        }
    }
    void UpdateFlipsRemaining(int num, bool animate = false) {
        if (animate) {
            this.AnimateValue<int>("FlipsRemaining", num, onFrameFunc: UpdateFlipsRemainingDisplay);
        }
        else {
            flipsRemaining = num;
            UpdateFlipsRemainingDisplay();
        }
    }
    void UpdateScoreDisplay() {
        scoreDisplay.text = $"Score: {score}";
    }
    void UpdateScore(int num, bool animate = false) {
        if (animate) {
            this.AnimateValue<int>("Score", num, onFrameFunc: UpdateScoreDisplay);
        }
        else {
            score = num;
            UpdateScoreDisplay();
        }
    }
    void AddToScore(int addition) {
        StartCoroutine(addToScore(addition));
    }
    IEnumerator addToScore(int addition) {
        // Pop up the number 
        TextMeshProUGUI tempText = Instantiate(scoreDisplay);
        tempText.transform.SetParent(scoreDisplay.transform.parent);
        tempText.GetComponent<RectTransform>().pivot = new Vector2(0.06f, 0.4f);
        tempText.GetComponent<RectTransform>().offsetMax = new Vector2(-100, 365);
        tempText.GetComponent<RectTransform>().offsetMin = new Vector2(110, 315);
        tempText.color = PrimerColor.Green * Mathf.Sqrt(2);
        // tempText.transform.localPosition = new Vector3(tempText.transform.localPosition.x, 490, tempText.transform.localPosition.z);
        tempText.text = $"+{addition}";
        tempText.GetComponent<PrimerText>().ScaleUpFromZero(duration: 0.5f);
        UpdateScore(score + addition, animate: true);
        yield return new WaitForSeconds(1f);
        // Merge into score
        tempText.GetComponent<PrimerText>().Disappear(duration: 0.5f);
    }
    void AddToCoins(int addition) {
        StartCoroutine(addToCoins(addition));
    }
    IEnumerator addToCoins(int addition) {
        // Pop up the number 
        TextMeshProUGUI tempText = Instantiate(flipsRemainingDisplay);
        tempText.transform.SetParent(scoreDisplay.transform.parent);
        tempText.GetComponent<RectTransform>().pivot = new Vector2(0.94f, 0.4f);
        tempText.GetComponent<RectTransform>().offsetMax = new Vector2(-178, 365);
        tempText.GetComponent<RectTransform>().offsetMin = new Vector2(200, 315);
        if (addition > 0) {
            tempText.color = PrimerColor.Green * Mathf.Sqrt(2); // Not the best green. Text color is different somehow.
            tempText.text = $"+{addition}";
        }
        else {
            tempText.color = PrimerColor.Red;// * Mathf.Sqrt(2);
            tempText.text = $"{addition}";
        }
        // tempText.transform.localPosition = new Vector3(tempText.transform.localPosition.x, 490, tempText.transform.localPosition.z);
        tempText.GetComponent<PrimerText>().ScaleUpFromZero(duration: 0.5f);
        UpdateFlipsRemaining(flipsRemaining + addition, animate: true);
        yield return new WaitForSeconds(1f);
        // Merge into total 
        tempText.GetComponent<PrimerText>().Disappear(duration: 0.5f);

        // Uses new flipsRemaining
        if (flipsRemaining <= 0) {
            yield return new WaitForSeconds(1f);
            ToggleGameState();
        }
    }
    void Rewards(CoinFlipper f, bool animate = false) {
        int newScore = score;
        int newFlipBudget = FlipsRemaining;
        if (f.labeledType == PlayerType.Cheater) {
            if (f.trueType == PlayerType.Cheater) {
                newScore += correctPointReward;
                newFlipBudget += tpReward;
            }
            else { newFlipBudget -= fpPenalty; }
        }
        if (f.labeledType == PlayerType.Fair) {
            if (f.trueType == PlayerType.Fair) {
                newScore += correctPointReward;
                newFlipBudget += tnReward;
            }
            else { newFlipBudget -= fnPenalty; }
        }
        // UpdateScore(newScore, animate: animate);
        if (newScore - score != 0) {
            AddToScore(newScore - score);
        }
        AddToCoins(newFlipBudget - FlipsRemaining);
        // UpdateFlipsRemaining(newFlipBudget, animate: animate);
    }
    void SetUp() {
        StartCoroutine(setup());
    }
    IEnumerator setup() {
        flipSimManager.Initialize(Resources.Load<CoinFlipper>("FlipperPrefabBlob"));
        
        AddFlipper();
        MoveCamToFlippingPosition();
        yield return new WaitForSeconds(0.5f);
        instruction1.gameObject.SetActive(true);
        instruction1.ScaleUpFromZero();
        animatingFlipperChange = false;
        UpdateFlipsRemaining(startingFlipPool, animate: true);
        UpdateScore(startingScore, animate: true);
        yield return new WaitForSeconds(0.5f);
        instruction2.gameObject.SetActive(true);
        instruction2.ScaleUpFromZero();
        yield return new WaitForSeconds(0.5f);
        instruction3.gameObject.SetActive(true);
        instruction3.ScaleUpFromZero();
    }
    void AddFlipper() {
        currentFlipper = flipSimManager.AddFlipper(fractionCheaters, cheaterHeadsRate);
        currentFlipper.transform.position = Vector3.down * 0.8f;
        currentFlipper.Appear();
        RandomizeBlobAppearance();
        currentFlipper.resultsDisplayMode = ResultsDisplayMode.Numeric;
    }
    void NextFlipper() {
        StartCoroutine(nextFlipper());
    }
    IEnumerator nextFlipper() {
        animatingFlipperChange = true;
        AddFlipper();
        yield return new WaitForSeconds(1);
        animatingFlipperChange = false;
        endButton.interactable = true;
        endButtonText.text = "Finish";
    }
    void RandomizeBlobAppearance() {
        PrimerBlob blob = currentFlipper.flipperCharacter.GetComponent<PrimerBlob>();
        blob.SetColor(PrimerColor.BlobColors[sceneRandom.Next(PrimerColor.BlobColors.Count)]);
        double accessoryChance = 0.7f;
        double complementaryChance = 0.5f;

        if (sceneRandom.NextDouble() < accessoryChance) {
            bool colorMatch = true;
            AccessoryType aType = AccessoryOptions[sceneRandom.Next(AccessoryOptions.Count)];
            if (aType == AccessoryType.beard || aType == AccessoryType.eyePatch) { colorMatch = false; }
            blob.AddAccessory(aType, colorMatch: colorMatch);
        }
    }
    List<AccessoryType> AccessoryOptions = new List<AccessoryType>() {
        AccessoryType.beard,
        AccessoryType.glasses,
        AccessoryType.sunglasses,
        AccessoryType.froggyHat,
        AccessoryType.beanie,
        AccessoryType.eyePatch,
        AccessoryType.propellerHat,
        AccessoryType.starShades,
        AccessoryType.wizardHat,
        AccessoryType.monocle
    };
    // Flip 
    public void Flip(int num) {
        if (TakingInputs()) {
            StartCoroutine(flip(num));
        }
    }
    public void UpdateButtonNumber() {
        if (flipNumSlider.value > flipsRemaining) {
            flipNumSlider.value = flipsRemaining;
        }
        string timesText = "times";
        if (flipNumSlider.value == 1) { timesText = "time"; }
        
        flipButtonText.text = $"Flip {flipNumSlider.value} {timesText}";
    }
    IEnumerator flip(int num) {
        if (animationToggle.isOn) {
            Time.timeScale = Mathf.Floor(1 + (float) num / 2);
            // I do not remember why I made this a while loop instead of a for.
            int i = 0;
            while (i < num) {
                yield return currentFlipper.flipAndRecord();
                i++;
                UpdateFlipsRemaining(flipsRemaining - 1);
            }
            // Wait for the last flip
            while (!TakingInputs()) {
                yield return null;
            }
            Time.timeScale = 1;
        }
        else {
            for (int i = 0; i < num; i++) {
                currentFlipper.NonAnimatedFlip();
                UpdateFlipsRemaining(flipsRemaining - 1);
                yield return null;
            }
        }
    }
    // Adding labels
    public void Accuse() {
        if (TakingInputs()) {
            if (instruction1 != null) {
                instruction1.ScaleDownToZero();
                instruction2.ScaleDownToZero();
                instruction3.ScaleDownToZero();
            }
            currentFlipper.labeledType = PlayerType.Cheater;
            StartCoroutine(flipperChange());
            // MoveFlipperToBin(currentFlipper);
            // NextFlipper();
        }
    }
    public void Exonerate() {
        if (TakingInputs()) {
            if (instruction1 != null) {
                instruction1.ScaleDownToZero();
                instruction2.ScaleDownToZero();
                instruction3.ScaleDownToZero();
            }
            currentFlipper.labeledType = PlayerType.Fair;
            StartCoroutine(flipperChange());
            // MoveFlipperToBin(currentFlipper);
            // NextFlipper();
        }
    }
    IEnumerator flipperChange() {
        yield return moveFlipperToBin(currentFlipper);
        if (flipsRemaining > 0) {
            yield return nextFlipper();
        }
        else {
            yield return new WaitForSeconds(0.5f);
            gameOverText.ScaleUpFromZero();
        }
    }
    void MoveFlipperToBin(CoinFlipper c) {
        StartCoroutine(moveFlipperToBin(c));
    }
    IEnumerator moveFlipperToBin(CoinFlipper c) {
        movingFlipperToBin = true;
        PlayerType label = c.labeledType;
        c.FormOfBlob();
        c.MoveBy(Vector3.down * 0.4f);
        if (labels.transform.localScale == Vector3.zero) {
            labels.transform.localPosition = new Vector3(0, 6.4f, 9);
            labels.ScaleUpFromZero();
            lFair.transform.localPosition = new Vector3(-2, 5, 9);
            lFair.ScaleTo(0.5f);
            lCheater.transform.localPosition = new Vector3(2, 5, 9);
            lCheater.ScaleTo(0.5f);

            truePositiveContainer = Instantiate(floorPrefab);
            truePositiveContainer.transform.localPosition = new Vector3(2.25f, 2.69f, 5.785f);
            truePositiveContainer.SetIntrinsicScale(2);
            truePositiveContainer.ScaleUpFromZero();

            trueNegativeContainer = Instantiate(floorPrefab);
            trueNegativeContainer.transform.localPosition = new Vector3(-2.25f, 2.69f, 5.785f);
            trueNegativeContainer.SetIntrinsicScale(2);
            trueNegativeContainer.ScaleUpFromZero();
        }
        // yield return new WaitForSeconds(0.5f);
        BlobAccessory sign = null;
        if (label == PlayerType.Cheater) {
            sign = c.flipperCharacter.GetComponent<PrimerBlob>().AddAccessory(AccessoryType.cheaterSign, animate: true);
        }
        else {
            sign = c.flipperCharacter.GetComponent<PrimerBlob>().AddAccessory(AccessoryType.fairSign, animate: true);
        }
        float vDisp = 1f;
        float extraDisp = 0;
        BlobAccessory prev = c.flipperCharacter.GetComponent<PrimerBlob>().accessories[0];
        if (signHeights.ContainsKey(prev.accessoryType)) { extraDisp = signHeights[prev.accessoryType]; }
        sign.transform.localPosition = (vDisp + extraDisp) * Vector3.up;
        yield return new WaitForSeconds(0.5f);
        sign.MoveTo(extraDisp * Vector3.up);
        yield return new WaitForSeconds(0.5f);

        if (c.trueType == c.labeledType) {
            PrimerText check = Instantiate(Resources.Load<PrimerText>("checkmark"));
            check.transform.parent = c.flipperCharacter.transform;
            check.transform.localPosition = (3 + extraDisp) * Vector3.up;
            check.SetColor(PrimerColor.Green);
            check.ScaleUpFromZero(duration: 0.25f);
        }
        else {
            PrimerText x = Instantiate(Resources.Load<PrimerText>("x"));
            x.transform.parent = c.flipperCharacter.transform;
            x.transform.localPosition = (3 + extraDisp) * Vector3.up;
            x.SetColor(PrimerColor.Red);
            x.ScaleUpFromZero(duration: 0.25f);
        }
        
        Rewards(c, animate: true);

        yield return new WaitForSeconds(0.5f);
        PrimerObject bin = truePositiveContainer;
        List<CoinFlipper> fList = accused;
        PoissonDiscPointSet discSet = pSet;
        if (label == PlayerType.Fair) { 
            bin = trueNegativeContainer;
            fList = exonerated;
            discSet = nSet;
        }

        c.transform.parent = bin.transform;
        if (fList.Count < 40) {
            discSet.AddPoint();
            Vector2 pos = discSet.GetCenteredPoints()[discSet.GetCenteredPoints().Count - 1];
            c.MoveTo(new Vector3(pos.x, 0, pos.y));
        }
        else {
            CoinFlipper oldFlipper = fList[0];
            Vector3 oldPos = oldFlipper.transform.localPosition;
            oldFlipper.Disappear();
            fList.Remove(oldFlipper);
            c.MoveTo(oldPos);
        }
        fList.Add(c);
        c.ScaleTo(0.2f * Vector3.one);
        c.flipperCharacter.GetComponent<PrimerBlob>().SwapMesh();
        movingFlipperToBin = false;
    }
    Dictionary<AccessoryType, float> signHeights = new Dictionary<AccessoryType, float>() {
        {AccessoryType.froggyHat, 0.2f},
        {AccessoryType.beanie, 0.34f},
        {AccessoryType.propellerHat, 0.3f},
        {AccessoryType.wizardHat, 0.43f}
    };

    // See results
    public void ToggleGameState() {
        if (TakingInputs()) {
            if (gameState == GameState.Playing) {
                End();
                gameState = GameState.EndScreen;
            }
            else if (gameState == GameState.EndScreen) {
                Reset();
                gameState = GameState.Playing;
            }
        }
    }
    void End() {
        StartCoroutine(end());
    }
    internal IEnumerator end() {
        attemptID = System.Guid.NewGuid().ToString();
        AutoSubmit();
        instruction1.ScaleDownToZero();
        instruction2.ScaleDownToZero();
        instruction3.ScaleDownToZero();
        yield return new WaitForSeconds(0.5f);
        gameOverText.ScaleDownToZero();

        SwitchToEndView();
        // Calculate stats and save
        if (testingEndScreen) {
            AddTestingSet();
            yield return new WaitForSeconds(1.0f);
        }
        // flipSimManager.flippers.Remove(currentFlipper);
        flipSimManager.CategorizeTestedFlippers();

        // Animate to summary screen
        // animatingFlipperChange = true;
        // currentFlipper.Disappear();
        yield return new WaitForSeconds(0.5f);
        MoveCamToEndScreenPosition();
        // yield return new WaitForSeconds(0.5f);
        SortToTable();
        // yield return new WaitForSeconds(0.5f);

        // Table labels
        truth.transform.position = new Vector3(-9.5f, 1.17f, 13);
        truth.transform.localRotation = Quaternion.Euler(17, 0, 90);
        truth.ScaleUpFromZero();
        tFair.transform.position = new Vector3(-7.5f, 3.17f, 13);
        tFair.transform.localRotation = Quaternion.Euler(17, 0, 90);
        tFair.ScaleTo(0.5f);
        tCheater.transform.position = new Vector3(-7.5f, 0.17f, 13);
        tCheater.transform.localRotation = Quaternion.Euler(17, 0, 90);
        tCheater.ScaleTo(0.5f);

        // False negative and false positive rates
        double fpRate = 100 * (double) flipSimManager.falsePositives.Count / flipSimManager.nulls.Count;
        double roundedNum = (double) Math.Round(fpRate, 1);
        string numString = roundedNum.ToString() + "%";
        if (fpRate != roundedNum) { numString = "~" + numString; }
        fpRateLabel.tmpro.text = $"False-positive\nrate: {numString}";
        fpRateLabel.transform.position = new Vector3(7.1f, 3.17f, 10);
        fpRateLabel.transform.localRotation = Quaternion.Euler(17, 0, 0);
        fpRateLabel.ScaleTo(0.5f);
        double fnRate = 100 * (double) flipSimManager.falseNegatives.Count / flipSimManager.alternatives.Count;
        roundedNum = (double) Math.Round(fnRate, 1);
        numString = roundedNum.ToString() + "%";
        if (fnRate != roundedNum) { numString = "~" + numString; }
        fnRateLabel.tmpro.text = $"False-negative\nrate: {numString}";
        fnRateLabel.transform.position = new Vector3(7.6f, 0.5f, 10);
        fnRateLabel.transform.localRotation = Quaternion.Euler(17, 0, 0);
        fnRateLabel.ScaleTo(0.5f);

        yield return new WaitForSeconds(0.5f);
        finalScoreText.ScaleUpFromZero();

        // fpLabel = Instantiate(primerTextPrefab);
        // fpLabel.tmpro.text = $"{Mathf.Round(summary.fpRate * 100)}% of fair players wrongfully accused";
        // fpLabel.transform.position = new Vector3(-1, -9, 15);
        // fpLabel.transform.rotation = camRotationFlipping;
        // fpLabel.ScaleUpFromZero();

        // tpLabel = Instantiate(primerTextPrefab);
        // tpLabel.tmpro.text = $"{Mathf.Round(summary.tpRate * 100)}% of cheaters caught";
        // tpLabel.transform.position = new Vector3(-1, -7, 15);
        // tpLabel.transform.rotation = camRotationFlipping;
        // tpLabel.ScaleUpFromZero();

        // avgLabel = Instantiate(primerTextPrefab);
        // avgLabel.tmpro.text = $"{Mathf.Round(summary.avgFlips * 10) / 10} flips per test";
        // avgLabel.transform.position = new Vector3(-1, -5, 15);
        // avgLabel.transform.rotation = camRotationFlipping;
        // avgLabel.ScaleUpFromZero();

        // animatingFlipperChange = false;
    }
    void SortToTable() {
        falsePositiveContainer = Instantiate(floorPrefab);
        falsePositiveContainer.transform.localPosition = truePositiveContainer.transform.localPosition;
        falsePositiveContainer.transform.localScale = truePositiveContainer.transform.localScale;
        foreach (CoinFlipper c in accused) {
            if (flipSimManager.falsePositives.Contains(c)) {
                c.transform.parent = falsePositiveContainer.transform;
            }
        }
        truePositiveContainer.MoveTo(new Vector3(2.25f, 1, 4));
        falseNegativeContainer = Instantiate(floorPrefab);
        falseNegativeContainer.transform.localPosition = trueNegativeContainer.transform.localPosition;
        falseNegativeContainer.transform.localScale = trueNegativeContainer.transform.localScale;
        foreach (CoinFlipper c in exonerated) {
            if (flipSimManager.falseNegatives.Contains(c)) {
                c.transform.parent = falseNegativeContainer.transform;
            }
        }
        falseNegativeContainer.MoveTo(new Vector3(-2.25f, 1, 4));
        for (int i = 0; i < flipSimManager.truePositives.Count; i++) {
            CoinFlipper f = flipSimManager.truePositives[i];
            f.flipperCharacter.animator.SetBool("Sad", true);
        }
        for (int i = 0; i < flipSimManager.trueNegatives.Count; i++) {
            CoinFlipper f = flipSimManager.trueNegatives[i];
            f.flipperCharacter.animator.SetTrigger("MouthSmile");
        }
        for (int i = 0; i < flipSimManager.falsePositives.Count; i++) {
            CoinFlipper f = flipSimManager.falsePositives[i];
            f.flipperCharacter.animator.SetBool("Sad", true);
        }
        for (int i = 0; i < flipSimManager.falseNegatives.Count; i++) {
            CoinFlipper f = flipSimManager.falseNegatives[i];
            f.flipperCharacter.animator.SetBool("Victory", true);
            f.flipperCharacter.animator.SetTrigger("MouthSmile");
            f.flipperCharacter.animator.SetTrigger("LeftEyeStern");
            f.flipperCharacter.animator.SetTrigger("RightEyeStern");
        }
    }
    void MoveCamToEndScreenPosition() {
        camRig.SwivelTo(camRotationEndScreen);
        camRig.MoveCenterTo(camCenterEndScreen);
        camRig.ZoomTo(camZoomEndScreen);
    }
    // Reset
    internal void Reset() {
        SwitchToPlayingView();
        StartCoroutine(reset());
    }
    internal IEnumerator reset() {
        animatingFlipperChange = true;
        finalScoreText.ScaleDownToZero();
        foreach (CoinFlipper f in flipSimManager.flippers) {
            f.Disappear();
        }
        foreach (PrimerShapesLine l in GameObject.FindObjectsOfType<PrimerShapesLine>()) {
            l.Disappear();
        }
        exonerated = new List<CoinFlipper>();
        accused = new List<CoinFlipper>();

        truth.ScaleDownToZero();
        tFair.ScaleDownToZero();
        tCheater.ScaleDownToZero();
        // fpLabel.ScaleDownToZero();
        // tpLabel.ScaleDownToZero();
        // avgLabel.ScaleDownToZero();
        fpRateLabel.ScaleDownToZero();
        fnRateLabel.ScaleDownToZero();

        falseNegativeContainer.Disappear();
        truePositiveContainer.Disappear();
        truePositiveContainer = falsePositiveContainer;
        
        flipSimManager.flippers = new List<CoinFlipper>();
        yield return new WaitForSeconds(0.1f);
        MoveCamToFlippingPosition();
        yield return new WaitForSeconds(0.5f);
        submitNameButtonText.text = "Submit";
        AddFlipper();
        UpdateFlipsRemaining(startingFlipPool, animate: false);
        UpdateScore(startingScore, animate: true);
        animatingFlipperChange = false;
        endButton.interactable = false;
        endButtonText.text = "Label at least one blob";
    }
    void MoveCamToFlippingPosition() {
        camRig.SwivelTo(camRotationFlipping);
        camRig.MoveCenterTo(camCenterFlipping);
        camRig.ZoomTo(camZoomFlipping);
    }
    // Manage inputs
    internal bool TakingInputs() {
        if (animatingFlipperChange || movingFlipperToBin) {
            return false;
        }
        if (currentFlipper.currentlyFlipping) {
            return false;
        }
        return true;
    }
    //Testing
    void AddTestingSet() {
        camRig.cam.backgroundColor = Color.black;
        StartCoroutine(addTestingSet());
    }
    IEnumerator addTestingSet() {
        int numBlobs = 40;
        for (int i = 0; i < numBlobs; i++) {
            // Accuse every other
            if (i % 2 == 0) {
                currentFlipper.labeledType = PlayerType.Cheater;
                MoveFlipperToBin(currentFlipper);
            }
            else {
                currentFlipper.labeledType = PlayerType.Fair;
                MoveFlipperToBin(currentFlipper);
            }
            if (i < numBlobs - 1) {
                AddFlipper();
            }
        }
        yield return null;
    }
    // Database!
    static HttpClient client;
    static HttpClientHandler clientHandler;
    // static CookieContainer cookieContainer;
    internal void SendToDataBase(SessionResult session, bool manual) {
        // StartCoroutine(sendToDataBase(session, manual));
        sendToDataBase(session, manual);
    }
    async void sendToDataBase(SessionResult session, bool manual) {
        talkingToServer = true;
        string json = JsonUtility.ToJson(session);
        byte[] encrypted = Encrypt(json, "%L9~SLS;y;T[af4Uj8^UgxTV#S]wY$");
        try {
            await client.PutAsync("https://primerlearning.org/api/v1/coin-flip-cheaters/log-results", new ByteArrayContent(encrypted));
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, clientHandler.CookieContainer);

            PlayerPrefs.SetString("Cookie", Convert.ToBase64String(stream.ToArray()));

            stream.Close();
            // PlayerPrefs.SetString("Cookie", JsonUtility.ToJson(clientHandler.CookieContainer));
            if (manual) {
                SubmissionComplete(true);
            }
        }
        catch(HttpRequestException e)
        {
            Debug.Log("\nException Caught!");  
            Debug.Log(e.Message);
            if (manual) {
                SubmissionComplete(false);
            }
        }
        // Debug.Log(json);
        // Debug.Log(encrypted);
        // Debug.Log(Decrypt(encrypted, "%L9~SLS;y;T[af4Uj8^UgxTV#S]wY$"));

        // Webgl code with relative url (interacts with real or test server)
        // UnityWebRequest www = UnityWebRequest.Put("/api/v1/coin-flip-cheaters/log-results", encrypted);

        // Android code with absolute url and manual cookie handling
        // UnityWebRequest www = UnityWebRequest.Put("https://primerlearning.org/api/v1/coin-flip-cheaters/log-results", encrypted);
        //     Debug.Log("||||||||||||||||||||||||||||||||||||||||||||||");
        // if (PlayerPrefs.HasKey("Cookie")) {
        //     Debug.Log("Sending Cookie in put");
        //     www.SetRequestHeader("Cookie", PlayerPrefs.GetString("Cookie"));
        //     Debug.Log(www.GetRequestHeader("Cookie"));
        // }

        // await www.SendWebRequest();

        // if (www.result != UnityWebRequest.Result.Success) {
        //     Debug.Log(www.error);
        //     if (manual) {
        //         SubmissionComplete(false);
        //     }
        // }
        // else {
        //     // Android code to save the cookie if the server gives one
        //     string setCookieHeader = www.GetResponseHeader("Set-Cookie");
        //     if (setCookieHeader != null) {
        //         PlayerPrefs.SetString("Cookie", setCookieHeader.Split(';')[0]);
        //         Debug.Log("Saving Cookie from put response");
        //         Debug.Log(setCookieHeader.Split(';')[0]);
        //     }
        //     Debug.Log("Upload complete!");

        // }
        talkingToServer = false;
    }
    internal void GetTopScoresFromDataBase() {
        // StartCoroutine(getTopScoresFromDataBase());
        getTopScoresFromDataBase();
    }
    async void getTopScoresFromDataBase() {
        talkingToServer = true;
        // Webgl code with relative url (interacts with real or test server)
        // UnityWebRequest www = UnityWebRequest.Get("/api/v1/coin-flip-cheaters/leaderboard");

        // Android code with absolute url and manual cookie handling
        // UnityWebRequest www = UnityWebRequest.Get("https://primerlearning.org/api/v1/coin-flip-cheaters/leaderboard");
        //     Debug.Log("||||||||||||||||||||||||||||||||||||||||||||||");
        // if (PlayerPrefs.HasKey("Cookie")) {
        //     Debug.Log("Sending Cookie in get");
        //     www.SetRequestHeader("Cookie", PlayerPrefs.GetString("Cookie"));
        //     Debug.Log(www.GetRequestHeader("Cookie"));
        // }

        // Call asynchronous network methods in a try/catch block to handle exceptions.
        try   
        {
            byte[] responseBody = await client.GetByteArrayAsync("https://primerlearning.org/api/v1/coin-flip-cheaters/leaderboard");

            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, clientHandler.CookieContainer);

            PlayerPrefs.SetString("Cookie", Convert.ToBase64String(stream.ToArray()));

            stream.Close();

            // PlayerPrefs.SetString("Cookie", JsonUtility.ToJson(clientHandler.CookieContainer));
            // Debug.Log(clientHandler.CookieContainer.ToString());
            // Debug.Log(PlayerPrefs.GetString("Cookie"));
            // Console.WriteLine(responseBody);

            // Android code to save the cookie if the server gives one
            // string setCookieHeader = www.GetResponseHeader("Set-Cookie");
            // if (setCookieHeader != null) {
            //     Debug.Log("Saving Cookie from get response");
            //     PlayerPrefs.SetString("Cookie", setCookieHeader.Split(';')[0]);
            //     Debug.Log(setCookieHeader.Split(';')[0]);
            // }

            // Show results as text
            // Debug.Log(www.downloadHandler.text);
            globalTopTen = new List<SessionResult>();
            personalTopTen = new List<SessionResult>();
            string decrypted = Decrypt(responseBody, "%L9~SLS;y;T[af4Uj8^UgxTV#S]wY$");
            // Debug.Log(decrypted);
            ResultsPackage resPack = JsonUtility.FromJson<ResultsPackage>(decrypted);
            // ResultsPackage resPack = JsonUtility.FromJson<ResultsPackage>(www.downloadHandler.text);
            foreach (SessionResult res in resPack.topPlayers) {
                globalTopTen.Add(res);
            }
            foreach (SessionResult res in resPack.yourScores) {
                personalTopTen.Add(res);
            }
            globalTopTen = globalTopTen.OrderByDescending(x => x.score).ToList();
            personalTopTen = personalTopTen.OrderByDescending(x => x.score).ToList();

            // Debug.Log(responseBody);
        }
        catch(HttpRequestException e)
        {
            Debug.Log("\nException Caught!");  
            Debug.Log(e.Message);
        }
        // yield return www.SendWebRequest(); 
        // if (www.result != UnityWebRequest.Result.Success) {
        //     talkingToServer = false;
        //     Debug.Log(www.error);
        // }
        // else {
        //     // Android code to save the cookie if the server gives one
        //     string setCookieHeader = www.GetResponseHeader("Set-Cookie");
        //     if (setCookieHeader != null) {
        //         Debug.Log("Saving Cookie from get response");
        //         PlayerPrefs.SetString("Cookie", setCookieHeader.Split(';')[0]);
        //         Debug.Log(setCookieHeader.Split(';')[0]);
        //     }

        //     // Show results as text
        //     // Debug.Log(www.downloadHandler.text);
        //     globalTopTen = new List<SessionResult>();
        //     personalTopTen = new List<SessionResult>();
        //     string decrypted = Decrypt((byte[])www.downloadHandler.data, "%L9~SLS;y;T[af4Uj8^UgxTV#S]wY$");
        //     // Debug.Log(decrypted);
        //     ResultsPackage resPack = JsonUtility.FromJson<ResultsPackage>(decrypted);
        //     // ResultsPackage resPack = JsonUtility.FromJson<ResultsPackage>(www.downloadHandler.text);
        //     foreach (SessionResult res in resPack.topPlayers) {
        //         globalTopTen.Add(res);
        //     }
        //     foreach (SessionResult res in resPack.yourScores) {
        //         personalTopTen.Add(res);
        //     }
        //     globalTopTen = globalTopTen.OrderByDescending(x => x.score).ToList();
        //     personalTopTen = personalTopTen.OrderByDescending(x => x.score).ToList();
        // }
        talkingToServer = false;
    }
    SessionResult ParseResultJson(string resultJson) {
        return JsonUtility.FromJson<SessionResult>(resultJson);
    }
    void GenerateSubmissionBlobAndCam() {
        Vector3 pos = new Vector3(100, 0, 0);
        submissionPreviewBlob = Instantiate((PrimerBlob)flipSimManager.flipperPrefab.flipperCharacterPrefab);
        submissionPreviewBlob.transform.position = pos;
        submissionPreviewBlob.transform.rotation = Quaternion.Euler(0, 160, 0);

        List<Color> colorOptions = new List<Color> (PrimerColor.BlobColors);
        colorOptions.Add(PrimerColor.Gray);
        colorOptions.Add(PrimerColor.White);

        int colorIndex = SceneManager.sceneRandom2.Next(colorOptions.Count);
        submissionPreviewBlob.SetColor(colorOptions[colorIndex]);
        colorDropDown.value = colorIndex;

        int accessoryIndex = SceneManager.sceneRandom2.Next(AccessoryOptions.Count + 1);
        if (accessoryIndex > 0) { // 0 Means no accessory
            AccessoryType aType = PrimerBlob.AccessoryOptions[accessoryIndex - 1];
            bool colorMatch = true;
            if (aType == AccessoryType.beard || aType == AccessoryType.eyePatch) { colorMatch = false; }
            submissionPreviewBlob.AddAccessory(aType, colorMatch: colorMatch);
        }
        accessoryDropDown.value = accessoryIndex;

        // Set up camera
        RenderTexture rt = new RenderTexture(80, 80, 24);
        cam = new GameObject().AddComponent<Camera>();
        cam.targetTexture = rt;
        submissionPreviewWindow.GetComponent<RawImage>().texture = rt;
        cam.clearFlags = CameraClearFlags.SolidColor;
        cam.backgroundColor = PrimerColor.Gray;

        cam.fieldOfView = 35;
        cam.transform.position = pos + new Vector3(0, 1.4f, -2);

        // RenderTexture.active = rt;
        // image = new Texture2D(resWidth, resHeight, TextureFormat.RGBA32, false);
        
        // // Render to image texture
        // cam.Render();
        // image.ReadPixels(new Rect(0, 0, resWidth, resHeight), 0, 0);
    }
    public void ChangeSubmissionPreviewBlobAccessory() {
        while (submissionPreviewBlob.accessories.Count > 0) {
            BlobAccessory ba = submissionPreviewBlob.accessories[0];
            submissionPreviewBlob.accessories.Remove(ba);
            // Destroy(ba.gameObject);
            ba.Disappear();
        }
        if (accessoryDropDown.value > 0) {
            AccessoryType aType = PrimerBlob.AccessoryOptions[accessoryDropDown.value - 1];
            bool colorMatch = true;
            if (aType == AccessoryType.beard || aType == AccessoryType.eyePatch) { colorMatch = false; }
            submissionPreviewBlob.AddAccessory(aType, colorMatch: colorMatch, animate: true);
        }
    }
    public void ChangeSubmissionPreviewBlobColor() {
        List<Color> colorOptions = new List<Color> (PrimerColor.BlobColors);
        colorOptions.Add(PrimerColor.Gray);
        colorOptions.Add(PrimerColor.White);
        submissionPreviewBlob.ChangeColor(colorOptions[colorDropDown.value]);
        ChangeSubmissionPreviewBlobAccessory();
    }
    internal void DestroyPreviewBlobAndCamera() {
        if (submissionPreviewBlob != null) {
            submissionPreviewBlob.Disappear();
        }
        if (submissionPreviewCamera != null) {
            Destroy(submissionPreviewCamera.gameObject);
        }
    }
    byte[] Encrypt(string text, string key)
    {
        byte[] textAsByteArray = Encoding.UTF8.GetBytes(text);
        byte[] keyAsByteArray = Encoding.UTF8.GetBytes(key);
        byte[] result = new byte[textAsByteArray.Length];
        for (int i = 0; i < textAsByteArray.Length; i++) {
            result[i] = (byte)(textAsByteArray[i] ^ keyAsByteArray[i % keyAsByteArray.Length]);
        }
        return result;
    }
    string Decrypt(byte[] data, string key)
    {
        byte[] keyAsByteArray = Encoding.UTF8.GetBytes(key);
        byte[] result = new byte[data.Length];
        for (int i = 0; i < data.Length; i++) {
            result[i] = (byte)(data[i] ^ keyAsByteArray[i % keyAsByteArray.Length]);
        }
        return Encoding.UTF8.GetString(result);
    }

    // string EncryptOrDecrypt(string text, string key)
    // {
    //     var result = new StringBuilder();
    //     for (int c = 0; c < text.Length; c++)
    //     {
    //         // take next character from string
    //         char character = text[c];

    //         // cast to a uint
    //         byte charCode = Encoding.UTF8.GetBytes(new char[] {character})[0];

    //         // figure out which character to take from the key
    //         int keyPosition = c % key.Length; // use modulo to "wrap round"

    //         // take the key character
    //         char keyChar = key[keyPosition];

    //         // cast it to a uint also
    //         // byte keyCode = (uint)keyChar;
    //         byte keyCode = Encoding.UTF8.GetBytes(new char[] {keyChar})[0];

    //         // perform XOR on the two character codes
    //         byte combinedCode = (byte)(charCode ^ keyCode);

    //         // cast back to a char
    //         char combinedChar = (char)combinedCode;

    //         // add to the result
    //         result.Append(combinedChar);
    //     }
    //     return result.ToString();
    // }
    void AutoSubmit() {
        SessionResult summary = new SessionResult(flipSimManager);
        summary.attempt_id = attemptID;
        summary.name = "Anonymous blob";
        // summary.blobAccessory = AccessoryType.none;
        // summary.blobColor = PrimerColor.Blue;
        summary.avatar.color = PrimerColor.Blue;
        summary.avatar.aType = AccessoryType.none;
        summary.email = "";
        SendToDataBase(summary, false);
    }
    public void SubmitResults() {
        submitNameButton.interactable = false;
        submitNameButtonText.text = "Submitting...";
        SessionResult summary = new SessionResult(flipSimManager);

        summary.attempt_id = attemptID;
        summary.name = nameForScoreBoard.text;

        if (summary.name == "​") { summary.name = ""; }
        // summary.blobAccessory = submissionPreviewBlob.accessories[0].accessoryType;
        // summary.blobColor = submissionPreviewBlob.color;
        summary.avatar.color = submissionPreviewBlob.color;
        if (submissionPreviewBlob.accessories.Count > 0) {
            summary.avatar.aType = submissionPreviewBlob.accessories[0].accessoryType;
        }
        else {
            summary.avatar.aType = AccessoryType.none;
        }
        summary.email = emailSubmission.text;
        if (summary.email == "​") { summary.email = ""; }
        SendToDataBase(summary, true);
    }
    // public void FillNameField() {
    //     // nameForScoreBoardPlaceHolder.enabled = false;
    //     // nameForScoreBoard.gameObject.text.SetActive(true);
    //     keyboard = TouchScreenKeyboard.Open(nameForScoreBoard.text, TouchScreenKeyboardType.Default);
    //     StartCoroutine(syncNameFieldWithKeyBoard());
    // }
    // IEnumerator syncNameFieldWithKeyBoard() {
    //     while (keyboard.active && keyboard.status != TouchScreenKeyboard.Status.Done) {
    //         nameForScoreBoard.text = keyboard.text;
    //         yield return null;
    //     }
    // }
    // public void PopulateNameField(string text) {
    // }
    void SubmissionComplete(bool success) {
        if (success) {
            submitNameButtonText.text = "Submitted!";
        }
        else {
            submitNameButtonText.text = "Submission failed. Try again?";
            submitNameButton.interactable = true;
        }
    }
    // GameState management
    void SwitchToPlayingView() {
        gameState = GameState.Playing;
        endScreenView.MoveRectTo(550 * Vector2.down);
        playingView.MoveRectTo(Vector2.zero);
        DestroyPreviewBlobAndCamera();
        StartCoroutine(delayedShadowModeSwitch(LightShadows.Soft));
    }
    IEnumerator delayedShadowModeSwitch(LightShadows newState, float delay = 0.5f) {
        yield return new WaitForSeconds(delay);
        GameObject.Find("Directional Light").GetComponent<Light>().shadows = newState;
    }
    void SwitchToEndView() {
        submitNameButton.interactable = true;
        gameState = GameState.EndScreen;
        playingView.MoveRectTo(500 * Vector2.down);
        endScreenView.MoveRectTo(Vector2.zero);
        // finalScoreText.GetComponent<TextMeshProUGUI>().text = score.ToString();
        finalScoreText.GetComponent<TextMeshProUGUI>().text = $"Score: {score}";
        StartCoroutine(delayedShadowModeSwitch(LightShadows.None));

        GenerateSubmissionBlobAndCam();
    }
    // UIState management
    public void SwitchToGameView() {
        // BlurVolume.SetActive(false);
        // if (uiState == UIState.Scoreboard) { 
        scoreboardView.MoveRectTo(2 * 720 * Vector2.right);
        // }
        // if (uiState == UIState.Info) {
        infoView.MoveRectTo(720 * Vector2.right);
        // }
        gameView.MoveRectTo(Vector3.zero);
        if (gameState == GameState.Playing) {
            StartCoroutine(delayedShadowModeSwitch(LightShadows.Soft, delay: 0.25f));
        }
        uiState = UIState.Game;
    }
    public void SwitchToScoreboardView() {
        // StartCoroutine(getTopScoresFromDataBase());
        getTopScoresFromDataBase();

        // BlurVolume.SetActive(true);
        // if (uiState == UIState.Game) { gameView.MoveRectTo(720 * Vector2.left); }
        // if (uiState == UIState.Info) { infoView.MoveRectTo(720 * Vector2.left); }
        scoreboardView.MoveRectTo(Vector3.zero);
        infoView.MoveRectTo(720 * Vector2.left);
        gameView.MoveRectTo(2 * 720 * Vector3.left);
        if (uiState != UIState.Scoreboard) {
            if (scoreboardState == ScoreboardState.None || scoreboardState == ScoreboardState.Global) {
                StartCoroutine(drawScoreboard(personal: false));
                // StartCoroutine(delayedShadowModeSwitch(LightShadows.None));
            }
            else {
                StartCoroutine(drawScoreboard(personal: true));
            }
            StartCoroutine(delayedShadowModeSwitch(LightShadows.None, delay: 0.25f));
        }
        // UpdateNav(UIState.Scoreboard);
        uiState = UIState.Scoreboard;
    }
    public void SwitchToInfoView() {
        // BlurVolume.SetActive(true);
        // if (uiState == UIState.Game) { gameView.MoveRectTo(720 * Vector2.left); }
        // if (uiState == UIState.Scoreboard) { scoreboardView.MoveRectTo(720 * Vector2.right); }
        infoView.MoveRectTo(Vector3.zero);
        scoreboardView.MoveRectTo(720 * Vector3.right);
        gameView.MoveRectTo(720 * Vector3.left);
        uiState = UIState.Info;
        // UpdateNav(UIState.Info);
    }
    // void UpdateNav(UIState newUIState, float duration = 0.5f) {
    //     uiState = newUIState;
    //     StartCoroutine(updateLayoutContinuously(navContainer, duration));
    //     if (uiState == UIState.Game) {
    //         gameNavButton.ScaleTo(0);
    //         infoNavButton.ScaleTo(1);
    //         leaderboardNavButton.ScaleTo(1);
    //     }
    //     else if (uiState == UIState.Info) {
    //         gameNavButton.ScaleTo(1);
    //         infoNavButton.ScaleTo(0);
    //         leaderboardNavButton.ScaleTo(1);
    //     }
    //     else if (uiState == UIState.Scoreboard) {
    //         gameNavButton.ScaleTo(1);
    //         infoNavButton.ScaleTo(1);
    //         leaderboardNavButton.ScaleTo(0);
    //     }
    // }
    IEnumerator updateLayoutContinuously(RectTransform rect, float duration) {
        float startTime = Time.time;
        while (Time.time < startTime + duration) {
            LayoutRebuilder.MarkLayoutForRebuild(rect);
            yield return null;
        }
    }
    public void DrawPersonalScoreBoard() {
        if (scoreboardState != ScoreboardState.Personal) {
            if (currentUIAnim != null) {
                StopCoroutine(currentUIAnim);
            }
            currentUIAnim = drawScoreboard(personal: true);
            StartCoroutine(currentUIAnim);
        }
    }
    public void DrawGlobalScoreBoard() {
        if (scoreboardState != ScoreboardState.Global) {
            if (currentUIAnim != null) {
                StopCoroutine(currentUIAnim);
            }
            currentUIAnim = drawScoreboard(personal: false);
            StartCoroutine(currentUIAnim);
            // StopCoroutine("drawScoreboard");
            // StartCoroutine(drawScoreboard(personal: false));
        }
    }
    IEnumerator drawScoreboard(float delay = 0.0f, bool personal = false) {
        if (personal) {
            scoreboardState = ScoreboardState.Personal;
        }
        else {
            scoreboardState = ScoreboardState.Global;
        }
        // Kick off http request and destroy old entries
        foreach (ScoreboardEntry entry in scoresContainer.GetComponentsInChildren<ScoreboardEntry>()) {
            // Destroy(entry.gameObject);
            entry.GoAway();
        }
        // Wait for slide animation, then make entries appear
        yield return new WaitForSeconds(delay);
        // bool immediateUpdate = false;
        // if (!talkingToServer) { immediateUpdate = true; }
        List<ScoreboardEntry> entries = new List<ScoreboardEntry>();
        // for (int i = 0; i < 10; i++) {
        //     if (immediateUpdate) {
        //         if (personal) {
        //             if (i < personalTopTen.Count) {
        //                 ScoreboardEntry entry = Instantiate(scoreboardEntryPrefab);
        //                 entry.index = i;
        //                 entries.Add(entry);
        //                 entry.GetComponent<RectTransform>().SetParent(scoresContainer.transform);
        //                 entry.ScaleUpFromZero();
        //                 entry.GenerateBlobAndCam();
        //                 entry.session = personalTopTen[i];
        //                 entry.ShowInfo();
        //             }
        //         }
        //         else {
        //             if (i < globalTopTen.Count) {
        //                 ScoreboardEntry entry = Instantiate(scoreboardEntryPrefab);
        //                 entry.index = i;
        //                 entries.Add(entry);
        //                 entry.GetComponent<RectTransform>().SetParent(scoresContainer.transform);
        //                 entry.ScaleUpFromZero();
        //                 entry.GenerateBlobAndCam();
        //                 entry.session = globalTopTen[i];
        //                 entry.ShowInfo();
        //             }
        //         }
        //     }
        //     yield return new WaitForSeconds(0.05f); 
        // }

        // If we didn't have immediate results, update them now.
        // if (!immediateUpdate) {
        while (talkingToServer) { yield return null; }
        if (personal) {
            for (int i = 0; i < Mathf.Min(personalTopTen.Count, 10); i++) {
                ScoreboardEntry entry = Instantiate(scoreboardEntryPrefab);
                entry.index = i;
                entries.Add(entry);
                entry.GetComponent<RectTransform>().SetParent(scoresContainer.transform);
                entry.ScaleUpFromZero();
                entry.GenerateBlobAndCam();
                entry.session = personalTopTen[i];
                entry.ShowInfo();
                yield return new WaitForSeconds(0.05f); 
            }
            if (entries.Count == 0) {
                ScoreboardEntry entry = Instantiate(scoreboardEntryPrefab);
                entry.GetComponent<RectTransform>().SetParent(scoresContainer.transform);
                entry.ScaleUpFromZero();
                entry.GenerateBlobAndCam();
                entry.MakeNullSession();
                entry.ShowInfo();
            }
        }
        else {
            for (int i = 0; i < Mathf.Min(globalTopTen.Count, 10); i++) {
                ScoreboardEntry entry = Instantiate(scoreboardEntryPrefab);
                entry.index = i;
                entries.Add(entry);
                entry.GetComponent<RectTransform>().SetParent(scoresContainer.transform);
                entry.ScaleUpFromZero();
                entry.GenerateBlobAndCam();
                entry.session = globalTopTen[i];
                entry.ShowInfo();
                yield return new WaitForSeconds(0.05f); 
            }
        }
    }
    void Update() {
        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            SwitchToScoreboardView();
        }
        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            SwitchToGameView();
        }
    }
}
[Serializable]
public class SessionResult
{   
    public string attempt_id;
    public int score;
    public List<FlipperData> flipperData;
    internal SessionResult(CoinFlipSimManager flipSimManager) {
        // Make the list of results data blocks for each flipper
        this.flipperData = new List<FlipperData>();
        foreach (CoinFlipper f in flipSimManager.flippers) {
            this.flipperData.Add(new FlipperData(f));
        }
        CoinFlipGame game = GameObject.FindObjectOfType<CoinFlipGame>();
        this.score = game.Score;
        this.avatar = new Avatar();
    }
    internal SessionResult(){}
    // Optional params
    public string name;
    // public Color blobColor;
    // public AccessoryType blobAccessory;
    public string email;
    public Avatar avatar;
}
[Serializable]
public class FlipperData //just strips out the results info from the CoinFlipper class
{
    public List<int> results;
    public PlayerType trueType;
    public PlayerType labeledType;
    public float headsRate;
    public Color blobColor;
    public AccessoryType accessoryType;
    public FlipperData(CoinFlipper f) {
        this.results = f.results;
        this.trueType = f.trueType;
        this.labeledType = f.labeledType;
        this.headsRate = f.headsRate;
        this.blobColor = ((PrimerBlob)f.flipperCharacter).color;
        this.accessoryType = ((PrimerBlob)f.flipperCharacter).accessories[0].accessoryType;
    }
}
public class ResultsPackage 
{
    public SessionResult[] topPlayers;
    public SessionResult[] yourScores;
}
[Serializable]
public class Avatar
{
    public Color color;
    public AccessoryType aType;
}